package org.hive.sample;

import org.junit.Test;
import com.mjolnirr.lib.Person;
import com.mjolnirr.sample.PersonModule;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/2/13
 * Time: 2:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonModuleTest {
    private PersonModule module = new PersonModule();

    @Test
    public void testCreatePerson() throws Exception {
        try {
            Person one = module.createPerson(new Person("John", "Doe"));
            Person two = module.createPerson(new Person("Ololo", "Trololo"));

            assert (one.getId() != 0);
            assert (two.getId() != 0);
        } catch (Exception e) {
            e.printStackTrace();

            assert false;
        }
    }

    @Test
    public void testGetPersonByID() throws Exception {
        try {
            List<Person> persons = module.getAllPersons();

            assert (persons.get(1).getId() != 0);
        } catch (Exception e) {
            e.printStackTrace();

            assert false;
        }
    }

    @Test
    public void testUpdatePerson() throws Exception {
        try {
            List<Person> persons = module.getAllPersons();
            module.updatePerson(persons.get(1));
        } catch (Exception e) {
            e.printStackTrace();

            assert false;
        }
    }

    @Test
    public void testGetAllPersons() throws Exception {
        try {
            List<Person> persons = module.getAllPersons();

            for (Person person : persons) {
                System.out.println(person.getId() + "" + person.getFirstName() + " " + person.getLastName());
            }
        } catch (Exception e) {
            e.printStackTrace();

            assert false;
        }
    }

    @Test
    public void testFindPersonsByLastName() throws Exception {
        try {
            List<Person> persons = module.findPersonsByLastName("Doe");

            for (Person person : persons) {
                assert person.getLastName().equals("Doe");
            }
        } catch (Exception e) {
            e.printStackTrace();

            assert false;
        }
    }

    @Test
    public void testDeletePerson() throws Exception {
        try {
            List<Person> persons = module.getAllPersons();

            assert module.getAllPersons().size() != 0;

            for (Person person : persons) {
                module.deletePerson(person.getId());
            }

            assert module.getAllPersons().size() == 0;
        } catch (Exception e) {
            e.printStackTrace();

            assert false;
        }
    }
}

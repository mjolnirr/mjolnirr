importClass(org.hive.browser.lib.helpers.InjectorHelper);
importClass(java.util.ArrayList);

function addToInputField(event) {
    input_fld.text = input_fld.text + event.getSource().id;
}

function clearInput(event) {
    input_fld.text = "";
}

function calculate(event) {
    var injectorHelper = new InjectorHelper();
    var containerTransport = injectorHelper.getMainRegister();

    var args = new ArrayList();
    args.add(input_fld.text);

    try {
        input_fld.text = containerTransport.processRequest("localhost", "8080", "Calculator", "calculate", args);
    } catch (err) {
        var message = "Something went wrong! \n" + err.message;
        input_fld.text = message;
        java.lang.System.out.println(err);
    }
}
package com.mjolnirr.sample;

import com.mjolnirr.lib.component.AbstractModule;
import org.hibernate.*;
import com.mjolnirr.lib.Person;
import com.mjolnirr.lib.component.ComponentContext;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/1/13
 * Time: 12:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonModule extends AbstractModule {
    private SessionFactory sessionFactory;

    public PersonModule() {
        sessionFactory = MjolnirrSessionFactory.getSessionFactory();
    }

    public Person createPerson(Person person) {
        Session session = sessionFactory.openSession();
        Person internalPerson = null;

        Transaction transaction = null;

        try {
            internalPerson = findPerson(person.getId());

            transaction = session.beginTransaction();

            if (internalPerson == null) {
                internalPerson = new Person(person);
            }

            session.save(internalPerson);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return internalPerson;
    }

    public Person getPersonByID(int id) {
        return findPerson(id);
    }

    public void updatePerson(Person person) {
        Session session = sessionFactory.openSession();

        Transaction transaction = null;

        try {
            Person internalPerson = findPerson(person.getId());

            transaction = session.beginTransaction();

            internalPerson.setFirstName(person.getFirstName());
            internalPerson.setLastName(person.getLastName());

            session.save(internalPerson);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void deletePerson(int id) {
        Session session = sessionFactory.openSession();

        Transaction transaction = null;

        try {
            Person internalPerson = findPerson(id);

            transaction = session.beginTransaction();

            session.delete(internalPerson);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Person> getAllPersons() {
        List<Person> result = new ArrayList<Person>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List personList = session.createSQLQuery("SELECT * FROM Persons").addEntity(Person.class).list();
            for (Iterator iterator = personList.iterator(); iterator.hasNext(); ) {
                Person person = (Person) iterator.next();

                result.add(person);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }

    public List<Person> findPersonsByLastName(String lastName) {
        List<Person> result = new ArrayList<Person>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createSQLQuery("SELECT * FROM Persons WHERE Persons.last_name=:name").addEntity(Person.class);
            query.setString("name", lastName);

            List personList = query.list();
            for (Iterator iterator = personList.iterator(); iterator.hasNext(); ) {
                Person person = (Person) iterator.next();

                result.add(person);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }

    private Person findPerson(int personID) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        Person work = (Person) session.get(Person.class, personID);
        session.getTransaction().commit();

        session.close();

        return work;
    }

    @Override
    public void initialize(ComponentContext context) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
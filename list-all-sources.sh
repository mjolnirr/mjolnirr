tree -if --noreport . | grep \\.java | while read filename; do
    echo "\n\n"
    echo $filename
    cat $filename
done

tree -if --noreport . | grep \\.xml | while read filename; do
    echo "\n\n"
    echo $filename
    cat $filename
done

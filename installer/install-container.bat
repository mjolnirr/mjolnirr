set PR_PATH=%CD%
SET PR_SERVICE_NAME=MjolnirrContainerService
SET PR_JAR=container/container.jar
SET START_CLASS=com.mjolnirr.container.Main
SET START_METHOD=main
SET STOP_CLASS=com.mjolnirr.container.WindowsServiceStopper
SET STOP_METHOD=main
rem ; separated values
SET STOP_PARAMS=0
rem ; separated values
SET JVM_OPTIONS=-Dapp.home=%PR_PATH%
lib\mjolnirr-container.exe //IS//%PR_SERVICE_NAME% --Install="%PR_PATH%\lib\mjolnirr-container.exe" --StartPath=%PR_PATH% --Startup=manual --StartMode=Java --StartClass=%START_CLASS% --StartMethod=%START_METHOD% --StopMode=Java --StopClass=%STOP_CLASS% --StopMethod=%STOP_METHOD% ++StopParams=%STOP_PARAMS% --Classpath="%PR_PATH%\%PR_JAR%" --DisplayName="%PR_SERVICE_NAME%" ++JvmOptions=%JVM_OPTIONS% --LogLevel=DEBUG --LogPath="%PR_PATH%\logs" --StdOutput="%PR_PATH%\logs\container-stdout.txt" --StdError="%PR_PATH%\logs\container-stderr.txt"
@echo off

if exist services.txt goto startServices
echo "Installing Mjolnirr services..."

if not "${proxy-installed}" == "" (
    call proxy\install-proxy.bat
)

if not "${container-installed}" == "" (
    call container\install-container.bat
)

:startServices
echo "Starting Mjolnirr services..."

if not "${proxy-installed}" == "" (
    net start MjolnirrProxyService
)

if not "${container-installed}" == "" (
    net start MjolnirrContainerService
)
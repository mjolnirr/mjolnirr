set PR_PATH=%CD%
SET PR_SERVICE_NAME=MjolnirrProxyService
SET PR_JAR=proxy/proxy.jar
SET START_CLASS=com.mjolnirr.proxy.Main
SET START_METHOD=main
SET STOP_CLASS=com.mjolnirr.proxy.WindowsServiceStopper
SET STOP_METHOD=main
rem ; separated values
SET STOP_PARAMS=0
rem ; separated values
SET JVM_OPTIONS=-Dapp.home=%PR_PATH%
lib\mjolnirr-proxy.exe //IS//%PR_SERVICE_NAME% --Install="%PR_PATH%\lib\mjolnirr-proxy.exe" --StartPath=%PR_PATH% --Startup=manual --StartMode=Java --StartClass=%START_CLASS% --StartMethod=%START_METHOD% --StopMode=Java --StopClass=%STOP_CLASS% --StopMethod=%STOP_METHOD% ++StopParams=%STOP_PARAMS% --Classpath="%PR_PATH%\%PR_JAR%" --DisplayName="%PR_SERVICE_NAME%" ++JvmOptions=%JVM_OPTIONS% --LogLevel=DEBUG --LogPath="%PR_PATH%\logs" --StdOutput="%PR_PATH%\logs\stdout.txt" --StdError="%PR_PATH%\logs\stderr.txt"
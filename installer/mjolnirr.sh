#!/bin/bash

echo "******************************"
echo "*"
echo "* Mjolnirr control"
echo "*"
echo "******************************"
echo

#
# components to be started
#
control_proxy="${proxy-installed}"
control_container="${container-installed}"

action="$1";

case $action in
  "start" )
    if [ -e proxy ]
    then if [ "${control_proxy}" = "true" ]
    then
      echo "Starting Mjolnirr proxy..."
      proxy/proxy.sh start ${*:2}
      sleep 5
    fi
    fi

    if [ -e container ]
    then if [ "${control_container}" = "true" ]
    then
      echo "Starting Mjolnirr container..."
      container/container.sh start ${*:2}
    fi
    fi;;
  "status" )
  if [ -e proxy ]
      then if [ "${control_proxy}" = "true" ]
      then
        echo "Mjolnirr proxy status:"
        proxy/proxy.sh status
      fi
      fi

      if [ -e container ]
      then if [ "${control_container}" = "true" ]
      then
        echo "Mjolnirr container status:"
        container/container.sh status
      fi
      fi;;
  "stop" )
  if [ -e proxy ]
      then if [ "${control_proxy}" = "true" ]
      then
        echo "Stopping Mjolnirr proxy..."
        proxy/proxy.sh stop
      fi
      fi

      if [ -e container ]
      then if [ "${control_container}" = "true" ]
      then
        echo "Stopping Mjolnirr container..."
        container/container.sh stop
      fi
      fi;;
  *)
    echo "Unknown option. Usage - mjolnirr.sh ACTION [PARAMS]"
    echo "Available actions:"
    echo "start"
    echo "status"
    echo "stop"
esac
#!/bin/bash

echo "******************************"
echo "*"
echo "* Mjolnirr control"
echo "*"
echo "******************************"
echo

#
# components to be started
#
control_proxy="${proxy-installed}"
control_container="${container-installed}"

if [ -e proxy ]
then if [ "${control_proxy}" = "true" ]
then
  echo "Starting Mjolnirr proxy..."
  proxy/proxy.sh start ${*:2}
  sleep 5
fi
fi

if [ -e container ]
then if [ "${control_container}" = "true" ]
then
  echo "Starting Mjolnirr container..."
  container/container.sh start ${*:2}
fi
fi
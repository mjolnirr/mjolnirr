@echo off


echo "Stopping Mjonirr Services..."

if not "${proxy-installed}" == "" (
    net stop MjolnirrProxyService
)

if not "${container-installed}" == "" (
    net stop MjolnirrContainerService
)

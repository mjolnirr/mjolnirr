#/bin/bash

#
# Startup script for Mjolnirr container
#

#
# Basic properties for UNICORE
#

#
# Java to use
#
JAVA="${JAVA_CMD}"
JAVA=${JAVA:-java}

#
# memory settings
#
MEM="-Xmx128m"

#
# PID file
#
SERVICE="proxy"
PID="PROXY_LAST_PID"

#
# Options to the Java VM
#

# set this one if you have ssl problems and need debug info
#OPTS=$OPTS" -Djavax.net.debug=ssl,handshake"

#
# check whether the server might be already running
#

action="$1";

case $action in
  "start" )
    echo "Starting proxy...";
    if [ -e $PID ]
     then
      if [ -d /proc/$(cat $PID) ]
       then
         echo "A Proxy instance may be already running with process id "$(cat $PID)
         echo "If this is not the case, delete the file $INST/$PID and re-run this script"
         exit 1
       fi
    fi

    nohup ${JAVA} -jar ${MEM} ${OPTS} ${DEFS} proxy/proxy.jar ${*:2} ${PARAM} 2>&1  & echo $! > ${PID}

    echo "Mjolnirr proxy started";;
  "status" )
    if [ ! -e $PID ]
    then
     echo "Proxy not running (no PID file)"
     exit 0
    fi

    PIDV=$(cat $PID)

    if ps axww | grep -v grep | grep $PIDV | grep $SERVICE > /dev/null 2>&1 ; then
     echo "Mjolnirr service ${SERVICE} running with PID ${PIDV}"
     exit 0
    fi

    #else not running, but PID file found
    echo "warn: Mjolnirr service ${SERVICE} not running, but PID file $PID found";;

  "stop" )
    if [ ! -e $PID ]
    then
     echo "No PID file found, proxy probably already stopped."
     exit 0
    fi

    cat $PID | xargs kill -SIGTERM

    #
    # wait for shutdown
    #
    P=$(cat $PID)
    echo "Waiting for proxy to stop..."
    stopped="no"
    until [ "$stopped" = "" ]; do
      stopped=$(ps -p $P | grep $P)
      if [ $? != 0 ] 
      then
        stopped=""
      fi
      sleep 2
    done

    echo "Proxy stopped."

    rm -f $PID;;
  *)
    echo "Unknown option. Usage - mjolnirr.sh ACTION"
    echo "Available actions:"
    echo "start"
    echo "status"
    echo "stop"
esac

printf "\n";
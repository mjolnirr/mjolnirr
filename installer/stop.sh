#!/bin/bash

echo "******************************"
echo "*"
echo "* Mjolnirr control"
echo "*"
echo "******************************"
echo

#
# components to be started
#
control_proxy="${proxy-installed}"
control_container="${container-installed}"

if [ -e proxy ]
then if [ "${control_proxy}" = "true" ]
then
echo "Stopping Mjolnirr proxy..."
proxy/proxy.sh stop
fi
fi

if [ -e container ]
then if [ "${control_container}" = "true" ]
then
echo "Stopping Mjolnirr container..."
container/container.sh stop
fi
fi
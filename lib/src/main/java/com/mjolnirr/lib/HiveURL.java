package com.mjolnirr.lib;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/24/13
 * Time: 1:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class HiveURL {
    public static final int HIVE_DEFAULT_PORT = 8091;

    private String host;
    private int port;
    private String applicationName;
    private String methodName;

    public HiveURL(String url) throws URISyntaxException {
        this(new URI(url));
    }

    public HiveURL(URI requestUrl) throws URISyntaxException {
        requestUrl = requestUrl.normalize();

        String[] pathSegments = requestUrl.getPath().split("/");

        host = requestUrl.getHost();
        port = requestUrl.getPort();
        applicationName = pathSegments[1];

        if (port < 0) {
            port = HIVE_DEFAULT_PORT;
        }

        StringBuilder methodNameBuilder = new StringBuilder();

        for (int i = 2 ; i < pathSegments.length ; i++) {
            methodNameBuilder.append(pathSegments[i]);

            if (i != (pathSegments.length - 1)) {
                methodNameBuilder.append("/");
            }
        }

        methodName = methodNameBuilder.toString();
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public HostIdentifier getHostIdentifier() {
        return new HostIdentifier(host, port);
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getMethodName() {
        return methodName;
    }

    @Override
    public String toString() {
        StringBuilder stringRepresentationBuilder = new StringBuilder();

        stringRepresentationBuilder.append("https://");
        stringRepresentationBuilder.append(host);
        stringRepresentationBuilder.append(":");
        stringRepresentationBuilder.append(port);
        stringRepresentationBuilder.append("/");
        stringRepresentationBuilder.append(applicationName);
        stringRepresentationBuilder.append("/");
        stringRepresentationBuilder.append(methodName);

        return stringRepresentationBuilder.toString();
    }

    public String getApplicationRootURL() {
        StringBuilder stringRepresentationBuilder = new StringBuilder();

        stringRepresentationBuilder.append("https://");
        stringRepresentationBuilder.append(host);
        stringRepresentationBuilder.append(":");
        stringRepresentationBuilder.append(port);
        stringRepresentationBuilder.append("/");
        stringRepresentationBuilder.append(applicationName);
        stringRepresentationBuilder.append("/");

        return stringRepresentationBuilder.toString();
    }
}

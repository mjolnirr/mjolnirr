package com.mjolnirr.lib.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/7/13
 * Time: 1:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class StaticFileCannotBeReadException extends MjolnirrException {
    public StaticFileCannotBeReadException() {

    }

    public StaticFileCannotBeReadException(String message) {
        super(message);
    }

    public StaticFileCannotBeReadException(String message, Exception cause) {
        super(message, cause);
    }

    public StaticFileCannotBeReadException(Exception cause) {
        super(cause);
    }

}

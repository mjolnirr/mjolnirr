package com.mjolnirr.lib.exceptions;

import org.apache.commons.lang.exception.ExceptionUtils;

public class ModuleAccessException extends MjolnirrException {

    private String originalTrace;

    public ModuleAccessException() {

    }

    public ModuleAccessException(String message) {
        super(message);
    }

    public ModuleAccessException(String message, Exception cause) {
        super(message, cause);

        originalTrace = ExceptionUtils.getStackTrace(cause);
    }

    public ModuleAccessException(Exception cause) {
        super(cause);
    }

    public String getOriginalTrace() {
        return originalTrace;
    }

    public void setOriginalTrace(String originalTrace) {
        this.originalTrace = originalTrace;
    }
}

package com.mjolnirr.lib.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/13/13
 * Time: 2:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class WrongSuperclassException extends MjolnirrException {
    public WrongSuperclassException() {

    }

    public WrongSuperclassException(String message) {
        super(message);
    }

    public WrongSuperclassException(String message, Exception cause) {
        super(message, cause);
    }

    public WrongSuperclassException(Exception cause) {
        super(cause);
    }
}

package com.mjolnirr.lib.exceptions;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/7/13
 * Time: 3:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class MjolnirrException extends Exception {
    private static final Map<String, MjolnirrException> hiveExceptions = new HashMap<String, MjolnirrException>();
    static {
        hiveExceptions.put("ApplicationNotFoundException", new ApplicationNotFoundException());
        hiveExceptions.put("ApplicationErrorException", new ApplicationErrorException());
        hiveExceptions.put("StaticFileCannotBeReadException", new StaticFileCannotBeReadException());
        hiveExceptions.put("StaticFileNotFoundException", new StaticFileNotFoundException());
        hiveExceptions.put("BadRequestException", new BadRequestException());
        hiveExceptions.put("ModuleAccessException", new ModuleAccessException());
    }

    public MjolnirrException() {

    }

    public MjolnirrException(String message) {
        super(message);
    }

    public MjolnirrException(String message, Exception cause) {
        super(message, cause);
    }

    public MjolnirrException(Exception cause) {
        super(cause);
    }

    public static MjolnirrException createException(String message) {
        Iterator it = hiveExceptions.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, MjolnirrException> currentPair = (Map.Entry<String, MjolnirrException>)it.next();

            if (message.endsWith(currentPair.getKey())) {
                return currentPair.getValue();
            }
        }

        return null;
    }
}

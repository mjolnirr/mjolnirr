package com.mjolnirr.lib.exceptions;

import org.apache.commons.lang.exception.ExceptionUtils;

public class ApplicationErrorException extends MjolnirrException {

    private String originalTrace;

    public ApplicationErrorException() {

    }

    public ApplicationErrorException(String message) {
        super(message);
    }

    public ApplicationErrorException(String message, Exception cause) {
        super(message, cause);

        originalTrace = ExceptionUtils.getStackTrace(cause);
    }

    public ApplicationErrorException(Exception cause) {
        super(cause);
    }

    public String getOriginalTrace() {
        return originalTrace;
    }

    public void setOriginalTrace(String originalTrace) {
        this.originalTrace = originalTrace;
    }
}

package com.mjolnirr.lib.exceptions;

public class ApplicationNotFoundException extends MjolnirrException {

    public ApplicationNotFoundException() {

    }

    public ApplicationNotFoundException(String message) {
        super(message);
    }

    public ApplicationNotFoundException(String message, Exception cause) {
        super(message, cause);
    }

    public ApplicationNotFoundException(Exception cause) {
        super(cause);
    }

}

package com.mjolnirr.lib.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: Scepion1d
 * Date: 9/26/13
 * Time: 2:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class ManifestParsingException extends Exception {

    public ManifestParsingException() {
        super();
    }

    public ManifestParsingException(String message) {
        super(message);
    }

    public ManifestParsingException(String message, Exception cause) {
        super(message, cause);
    }

    public ManifestParsingException(Exception cause) {
        super(cause);
    }

}

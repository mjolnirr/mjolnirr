package com.mjolnirr.lib.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/7/13
 * Time: 1:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class StaticFileNotFoundException extends MjolnirrException {
    public StaticFileNotFoundException() {

    }

    public StaticFileNotFoundException(String message) {
        super(message);
    }

    public StaticFileNotFoundException(String message, Exception cause) {
        super(message, cause);
    }

    public StaticFileNotFoundException(Exception cause) {
        super(cause);
    }

}

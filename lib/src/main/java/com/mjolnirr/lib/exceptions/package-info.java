 @javax.xml.bind.annotation.XmlSchema (
    xmlns = { 
      @javax.xml.bind.annotation.XmlNs(prefix = "com", namespaceURI="http://exceptions.lib.hive.org"),
      @javax.xml.bind.annotation.XmlNs(prefix = "xsd", namespaceURI="http://www.w3.org/2001/XMLSchema")          
    },
    namespace = "http://exceptions.lib.hive.org",
    elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
    attributeFormDefault = javax.xml.bind.annotation.XmlNsForm.UNQUALIFIED
  )  

package com.mjolnirr.lib.exceptions;

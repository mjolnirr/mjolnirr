package com.mjolnirr.lib.exceptions;

import javax.xml.ws.WebFault;

@WebFault
public class BadRequestException extends MjolnirrException {

    public BadRequestException() {
        super();
    }

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Exception cause) {
        super(message, cause);
    }

    public BadRequestException(Exception cause) {
        super(cause);
    }

}

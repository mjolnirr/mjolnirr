package com.mjolnirr.lib;

import org.restlet.data.MediaType;
import org.restlet.representation.OutputRepresentation;

import java.io.*;

/**
 * Created by sk_ on 1/25/14.
 */
public class DynamicFileRepresentation extends OutputRepresentation {
    private final boolean deleteAfterRead;
    private File file;

    public DynamicFileRepresentation(MediaType mediaType, File file, boolean deleteAfterRead) {
        super(mediaType, file.length());
        this.file = file;
        this.deleteAfterRead = deleteAfterRead;
    }

    @Override
    public void write(OutputStream outputStream) throws IOException {
        byte[] buf = new byte[8192];

        InputStream is = new FileInputStream(file);

        int c = 0;

        while ((c = is.read(buf, 0, buf.length)) > 0) {
            outputStream.write(buf, 0, c);
            outputStream.flush();
        }

        outputStream.close();
        is.close();

        if (deleteAfterRead) {
            file.delete();
        }
    }
}
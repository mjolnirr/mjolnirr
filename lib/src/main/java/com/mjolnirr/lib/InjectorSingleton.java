package com.mjolnirr.lib;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;


public class InjectorSingleton {
    private static Injector injector;

    // todo: fix
    public static <T extends AbstractModule> Injector getInjector(Class<T> module) {
        if (injector == null) {
            try {
                injector = Guice.createInjector(module.newInstance());
            } catch (InstantiationException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (IllegalAccessException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        return injector;
    }
}

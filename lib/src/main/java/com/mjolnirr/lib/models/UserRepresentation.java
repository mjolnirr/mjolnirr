package com.mjolnirr.lib.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

/**
* Created by sk_ on 3/5/14.
*/
@Entity
@Table(name = "users")
public class UserRepresentation {
    @Id
    @Column(precision = 256, scale = 2, name = "serial")
    private BigInteger serial;

    @Expose
    @SerializedName("serial")
    private String serialString;
    @Column(name = "username")
    @Expose
    private String username;
    @Column(name = "role")
    @Expose
    private String role;

    public UserRepresentation() {

    }

    public UserRepresentation(String username, BigInteger serial) {
        this.username = username;
        this.serial = serial;
        this.serialString = serial.toString();
    }

    public UserRepresentation(String username, BigInteger serial, String role) {
        this.username = username;
        this.serial = serial;
        this.role = role;
        this.serialString = serial.toString();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public BigInteger getSerial() {
        return serial;
    }

    public void setSerial(BigInteger serial) {
        this.serial = serial;
    }

    public String getSerialString() {
        return serialString;
    }

    public void setSerialString(String serialString) {
        this.serialString = serialString;
    }

    public boolean isAdmin() {
        return role.equalsIgnoreCase("admin");
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}

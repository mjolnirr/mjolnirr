package com.mjolnirr.lib.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

/**
* Created by sk_ on 3/5/14.
*/
@Entity
@Table(name = "container_certificates")
public class CertificateRepresentation {
    @Id
    @Column(precision = 256, scale = 2, name = "serial")
    private BigInteger serial;

    @Expose
    @SerializedName("serial")
    private String serialString;

    public CertificateRepresentation() {

    }

    public CertificateRepresentation(BigInteger serial) {
        this.serial = serial;
    }

    public BigInteger getSerial() {
        return serial;
    }

    public void setSerial(BigInteger serial) {
        this.serial = serial;
    }

    public String getSerialString() {
        return serialString;
    }

    public void setSerialString(String serialString) {
        this.serialString = serialString;
    }
}

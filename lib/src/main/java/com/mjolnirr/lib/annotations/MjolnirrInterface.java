package com.mjolnirr.lib.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by sk_ on 5/12/14.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MjolnirrInterface {
    public String pageNameWildCard() default "*";
    public String[] allowedUsers() default { "user", "privileged" };
}

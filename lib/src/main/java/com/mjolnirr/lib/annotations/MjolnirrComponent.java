package com.mjolnirr.lib.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by sk_ on 3/12/14.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) //on class level
public @interface MjolnirrComponent {
    String componentName();
    int instancesMinCount() default 1;
    int instancesMaxCount() default 255;
    long memoryVolume() default 256;

    public MjolnirrInterface[] interfaces() default {};
}

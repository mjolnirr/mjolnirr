package com.mjolnirr.lib.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by sk_ on 3/12/14.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MjolnirrMethod {
    int maximumExecutionTime() default 30;
    String[] roles() default { "user", "privileged" };
}

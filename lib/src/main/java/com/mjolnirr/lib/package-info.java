 @javax.xml.bind.annotation.XmlSchema (
    xmlns = { 
      @javax.xml.bind.annotation.XmlNs(prefix = "com", namespaceURI="http://lib.mjolnirr.com"),
      @javax.xml.bind.annotation.XmlNs(prefix = "xsd", namespaceURI="http://www.w3.org/2001/XMLSchema")          
    },
    namespace = "http://lib.mjolnirr.com",
    elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
    attributeFormDefault = javax.xml.bind.annotation.XmlNsForm.UNQUALIFIED
  )  

package com.mjolnirr.lib;

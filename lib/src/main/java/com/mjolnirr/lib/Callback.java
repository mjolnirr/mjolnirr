package com.mjolnirr.lib;

public interface Callback<T> {

    public void run(T result);

}

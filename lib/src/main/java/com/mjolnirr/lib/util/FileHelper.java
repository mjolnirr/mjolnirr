package com.mjolnirr.lib.util;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by sk_ on 3/25/14.
 */
public class FileHelper {
    public static String readFile(File path) throws IOException {
        byte[] encoded = Files.readAllBytes(path.toPath());
        return StandardCharsets.UTF_8.decode(ByteBuffer.wrap(encoded)).toString();
    }
}

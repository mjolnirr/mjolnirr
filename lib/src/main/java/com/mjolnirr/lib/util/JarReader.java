package com.mjolnirr.lib.util;

import com.mjolnirr.lib.assets.AssetsPreprocessors;
import com.mjolnirr.lib.assets.Preprocessor;
import com.mjolnirr.lib.util.StringExtensions;
import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.commons.io.IOUtils;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.exceptions.StaticFileCannotBeReadException;
import com.mjolnirr.lib.exceptions.StaticFileNotFoundException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.activation.DataHandler;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Class to get files and directories from JAR archive
 * */
public class JarReader {
    /**
     * Read static file from JAR archive
     *
     * @param jarLocation Location of the JAR to read
     * @param fileToRead Name of the file to read
     *
     * @return Static file as DataHandler
     *
     * @throws StaticFileNotFoundException
     * @throws StaticFileCannotBeReadException
     * */
    public static DataHandler readFile(String jarLocation, String fileToRead)
            throws
            StaticFileNotFoundException,
            StaticFileCannotBeReadException {
        LoggerFactory.getLogger().info(String.format("Reading file: %s/%s.", jarLocation, fileToRead));
        fileToRead = fileToRead.toLowerCase();

        String extension = StringExtensions.getExtension(fileToRead);
        Preprocessor preprocessor = AssetsPreprocessors.findPreprocessorByExtension(extension);

        //  Trying to find static file
        try {
            ZipFile zipFile = new ZipFile(jarLocation);
            Enumeration<? extends ZipEntry> e = zipFile.entries();

            while (e.hasMoreElements()) {
                ZipEntry entry = e.nextElement();

                if (!entry.isDirectory() && entry.getName().toLowerCase().equals(fileToRead)) {
                    byte[] bytes = IOUtils.toByteArray(zipFile.getInputStream(entry));
                    ByteArrayDataSource rawData= new ByteArrayDataSource(bytes);

                    return new DataHandler(rawData);
                }
            }
        } catch (IOException e) {
            LoggerFactory.getLogger().error(String.format("Failed to read file: %s/%s.", jarLocation, fileToRead), e);

            throw new StaticFileCannotBeReadException();
        }

        //  Looks like there is no such file. So let's look for substitutions
        String templateFile = StringExtensions.replaceLast(fileToRead, extension, preprocessor.getTemplateExtension()).toLowerCase();
        LoggerFactory.getLogger().info(String.format("Static file not found. Looking for template: %s/%s.", jarLocation, templateFile));

        if (preprocessor != null) {
            try {
                ZipFile zipFile = new ZipFile(jarLocation);
                Enumeration<? extends ZipEntry> e = zipFile.entries();

                while (e.hasMoreElements()) {
                    ZipEntry entry = e.nextElement();

                    if (!entry.isDirectory() && entry.getName().toLowerCase().equals(templateFile)) {
                        byte[] bytes = IOUtils.toByteArray(zipFile.getInputStream(entry));

                        return preprocessor.processContent(bytes);
                    }
                }
            } catch (IOException e) {
                LoggerFactory.getLogger().error(String.format("Failed to read file: %s/%s.", jarLocation, fileToRead), e);

                throw new StaticFileCannotBeReadException();
            }
        }

        //  Fires if there is no such file on the whole JAR-archive
        LoggerFactory.getLogger().error(String.format("File not found: %s/%s.", jarLocation, fileToRead));

        throw new StaticFileNotFoundException();
    }

    /**
     * Deploys configs from target JAR to specified location
     *
     * @param appConfigs Location to deploy
     * @param jarLocation JAR file location
     * */
    public static void deployConfigs(File appConfigs, String jarLocation) {
        LoggerFactory.getLogger().info(String.format("Extracting configs from: %s", jarLocation));

        try {
            ZipFile zipFile = new ZipFile(jarLocation);
            Enumeration<? extends ZipEntry> e = zipFile.entries();

            while (e.hasMoreElements()) {
                ZipEntry entry = e.nextElement();

                if (entry.getName().toLowerCase().startsWith("config/")) {
                    String entrySpecificName = entry.getName().replace("config/", "");
                    File entryOutputFile = new File(appConfigs, entrySpecificName);

                    if (entryOutputFile.exists()) {
                        continue;
                    }

                    if (entry.isDirectory()) {
                        entryOutputFile.mkdirs();

                        continue;
                    }

                    final InputStream zipStream = zipFile.getInputStream(entry);

                    IOUtils.copy(zipStream, new FileOutputStream(entryOutputFile));
                }
            }
        } catch (IOException e) {
            LoggerFactory.getLogger().error(String.format("Failed to extract config dir file: %s.", jarLocation), e);
        }
    }

    public static Map<String, File> getFilesFromDirectory(String jarLocation, String folder) {
        LoggerFactory.getLogger().info(String.format("Extracting configs from: %s", jarLocation));

        try {
            Map<String, File> result = new HashMap<String, File>();

            ZipFile zipFile = new ZipFile(jarLocation);
            Enumeration<? extends ZipEntry> e = zipFile.entries();

            while (e.hasMoreElements()) {
                ZipEntry entry = e.nextElement();

                if (entry.getName().toLowerCase().startsWith(folder)) {
                    if (entry.isDirectory()) {
                        continue;
                    }

                    String[] nameSegments = entry.getName().split("/");
                    String[] nameSegments2 = nameSegments[nameSegments.length - 1].split("\\.");

                    File file = File.createTempFile("hive", "");
                    final InputStream zipStream = zipFile.getInputStream(entry);

                    IOUtils.copy(zipStream, new FileOutputStream(file));

                    result.put(nameSegments2[0], file);
                }
            }

            return result;
        } catch (IOException e) {
            LoggerFactory.getLogger().error(String.format("Failed to extract config dir file: %s.", jarLocation), e);
        }

        return null;
    }
}

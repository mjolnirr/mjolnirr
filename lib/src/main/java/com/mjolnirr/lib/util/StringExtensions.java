package com.mjolnirr.lib.util;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 14.11.13
 * Time: 11:09
 * To change this template use File | Settings | File Templates.
 */
public class StringExtensions {
    public static String getExtension(String fileToRead) {
        String[] segments = fileToRead.split("\\.");

        return segments[segments.length - 1];
    }

    public static String replaceLast(String string, String toReplace, String replacement) {
        int pos = string.lastIndexOf(toReplace);
        if (pos > -1) {
            return string.substring(0, pos)
                    + replacement
                    + string.substring(pos + toReplace.length(), string.length());
        } else {
            return string;
        }
    }
}

package com.mjolnirr.lib;

import com.google.gson.*;
import org.hornetq.utils.Base64;

import java.lang.reflect.Type;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 17.10.13
 * Time: 13:51
 * To change this template use File | Settings | File Templates.
 */
public class GsonHelper {
    public static final Gson customGson = new GsonBuilder().registerTypeHierarchyAdapter(byte[].class,
            new ByteArrayToBase64TypeAdapter()).create();

    private static class ByteArrayToBase64TypeAdapter implements JsonSerializer<byte[]>, JsonDeserializer<byte[]> {
        public byte[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return Base64.decode(json.getAsString());
        }

        public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(Base64.encodeBytes(src));
        }
    }
}

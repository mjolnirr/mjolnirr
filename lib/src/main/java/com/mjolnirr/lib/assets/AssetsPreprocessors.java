package com.mjolnirr.lib.assets;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 13.11.13
 * Time: 12:33
 * To change this template use File | Settings | File Templates.
 */
public class AssetsPreprocessors {
    private static final Map<String, Preprocessor> preprocessorsRegister;
    private static final Map<String, Preprocessor> ownExtensionsRegister;
    public static final List<String> staticExtensions;
    public static final List<String> templateExtensions;
    static {
        Map<String, Preprocessor> aMap = new HashMap<String, Preprocessor>();

        aMap.put("xml", new JadePreprocessor());
        aMap.put("fxml", new JadePreprocessor());
        aMap.put("html", new JadePreprocessor());
        aMap.put("css", new ZussPreprocessor());

        preprocessorsRegister = Collections.unmodifiableMap(aMap);

        staticExtensions = new ArrayList<String>();
        templateExtensions = new ArrayList<String>();
        ownExtensionsRegister = new HashMap<String, Preprocessor>();

        for (Map.Entry<String, Preprocessor> entry : preprocessorsRegister.entrySet()) {
            staticExtensions.add(entry.getKey());
            templateExtensions.add(entry.getValue().getTemplateExtension());
            ownExtensionsRegister.put(entry.getValue().getTemplateExtension(), entry.getValue());
        }
    }

    public static Preprocessor findPreprocessorByExtension(String extension) {
        if (preprocessorsRegister.containsKey(extension)) {
            return preprocessorsRegister.get(extension);
        }

        return null;
    }

    public static Preprocessor findPreprocessorByOwnExtension(String extension) {
        if (ownExtensionsRegister.containsKey(extension)) {
            return ownExtensionsRegister.get(extension);
        }

        return null;
    }
}

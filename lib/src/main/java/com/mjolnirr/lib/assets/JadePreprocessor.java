package com.mjolnirr.lib.assets;

import de.neuland.jade4j.Jade4J;

import java.io.IOException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 13.11.13
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */
public class JadePreprocessor extends Preprocessor {
    @Override
    public String getTemplateExtension() {
        return "jade";
    }

    @Override
    public String processContent(String filename) throws IOException {
        return Jade4J.render(filename, null, true);
    }

    @Override
    public String processContent(String filename, Map<String, Object> model) throws IOException {
        return Jade4J.render(filename, model, true);
    }
}

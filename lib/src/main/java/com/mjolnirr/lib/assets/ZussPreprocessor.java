package com.mjolnirr.lib.assets;

import org.zkoss.zuss.Zuss;
import org.zkoss.zuss.metainfo.ZussDefinition;

import javax.activation.DataHandler;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 14.11.13
 * Time: 10:17
 * To change this template use File | Settings | File Templates.
 */
public class ZussPreprocessor extends Preprocessor {
    @Override
    public String getTemplateExtension() {
        return "zuss";
    }

    @Override
    public String processContent(String filename) throws IOException {
        ZussDefinition definition = Zuss.parse(new File(filename), null);

        ByteArrayOutputStream stylesheetStream = new ByteArrayOutputStream();

        Zuss.translate(definition, stylesheetStream, null, null);

        return new String(stylesheetStream.toByteArray());
    }

    @Override
    public String processContent(String filename, Map<String, Object> model) throws IOException {
        return processContent(filename);
    }
}

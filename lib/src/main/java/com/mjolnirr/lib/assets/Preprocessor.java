package com.mjolnirr.lib.assets;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import javax.activation.DataHandler;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 13.11.13
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
public abstract class Preprocessor {
    public abstract String getTemplateExtension();

    public abstract String processContent(String filename) throws IOException;

    public abstract String processContent(String filename, Map<String, Object> model) throws IOException;

    public DataHandler processContent(byte[] rawData) throws IOException {
        File temp = File.createTempFile("tempfile", ".tmp");

        BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(temp));
        bw.write(rawData);
        bw.close();

        ByteArrayDataSource rawDataSource = new ByteArrayDataSource(processContent(temp.getAbsolutePath()).getBytes());

        return new DataHandler(rawDataSource);
    }

    public DataHandler processContent(byte[] rawData, Map<String, Object> model) throws IOException {
        File temp = File.createTempFile("tempfile", ".tmp");

        BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(temp));
        bw.write(rawData);
        bw.close();

        ByteArrayDataSource rawDataSource = new ByteArrayDataSource(processContent(temp.getAbsolutePath(), model).getBytes());

        return new DataHandler(rawDataSource);
    }

    public void processContent(String absolutePath, File outputFile) throws IOException {
        FileUtils.writeStringToFile(outputFile, processContent(absolutePath));
    }
}

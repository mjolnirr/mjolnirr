package com.mjolnirr.lib;

import com.google.gson.Gson;
import com.mjolnirr.lib.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 12.11.13
 * Time: 10:16
 * To change this template use File | Settings | File Templates.
 */
public class ApplicationInstanceManager {
    private static ServerSocket socket;
    private static Thread instanceListenerThread;

    private static ApplicationInstanceListener subListener;

    /**
     * Randomly chosen, but static, high socket number
     */
    public static final int SINGLE_INSTANCE_NETWORK_SOCKET = 45893;

    /**
     * Must end with newline
     */
    public static final String SINGLE_INSTANCE_SHARED_KEY = "$$NewInstance$$\n";

    /**
     * Registers this instance of the application.
     *
     * @return true if first instance, false if not.
     * @param args
     */
    public static boolean registerInstance(String[] args) {
        return registerInstance(args, SINGLE_INSTANCE_NETWORK_SOCKET);
    }

    /**
     * Registers this instance of the application.
     *
     * @return true if first instance, false if not.
     * @param args
     */
    public static boolean registerInstance(String[] args, int port) {
        // returnValueOnError should be true if lenient (allows app to run on network error) or false if strict.
        boolean returnValueOnError = true;
        // try to open network socket
        // if success, listen to socket for new instance message, return true
        // if unable to open, connect to existing and send new instance message, return false
        try {
            socket = new ServerSocket(port, 10, InetAddress.getByAddress(new byte[] { 127, 0, 0, 1 }));
            System.out.println("Listening for application instances on socket " + port);
            instanceListenerThread = new Thread(new Runnable() {
                public void run() {
                    boolean socketClosed = false;
                    while (!socketClosed) {
                        if (socket.isClosed()) {
                            socketClosed = true;
                        } else {
                            try {
                                Socket client = socket.accept();
                                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));

                                String instanceKey = in.readLine();
                                if (SINGLE_INSTANCE_SHARED_KEY.trim().equals(instanceKey.trim())) {
                                    System.out.println("Shared key matched - new application instance found");

                                    String[] argsPassed = new Gson().fromJson(in.readLine(), String[].class);
                                    fireNewInstance(argsPassed);
                                }
                                in.close();
                                client.close();
                            } catch (IOException e) {
                                socketClosed = true;
                            }
                        }
                    }
                }
            });
            instanceListenerThread.start();
            // listen
        } catch (UnknownHostException e) {
            e.printStackTrace();

            return returnValueOnError;
        } catch (IOException e) {
            System.out.println("Port is already taken.  Notifying first instance.");
            try {
                Socket clientSocket = new Socket(InetAddress.getByAddress(new byte[]{127, 0, 0, 1}), port);
                OutputStream out = clientSocket.getOutputStream();
                out.write(SINGLE_INSTANCE_SHARED_KEY.getBytes());
                out.write(new Gson().toJson(args).getBytes());
                out.write("\n".getBytes());
                out.close();
                clientSocket.close();
                System.out.println("Successfully notified first instance.");
                return false;
            } catch (UnknownHostException e1) {
                e1.printStackTrace();

                return returnValueOnError;
            } catch (IOException e1) {
                System.out.println("Error connecting to local port for single instance notification");
                e1.printStackTrace();

                return returnValueOnError;
            }

        }
        return true;
    }

    public static void closeInstance() {
        if (socket != null) {
            try {
                socket.close();
                instanceListenerThread.stop();
            } catch (IOException e) {
                System.out.println("Error while closing the socket");
            }
        }
    }

    public static void setApplicationInstanceListener(ApplicationInstanceListener listener) {
        subListener = listener;
    }

    private static void fireNewInstance(String[] argsPassed) {
        if (subListener != null) {
            subListener.newInstanceCreated(argsPassed);
        }
    }

    public interface ApplicationInstanceListener {
        public void newInstanceCreated(String[] argsPassed);
    }
}
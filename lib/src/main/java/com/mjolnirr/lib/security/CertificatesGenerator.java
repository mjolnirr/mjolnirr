package com.mjolnirr.lib.security;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.mjolnirr.lib.LoggerFactory;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.jce.provider.X509CertificateObject;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

import java.io.*;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sk_ on 2/16/14.
 */
public class CertificatesGenerator {
    public static final String SERVER_ALIAS = "Server keypair";
    private final KeyStore keystore;
    private final KeyStore truststore;
    private final String keystoreLocation;
    private final String keystorePassword;
    private final String truststoreLocation;
    private final String truststorePassword;
    private Certificate serverCertificate;

    /**
     * This holds the certificate of the CA used to sign the new certificate. The object is created in the constructor.
     */
    private X509Certificate caCert;
    /**
     * This holds the private key of the CA used to sign the new certificate. The object is created in the constructor.
     */
    private RSAPrivateCrtKeyParameters caPrivateKey;

    private boolean useBCAPI;

    public CertificatesGenerator(String keystoreLocation, String keystorePassword, String truststoreLocation, String truststorePassword) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        this.keystoreLocation = keystoreLocation;
        this.keystorePassword = keystorePassword;
        this.truststoreLocation = truststoreLocation;
        this.truststorePassword = truststorePassword;

        keystore = initKeystore(keystoreLocation, keystorePassword);
        truststore = initKeystore(truststoreLocation, truststorePassword);

        serverCertificate = keystore.getCertificate(SERVER_ALIAS);
    }

    private KeyStore initKeystore(String keystoreLocation, String keystorePassword) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        KeyStore ks = KeyStore.getInstance("JKS");

        char[] password = keystorePassword.toCharArray();

        File keystoreFile = new File(keystoreLocation);
        FileInputStream fis = null;
        try {
            if (keystoreFile.exists()) {
                fis = new FileInputStream(keystoreFile);
                ks.load(fis, password);
            } else {
                ks.load(null, null);
            }
        } finally {
            if (fis != null) {
                fis.close();
            }
        }

        return ks;
    }

    private void addCertificateToTruststore(X509Certificate trustedCertificate, String alias) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        KeyStore.Entry newEntry = new KeyStore.TrustedCertificateEntry(trustedCertificate);
        truststore.setEntry(alias, newEntry, null);

        saveTruststore();
    }

    private void saveKeystore() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        saveKeystore(keystore, keystoreLocation, keystorePassword);
    }

    private void saveTruststore() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        saveKeystore(truststore, truststoreLocation, truststorePassword);
    }

    private void saveKeystore(KeyStore ks, String keystoreLocation, String keystorePassword) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException {
        FileOutputStream fOut = new FileOutputStream(keystoreLocation);

        ks.store(fOut, keystorePassword.toCharArray());
    }

    public void generateServerKeystores(String dn, String keystorePassword, String truststorePassword) throws KeyStoreException, NoSuchAlgorithmException, IOException, CertificateException, SignatureException, InvalidKeyException {
        SecureRandom sr = new SecureRandom();

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024, sr);
        KeyPair keypair = keyGen.generateKeyPair();
        PrivateKey privKey2 = keypair.getPrivate();
        PublicKey pubKey = keypair.getPublic();

        Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.DAY_OF_YEAR, 365);

        X509Name x509Name = new X509Name(dn);

        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
        certGen.setSerialNumber(new BigInteger(128, new SecureRandom()));
        certGen.setIssuerDN(x509Name);
        certGen.setSubjectDN(x509Name);
        certGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
        certGen.setPublicKey(pubKey);
        certGen.setNotBefore(new Date(System.currentTimeMillis()));
        certGen.setNotAfter(expiry.getTime());

        X509Certificate serverCertificate = certGen.generateX509Certificate(privKey2);

        keystore.setKeyEntry(SERVER_ALIAS, privKey2, keystorePassword.toCharArray(), new Certificate[]{serverCertificate});
        this.serverCertificate = serverCertificate;

        addCertificateToTruststore(serverCertificate, "server own certificate");

        saveKeystore();
        saveTruststore();
    }

    public byte[] createClientCertificate(
            String caFile,
            String caPassword,
            String caAlias,
            String username,
            boolean useBCAPI,
            String dn,
            int validityDays,
            String exportPassword,
            String keystoreType,
            BigInteger serial) throws
            IOException, InvalidKeyException, SecurityException, SignatureException, NoSuchAlgorithmException, DataLengthException, CryptoException, KeyStoreException, NoSuchProviderException, CertificateException, InvalidKeySpecException, UnrecoverableKeyException {
        this.useBCAPI = useBCAPI;

        System.out.println("Loading CA certificate and private key from file '" + caFile + "', using alias '" + caAlias + "' with "
                + (this.useBCAPI ? "Bouncycastle lightweight API" : "JCE API"));

        // load the key entry from the keystore
        Key key = keystore.getKey(caAlias, caPassword.toCharArray());
        if (key == null) {
            throw new RuntimeException("Got null key from keystore!");
        }
        RSAPrivateCrtKey privKey = (RSAPrivateCrtKey) key;
        caPrivateKey = new RSAPrivateCrtKeyParameters(privKey.getModulus(), privKey.getPublicExponent(), privKey.getPrivateExponent(),
                privKey.getPrimeP(), privKey.getPrimeQ(), privKey.getPrimeExponentP(), privKey.getPrimeExponentQ(), privKey.getCrtCoefficient());
        // and get the certificate
        caCert = (X509Certificate) keystore.getCertificate(caAlias);
        if (caCert == null) {
            throw new RuntimeException("Got null cert from keystore!");
        }
        System.out.println("Successfully loaded CA key and certificate. CA DN is '" + caCert.getSubjectDN().getName() + "'");
        caCert.verify(caCert.getPublicKey());
        System.out.println("Successfully verified CA certificate with its own public key.");

        LoggerFactory.getLogger().info("Generating certificate for distinguished subject name '" +
                dn + "', valid for " + validityDays + " days");
        SecureRandom sr = new SecureRandom();

        PublicKey pubKey;
        PrivateKey privKey2;

        LoggerFactory.getLogger().info("Creating RSA keypair");

        // generate the keypair for the new certificate
        // this is the JSSE way of key generation
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024, sr);
        KeyPair keypair = keyGen.generateKeyPair();
        privKey2 = keypair.getPrivate();
        pubKey = keypair.getPublic();

        Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.DAY_OF_YEAR, validityDays);

        X509Name x509Name = new X509Name("CN=" + dn + "|user-" + username);

        V3TBSCertificateGenerator certGen = new V3TBSCertificateGenerator();
        certGen.setSerialNumber(new DERInteger(serial));
        certGen.setIssuer(PrincipalUtil.getSubjectX509Principal(caCert));
        certGen.setSubject(x509Name);
        DERObjectIdentifier sigOID = X509Util.getAlgorithmOID("SHA1WithRSAEncryption");
        AlgorithmIdentifier sigAlgId = new AlgorithmIdentifier(sigOID, new DERNull());
        certGen.setSignature(sigAlgId);
        certGen.setSubjectPublicKeyInfo(new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(
                new ByteArrayInputStream(pubKey.getEncoded())).readObject()));
        certGen.setStartDate(new Time(new Date(System.currentTimeMillis())));
        certGen.setEndDate(new Time(expiry.getTime()));

        // attention: hard coded to be SHA1+RSA!
        SHA1Digest digester = new SHA1Digest();
        AsymmetricBlockCipher rsa = new PKCS1Encoding(new RSAEngine());
        TBSCertificateStructure tbsCert = certGen.generateTBSCertificate();

        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        DEROutputStream dOut = new DEROutputStream(bOut);
        dOut.writeObject(tbsCert);

        // and now sign
        byte[] signature;
        // or the JCE way
        PrivateKey caPrivKey = KeyFactory.getInstance("RSA").generatePrivate(
                new RSAPrivateCrtKeySpec(caPrivateKey.getModulus(), caPrivateKey.getPublicExponent(),
                        caPrivateKey.getExponent(), caPrivateKey.getP(), caPrivateKey.getQ(),
                        caPrivateKey.getDP(), caPrivateKey.getDQ(), caPrivateKey.getQInv()));

        Signature sig = Signature.getInstance(sigOID.getId());
        sig.initSign(caPrivKey, sr);
        sig.update(bOut.toByteArray());
        signature = sig.sign();

        // and finally construct the certificate structure
        ASN1EncodableVector v = new ASN1EncodableVector();

        v.add(tbsCert);
        v.add(sigAlgId);
        v.add(new DERBitString(signature));

        X509CertificateObject clientCert = new X509CertificateObject(new X509CertificateStructure(new DERSequence(v)));

        clientCert.verify(caCert.getPublicKey());

        PKCS12BagAttributeCarrier bagCert = clientCert;
        bagCert.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
                new DERBMPString("Mjolnirr key"));
        bagCert.setBagAttribute(
                PKCSObjectIdentifiers.pkcs_9_at_localKeyId,
                new SubjectKeyIdentifierStructure(pubKey));

        KeyStore store = KeyStore.getInstance(keystoreType);

        store.load(null, null);

        X509Certificate[] chain = new X509Certificate[2];
        // first the client, then the CA certificate
        chain[0] = clientCert;
        chain[1] = caCert;

        store.setKeyEntry("Mjolnirr key, user " + username, privKey2, exportPassword.toCharArray(), chain);

        if (keystoreType.equalsIgnoreCase("jks")) {
            KeyStore.Entry newEntry = new KeyStore.TrustedCertificateEntry(serverCertificate);
            store.setEntry(SERVER_ALIAS, newEntry, null);
        }

        File tempCertFile = File.createTempFile("hive", "s");
        FileOutputStream fOut = new FileOutputStream(tempCertFile);

        store.store(fOut, exportPassword.toCharArray());

        byte[] encoded = Files.readAllBytes(tempCertFile.toPath());
        tempCertFile.delete();

        return encoded;
    }
}

package com.mjolnirr.lib.manifest;

import com.google.gson.annotations.Expose;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xeustechnologies.jcl.JarClassLoader;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/1/13
 * Time: 2:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class MjolnirrMethod {
    @Expose     private String returnClassName;
    @Expose     private String name;
    @Expose     private List<MjolnirrMethodParameter> parameters;
    @Expose     private int maxExecutionTime;

    private Map<String, Class> primitiveTypes = new HashMap<String, Class>() {{
        put("byte", Byte.TYPE);
        put("short", Short.TYPE);
        put("int", Integer.TYPE);
        put("long", Long.TYPE);
        put("float", Float.TYPE);
        put("double", Double.TYPE);
        put("boolean", Boolean.TYPE);
        put("char", Character.TYPE);
    }};

    public MjolnirrMethod(String name, Class type, Class[] params, com.mjolnirr.lib.annotations.MjolnirrMethod methodAnnotation) {
        this.name = name;
        this.returnClassName = type.getCanonicalName();

        maxExecutionTime = methodAnnotation.maximumExecutionTime();

        parameters = new ArrayList<MjolnirrMethodParameter>();

        for (Class param : params) {
            parameters.add(new MjolnirrMethodParameter(param));
        }
    }

    public String getName() {
        return name;
    }

    public Method getReflectionMethod(JarClassLoader jcl, Object obj) throws NoSuchMethodException, ClassNotFoundException {
        Class[] argumentClasses = new Class[parameters.size()];

        for (int i = 0 ; i < argumentClasses.length ; i++) {
            MjolnirrMethodParameter param = parameters.get(i);

            if (primitiveTypes.containsKey(param.getType())) {
                argumentClasses[i] = primitiveTypes.get(param.getType());
            } else {
                try {
                    argumentClasses[i] = jcl.loadClass(param.getType());
                } catch (ClassNotFoundException e) {
                    argumentClasses[i] = Class.forName(param.getType());
                }
            }
        }

        return obj.getClass().getMethod(name, argumentClasses);
    }

    public String getReturnClassName() {
        return returnClassName;
    }

    public List<MjolnirrMethodParameter> getParameters() {
        return parameters;
    }

    public int getMaxExecutionTime() {
        return maxExecutionTime;
    }

    public void setMaxExecutionTime(int maxExecutionTime) {
        this.maxExecutionTime = maxExecutionTime;
    }
}

package com.mjolnirr.lib.manifest;

import com.google.gson.annotations.Expose;
import org.w3c.dom.Element;

public class MjolnirrMethodParameter {
    @Expose
    private String type;

    public MjolnirrMethodParameter(Class type) {
        this.type = type.getCanonicalName();
    }

    public String getType() {
        return type;
    }
}

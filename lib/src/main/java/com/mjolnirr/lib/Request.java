package com.mjolnirr.lib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.mjolnirr.lib.exceptions.MjolnirrException;
import com.mjolnirr.lib.manifest.MjolnirrMethod;
import com.mjolnirr.lib.manifest.MjolnirrMethodParameter;
import com.mjolnirr.lib.models.UserRepresentation;
import org.xeustechnologies.jcl.JarClassLoader;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Class for client requests to applications
 */
@XmlRootElement
public class Request {

    /**
     * Name of the target class
     */
    @Expose
    private String applicationName;

    /**
     * Name of the target method
     */
    @Expose
    private String methodName;

    /**
     * The address of the application which sent this request
     */
    @Expose
    private String source;

    /**
     * Callback for response on this request
     */
    @Expose
    private String onResponse;

    @Expose
    private boolean sync;

    /**
     * List of JSON-serialized arguments of the target method
     */
    @Expose
    @XmlElement(required = true)
    private List<String> args;

    /**
     * JSON serializer context
     */
    private Gson gson;

    @Expose
    private String token;

    @Expose
    private UserRepresentation currentUser;

    public Request() {
        applicationName = null;
        methodName = null;
        args = null;
        gson = new Gson();
    }

    /**
     * @param applicationName Name of the target class
     * @param methodName      Name of the target methods
     * @param args            List of arguments of the target method
     * @param source          private channel of application
     */
    public Request(String applicationName, String methodName, List<Object> args, String source, String onResponse) throws IOException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        this.applicationName = applicationName;
        this.methodName = methodName;
        this.source = source;
        this.onResponse = onResponse;
        this.gson = new Gson();
        this.args = new ArrayList<String>();

        if (args == null) {
            return;
        }
        for (Object obj : args) {
            if (obj instanceof File) {
                this.args.add(gson.toJson(TypeHelper.fromFile(obj)));
            } else if (obj instanceof List) {
                List list = (List) obj;
                List<String> results = new ArrayList<String>();

                if ((list.size() > 0) && (list.get(0) instanceof File)) {
                    System.out.println("List of files");

                    for (Object item : list) {
                        results.add(TypeHelper.fromFile(item));
                    }
                    this.args.add(gson.toJson(results));
                } else {
                    this.args.add(gson.toJson(obj));
                }
            } else {
                this.args.add(gson.toJson(obj));
            }
        }
    }

    /**
     * @param applicationName Name of the target class
     * @param methodName      Name of the target methods
     * @param args            List of arguments of the target method
     */
    public Request(String applicationName, String methodName, List<Object> args) throws IOException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        this.applicationName = applicationName;
        this.methodName = methodName;
        this.gson = new Gson();
        this.args = new ArrayList<String>();

        if (args == null) {
            return;
        }
        for (Object obj : args) {
            if (obj instanceof File) {
                this.args.add(gson.toJson(TypeHelper.fromFile(obj)));
            } else {
                this.args.add(gson.toJson(obj));
            }
        }
    }

    public Request(Request other, String source, String onResponse) {
        this.applicationName = other.applicationName;
        this.methodName = other.methodName;
        this.args = other.args;
        this.token = other.token;
        this.currentUser = other.currentUser;
        this.source = source;
        this.onResponse = onResponse;
        this.gson = new Gson();
    }

    /**
     * Check request validity
     *
     * @return True if request is valid
     */
    public boolean isValid() {
        return applicationName != null && !applicationName.isEmpty() &&
                methodName != null && !methodName.isEmpty() &&
                source != null && !source.isEmpty() &&
                args != null;
    }

    public UserRepresentation getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserRepresentation currentUser) {
        this.currentUser = currentUser;
    }

    public static String generateUniqueKey() {
        UUID uniqueKey = UUID.randomUUID();

        return uniqueKey.toString();
    }

    /**
     * Get an array of types of arguments of the target method
     *
     * @return An array of types
     */
    public Class[] getArgsTypes() {
        ArrayList<Class> argsTypes = new ArrayList<Class>();

        for (Object arg : args) {
            argsTypes.add(arg.getClass());
        }

        return argsTypes.toArray(new Class[argsTypes.size()]);
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<Object> getArgs(JarClassLoader jcl, MjolnirrMethod method) throws ClassNotFoundException, IOException, MjolnirrException {
        List<Object> results = new ArrayList<Object>();
        List<MjolnirrMethodParameter> parameters = method.getParameters();

        for (int i = 0; i < parameters.size(); i++) {
            String parameterType = parameters.get(i).getType();
        }

        for (int i = 0; i < parameters.size(); i++) {
            String parameterType = parameters.get(i).getType();

            if (TypeHelper.isPrimitive(parameterType) || TypeHelper.isBoxed(parameterType)) {
                //  Parse as primitive
                results.add(TypeHelper.parsePrimitive(args.get(i), parameterType));
            } else if (TypeHelper.isFile(parameterType)) {
                //  Put bytes to file
                results.add(TypeHelper.toFile(gson.fromJson(args.get(i), String.class)));
            } else {
                //  Parse as class
                Object result = gson.fromJson(args.get(i), jcl.loadClass(parameterType));

                if (result instanceof List) {
                    List list = (List) result;
                    for (int j = 0 ; j < list.size() ; j++) {
                        if (TypeHelper.isTransmittedFile((String) list.get(j))) {
                            list.set(j, TypeHelper.toFile((String) list.get(j)));
                        }
                    }
                }

                results.add(result);
            }
        }

        return results;
    }

    public List<String> getArgs() {
        return args;
    }

    public void setArgs(List<Object> args) {
        if (args == null) {
            this.args = new ArrayList<String>();
            return;
        }

        this.args = new ArrayList<String>();

        for (Object obj : args) {
            this.args.add(gson.toJson(obj));
        }
    }

    public void addArg(Object arg) {
        if (args == null) {
            args = new ArrayList<String>();
        }

        args.add(gson.toJson(arg));
    }

    public void addArgs(List<Object> args) {
        if (this.args == null) {
            this.args = new ArrayList<String>();
        }

        for (Object obj : args) {
            this.args.add(gson.toJson(obj));
        }
    }

    /**
     * Convert this object into Json string
     *
     * @return This object in json format
     */
    @Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return gson.toJson(this);
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOnResponse() {
        return onResponse;
    }

    public Request setToken(String token) {
        this.token = token;

        return this;
    }

    public String getToken() {
        return token;
    }
}

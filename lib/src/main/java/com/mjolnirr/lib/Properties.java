package com.mjolnirr.lib;

import com.beust.jcommander.Parameter;
import com.mjolnirr.lib.properties.ContainerPropertiesModel;
import com.mjolnirr.lib.properties.ContainerPropertiesReaderImpl;
import com.mjolnirr.lib.properties.PropertiesReader;

import java.io.File;
import java.util.UUID;

public class Properties {
    @Parameter(names = {"--host", "-h"}, description = "Container host")
    public static String HOST_NAME;

    @Parameter(names = {"--port", "-p"}, description = "Container port")
    public static Integer PORT;

    @Parameter(names = {"--queue-config", "-q"}, description = "Hornet config location")
    public static File HORNET_CONFIG_FILE;
    private static String HORNET_CONFIG;

    @Parameter(names = {"--name", "-n"}, description = "Container name")
    public static String CONTAINER_NAME;

    @Parameter(names = {"--jar", "-j"}, description = "Folder with component JARs")
    public static String JAR_DIR;

    @Parameter(names = {"--index-page", "-i"}, description = "Main page name")
    public static String MAIN_PAGE;

    @Parameter(names = {"--debug", "-d"}, description = "Debug mode")
    public static Boolean IS_DEBUG_MODE;

    @Parameter(names = {"--mode", "-m"}, description = "Mode")
    public static String MODE;

    @Parameter(names = {"--config", "-c"}, description = "Config directory")
    public static String CONFIG_DIR;

    @Parameter(names = {"--workspace", "-w"}, description = "Workspace directory")
    public static String WORKSPACE_DIR;

    @Parameter(names = {"--proxy-web-host"}, description = "Proxy host")
    public static String PROXY_WEB_HOST;
    @Parameter(names = {"--proxy-web-port"}, description = "Proxy web port")
    public static int PROXY_WEB_PORT;
    @Parameter(names = {"--proxy-queue-port"}, description = "Proxy queue port")
    public static int PROXY_QUEUE_PORT;
    @Parameter(names = {"--memory-quota", "-mq"}, description = "Memory quota for the container")
    public static double MEMORY_QUOTA;

    public static String CONTAINER_UUID;

    static {
        PropertiesReader properties = new ContainerPropertiesReaderImpl();

        if (properties.getConfig() != null) {
            HOST_NAME = properties.getConfig().getString("host", "localhost");
            PORT = properties.getConfig().getInt("port", 8080);
            HORNET_CONFIG = properties.getConfig().getString("hornet_config", "");
            JAR_DIR = properties.getConfig().getString("jar_dir", null);
            MAIN_PAGE = properties.getConfig().getString("default_page", "index");
            IS_DEBUG_MODE = properties.getConfig().getBoolean("debug", false);
            MODE = "production";
            CONFIG_DIR = properties.getConfig().getString("config_dir");
            WORKSPACE_DIR = properties.getConfig().getString("workspace_dir");

            PROXY_WEB_HOST = properties.getConfig().getString("proxy_web_host");
            PROXY_WEB_PORT = properties.getConfig().getInt("proxy_web_port");
            PROXY_QUEUE_PORT = properties.getConfig().getInt("proxy_queue_port");

            CONTAINER_NAME = properties.getConfig().getString("container_name", "");
            MEMORY_QUOTA = properties.getConfig().getDouble("memory_quota", 0.5);
        }

        CONTAINER_UUID = UUID.randomUUID().toString();
    }

    public static String getHornetConfigPath() {
        if (HORNET_CONFIG_FILE == null) {
            if ((HORNET_CONFIG == null) || (HORNET_CONFIG.equals(""))) {
                return "hornetq-conf.xml";
            } else {
                return new File(HORNET_CONFIG).toURI().toString();
            }
        } else {
            return HORNET_CONFIG_FILE.toURI().toString();
        }
    }

    public static boolean isDevelopmentMode() {
        return MODE.toLowerCase().startsWith("dev");
    }

    public static String privateChannelName(String componentName) {
        return String.format("%s/%s", CONTAINER_UUID, componentName);
    }

    public static ContainerPropertiesModel getModel() {
        return new ContainerPropertiesModel(HOST_NAME,
                PORT,
                JAR_DIR,
                MAIN_PAGE,
                CONTAINER_NAME,
                IS_DEBUG_MODE,
                MODE,
                CONFIG_DIR,
                WORKSPACE_DIR,

                PROXY_WEB_HOST,
                PROXY_WEB_PORT,
                PROXY_QUEUE_PORT,

                MEMORY_QUOTA);
    }
}

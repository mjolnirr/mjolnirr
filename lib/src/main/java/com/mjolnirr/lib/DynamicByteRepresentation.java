package com.mjolnirr.lib;

import org.restlet.data.MediaType;
import org.restlet.representation.OutputRepresentation;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by sk_ on 1/25/14.
 */
public class DynamicByteRepresentation extends OutputRepresentation {

    private byte[] fileData;

    public DynamicByteRepresentation(MediaType mediaType, long expectedSize, byte[] fileData) {
        super(mediaType, expectedSize);
        this.fileData = fileData;
    }

    @Override
    public void write(OutputStream outputStream) throws IOException {
        outputStream.write(fileData);
    }
}
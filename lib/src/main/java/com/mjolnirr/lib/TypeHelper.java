package com.mjolnirr.lib;

import com.mjolnirr.lib.exceptions.MjolnirrException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/25/13
 * Time: 1:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class TypeHelper {
    public static String PROXY_HOST;
    public static int PROXY_PORT;
    public static KeyStore keystore;

    private static Map<String, Class> primitiveTypes = new HashMap<String, Class>() {{
        put("byte", Byte.TYPE);
        put("short", Short.TYPE);
        put("int", Integer.TYPE);
        put("long", Long.TYPE);
        put("float", Float.TYPE);
        put("double", Double.TYPE);
        put("boolean", Boolean.TYPE);
        put("char", Character.TYPE);
        put("void", Void.TYPE);
    }};
    private static Map<String, Class> boxTypes = new HashMap<String, Class>() {{
        put("java.lang.Byte", Byte.TYPE);
        put("java.lang.Short", Short.TYPE);
        put("java.lang.Integer", Integer.TYPE);
        put("java.lang.Long", Long.TYPE);
        put("java.lang.Float", Float.TYPE);
        put("java.lang.Double", Double.TYPE);
        put("java.lang.Boolean", Boolean.TYPE);
        put("java.lang.Character", Character.TYPE);
        put("java.lang.Void", Void.TYPE);
    }};

    public static boolean isPrimitive(String typeName) {
        return primitiveTypes.containsKey(typeName);
    }

    public static boolean isBoxed(String typeName) {
        return boxTypes.containsKey(typeName);
    }

    public static Object parsePrimitive(String value, String type) {
        if (type.equals("byte") || type.equals("java.lang.Byte")) {
            return Byte.valueOf(value);
        } else if (type.equals("short") || type.equals("java.lang.Short")) {
            return Short.valueOf(value);
        } else if (type.equals("int") || type.equals("java.lang.Integer")) {
            return Integer.valueOf(value);
        } else if (type.equals("long") || type.equals("java.lang.Long")) {
            return Long.valueOf(value);
        } else if (type.equals("float") || type.equals("java.lang.Float")) {
            return Float.valueOf(value);
        } else if (type.equals("double") || type.equals("java.lang.Double")) {
            return Double.valueOf(value);
        } else if (type.equals("boolean") || type.equals("java.lang.Boolean")) {
            return Boolean.valueOf(value);
        } else if (type.equals("char") || type.equals("java.lang.Character")) {
            return Character.valueOf(value.charAt(0));
        }

        return null;
    }

    public static boolean isFile(String parameterType) {
        return parameterType.contains("java.io.File");
    }

    public static boolean isTransmittedFile(String fileIdentifier) {
        System.out.println("Check is it is transmitted file " + fileIdentifier);

        try {
            URL url = new URL("https://" + PROXY_HOST + ":" + PROXY_PORT + "/tmp/" + fileIdentifier);
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
            con.setRequestMethod("HEAD");

            System.out.println(con.getResponseCode() == HttpURLConnection.HTTP_OK);

            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static File toFile(String fileIdentifier, String extension) throws IOException, MjolnirrException {
        //  Download file from proxy and return it

        URL url = null;
        try {
            url = new URL("https://" + PROXY_HOST + ":" + PROXY_PORT + "/tmp/" + fileIdentifier);

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });

            InputStream inputStream = conn.getInputStream();  // throws an IOException

            File result;
            if (extension != null) {
                result = File.createTempFile("hive", "." + extension);
            } else {
                result = File.createTempFile("hive", "");
            }

            FileOutputStream resultStream = new FileOutputStream(result);
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                resultStream.write(bytes, 0, read);
            }
            inputStream.close();
            resultStream.close();

            return result;
        } catch (Exception e) {
            LoggerFactory.getLogger().fatal("Failed to download resource", e);

            throw MjolnirrException.createException(e.getMessage());
        }
    }

    public static File toFile(String fileIdentifier) throws IOException, MjolnirrException {
        return toFile(fileIdentifier, null);
    }

    public static String fromFile(Object responseObject) throws IOException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        //  Upload file to proxy and return identifier
        File binaryFile = (File) responseObject;

        String charset = "UTF-8";
        String url = "https://" + PROXY_HOST + ":" + PROXY_PORT + "/tmp";

        String boundary = Long.toHexString(System.currentTimeMillis()); // Just
// generate some unique random value.
        String CRLF = "\r\n"; // Line separator required by multipart/form-data.
        HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
        connection.setDoOutput(true);
        connection.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        });
        connection.setRequestProperty("Content-Type",
                "multipart/form-data; boundary=" + boundary);
        PrintWriter writer = null;
        try {
            OutputStream output = connection.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(output, charset),
                    true); // true = autoFlush, important!
            // Send binary file.
            writer.append("--" + boundary).append(CRLF);
            writer.append(
                    "Content-Disposition: form-data; name=\"binaryFile\"; filename=\""
                            + binaryFile.getName() + "\"").append(CRLF);
            writer.append(
                    "Content-Type: "
                            + URLConnection.guessContentTypeFromName(binaryFile
                            .getName())).append(CRLF);
            writer.append("Content-Transfer-Encoding: binary").append(CRLF);
            writer.append(CRLF).flush();
            InputStream input = new FileInputStream(binaryFile);
            try {
                byte[] buffer = new byte[1024];
                for (int length = 0; (length = input.read(buffer)) > 0;) {
                    output.write(buffer, 0, length);
                }
                output.flush(); // Important! Output cannot be closed. Close of
                // writer will close output as well.
            } finally {
                try { input.close(); } catch (IOException logOrIgnore) {}
            }
            writer.append(CRLF).flush(); // CRLF is important! It indicates end
            // of binary boundary.
            // End of multipart/form-data.
            writer.append("--" + boundary + "--").append(CRLF);
        } finally {
            if (writer != null) writer.close();
        }

// Connection is lazily executed whenever you request any status.
        HttpsURLConnection conn = (HttpsURLConnection) connection;
        conn.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        });

        int responseCode = conn.getResponseCode();
        System.out.println(responseCode);

        StringWriter stringWriter = new StringWriter();
        IOUtils.copy(conn.getInputStream(), stringWriter);
        return stringWriter.toString();
    }
}

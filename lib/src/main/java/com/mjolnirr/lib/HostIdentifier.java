package com.mjolnirr.lib;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/24/13
 * Time: 12:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class HostIdentifier {
    private String host;
    private int port;

    public HostIdentifier(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public int hashCode() {
        return host.hashCode() + port * 10000;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof HostIdentifier)) {
            return false;
        }

        HostIdentifier other = (HostIdentifier) obj;

        return (other.host.equals(host) && (other.port == port));
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}

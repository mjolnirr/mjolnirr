package com.mjolnirr.lib.component;

import com.google.gson.annotations.Expose;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.Properties;
import com.mjolnirr.lib.annotations.*;
import com.mjolnirr.lib.annotations.MjolnirrComponent;
import com.mjolnirr.lib.exceptions.ManifestParsingException;
import com.mjolnirr.lib.exceptions.WrongSuperclassException;
import com.mjolnirr.lib.manifest.MjolnirrMethod;
import com.mjolnirr.lib.util.JarReader;
import com.mjolnirr.lib.util.WildcardMatcher;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.reflections.Reflections;
import org.reflections.util.ConfigurationBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xeustechnologies.jcl.JarClassLoader;
import org.xeustechnologies.jcl.JclObjectFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Created by sk_ on 1/29/14.
 */
public class ComponentInterface {
    private static final String WILDCARD_KEY = "wildcard";
    private static final String ALLOWED_USERS_KEY = "allowed_users";

    @Expose
    private List<Map<String, Object>> pagesWildcards;
    @Expose
    private boolean isModule;
    @Expose
    private String componentName;
    @Expose
    private String className;
    @Expose
    private int minInstancesCount;
    @Expose
    private int maxInstancesCount;
    @Expose
    private long memoryVolume;
    @Expose
    private List<MjolnirrMethod> applicationMethods;

    private ComponentInterface() {
    }

    public static List<ComponentInterface> parsePackage(String jarLocation) throws MalformedURLException {
        //  JCL init
        JarClassLoader jcl = new JarClassLoader();
        jcl.add(jarLocation);

        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(new URL("jar:file:" + jarLocation + "!/")).addClassLoader(jcl));

        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(com.mjolnirr.lib.annotations.MjolnirrComponent.class);

        List<ComponentInterface> components = new ArrayList<ComponentInterface>();
        for (Class clazz : annotated) {
            String className = clazz.getCanonicalName();
            int minInstanceCount = 1;
            int maxInstanceCount = 255;
            long memoryVolume = 256;
            List<Map<String, Object>> pagesWildcards = new ArrayList<Map<String, Object>>();

            boolean isModule;
            if (AbstractApplication.class.isAssignableFrom(clazz)) {
                isModule = false;
            } else {
                isModule = true;
            }

            Annotation[] classAnnotations = clazz.getDeclaredAnnotations();

            String componentName = "";
            for (Annotation annotation : classAnnotations) {
                if (annotation.annotationType().equals(MjolnirrComponent.class)) {
                    MjolnirrComponent mjolnirrAnnotation = (MjolnirrComponent) annotation;
                    componentName = mjolnirrAnnotation.componentName();
                    minInstanceCount = mjolnirrAnnotation.instancesMinCount();
                    maxInstanceCount = mjolnirrAnnotation.instancesMaxCount();
                    memoryVolume = mjolnirrAnnotation.memoryVolume();

                    pagesWildcards = processInterfaceAnnotations(mjolnirrAnnotation.interfaces());

                    break;
                }
            }

            Method[] methods = clazz.getMethods();
            List<MjolnirrMethod> applicationMethods = new ArrayList<MjolnirrMethod>();

            for (Method method : methods) {
                Annotation[] methodAnnotations = method.getDeclaredAnnotations();
                boolean isExposed = false;
                com.mjolnirr.lib.annotations.MjolnirrMethod methodAnnotation = null;

                for (Annotation annotation : methodAnnotations) {
                    if (annotation.annotationType().equals(com.mjolnirr.lib.annotations.MjolnirrMethod.class)) {
                        methodAnnotation = (com.mjolnirr.lib.annotations.MjolnirrMethod) annotation;
                        isExposed = true;
                    }
                }

                if (isExposed) {
                    Class<?>[] params = method.getParameterTypes();
                    applicationMethods.add(new MjolnirrMethod(method.getName(), method.getReturnType(), params, methodAnnotation));
                }
            }

            ComponentInterface component = new ComponentInterface();
            component.isModule = isModule;
            component.applicationMethods = applicationMethods;
            component.className = className;
            component.componentName = componentName;
            component.minInstancesCount = minInstanceCount;
            component.maxInstancesCount = maxInstanceCount;
            component.memoryVolume = memoryVolume;
            component.pagesWildcards = pagesWildcards;
            components.add(component);
        }

        return components;
    }

    private static List<Map<String, Object>> processInterfaceAnnotations(MjolnirrInterface[] interfaces) {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        for (MjolnirrInterface page : interfaces) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put(WILDCARD_KEY, page.pageNameWildCard());
            item.put(ALLOWED_USERS_KEY, page.allowedUsers());

            result.add(item);
        }

        return result;
    }

    public boolean isModule() {
        return isModule;
    }

    public String getComponentName() {
        return componentName;
    }

    public String getClassName() {
        return className;
    }

    public List<MjolnirrMethod> getApplicationMethods() {
        return applicationMethods;
    }

    public boolean hasMethod(String page) {
        for (MjolnirrMethod method : applicationMethods) {
            if (method.getName().equals(page)) {
                return true;
            }
        }

        return false;
    }

    public int getMinInstancesCount() {
        return minInstancesCount;
    }

    public void setMinInstancesCount(int minInstancesCount) {
        this.minInstancesCount = minInstancesCount;
    }

    public int getMaxInstancesCount() {
        return maxInstancesCount;
    }

    public void setMaxInstancesCount(int maxInstancesCount) {
        this.maxInstancesCount = maxInstancesCount;
    }

    public long getMemoryVolume() {
        return memoryVolume;
    }

    public void setMemoryVolume(long memoryVolume) {
        this.memoryVolume = memoryVolume;
    }

    public List<Map<String, Object>> getPagesWildcards() {
        return pagesWildcards;
    }

    public void setPagesWildcards(List<Map<String, Object>> pagesWildcards) {
        this.pagesWildcards = pagesWildcards;
    }

    public ArrayList<String> getAllowedRoles(String requestedPage) {
        for (Map<String, Object> pair : pagesWildcards) {
            WildcardMatcher matcher = new WildcardMatcher((String) pair.get(WILDCARD_KEY));

            if (matcher.matches(requestedPage)) {
                return (ArrayList<String>) pair.get(ALLOWED_USERS_KEY);
            }
        }

        ArrayList<String> results = new ArrayList<String>();
        results.add("user");
        results.add("privileged");

        return results;
    }
}

package com.mjolnirr.lib.component;

import com.mjolnirr.lib.models.UserRepresentation;
import org.apache.log4j.Logger;
import org.xeustechnologies.jcl.JarClassLoader;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/14/13
 * Time: 9:25 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ComponentContext {
    void initialize(String componentName, File configDir, File workspace, UserRepresentation currentUser, JarClassLoader jcl);

    File getConfigDir();

    File getWorkspace();

    String getComponentName();

    String getToken();

    UserRepresentation getCurrentUser();

    void setCurrentUser(UserRepresentation currentUser);

    JarClassLoader getJarClassLoader();

    void setJarClassLoader(JarClassLoader jarClassLoader);
}

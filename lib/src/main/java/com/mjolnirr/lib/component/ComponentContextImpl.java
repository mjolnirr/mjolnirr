package com.mjolnirr.lib.component;

import com.mjolnirr.lib.models.UserRepresentation;
import org.xeustechnologies.jcl.JarClassLoader;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/13/13
 * Time: 2:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class ComponentContextImpl implements ComponentContext {
    private File configDir;
    private File workspace;
    private String componentName;
    private String token;
    private UserRepresentation currentUser;
    private JarClassLoader jarClassLoader;

    public ComponentContextImpl() {
    }

    public ComponentContextImpl(String componentName, File configDir, File workspace, JarClassLoader jcl) {
        initialize(componentName, configDir, workspace, null, jcl);
    }

    public ComponentContextImpl(String token) {
        this.token = token;
    }

    @Override
    public void initialize(String componentName, File configDir, File workspace, UserRepresentation currentUser, JarClassLoader jcl) {
        this.configDir = configDir;
        this.workspace = workspace;
        this.componentName = componentName;
        this.currentUser = currentUser;
        this.jarClassLoader = jcl;
    }

    @Override
    public File getConfigDir() {
        return configDir;
    }

    @Override
    public File getWorkspace() {
        return workspace;
    }

    public String getComponentName() {
        return componentName;
    }

    public String getToken() {
        return token;
    }

    @Override
    public UserRepresentation getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(UserRepresentation currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public JarClassLoader getJarClassLoader() {
        return jarClassLoader;
    }

    @Override
    public void setJarClassLoader(JarClassLoader jarClassLoader) {
        this.jarClassLoader = jarClassLoader;
    }
}

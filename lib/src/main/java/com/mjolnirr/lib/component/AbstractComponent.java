package com.mjolnirr.lib.component;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/13/13
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class AbstractComponent implements MjolnirrComponent {
    public abstract void initialize(ComponentContext context);
}

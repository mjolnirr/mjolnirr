package com.mjolnirr.lib.component;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/14/13
 * Time: 11:30 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MjolnirrComponent {
    void initialize(ComponentContext context);
}

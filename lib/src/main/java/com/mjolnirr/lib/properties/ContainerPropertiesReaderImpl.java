package com.mjolnirr.lib.properties;

import com.google.inject.Inject;
import com.mjolnirr.lib.LoggerFactory;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/28/13
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContainerPropertiesReaderImpl implements PropertiesReader {
    public static final String HIVE_CONTAINER_PROPERTIES = ".hive/container.properties";
    private PropertiesConfiguration config;

    @Inject
    public ContainerPropertiesReaderImpl() {
        try {
            config = new PropertiesConfiguration(HIVE_CONTAINER_PROPERTIES);
        } catch (Exception e) {
            LoggerFactory.getLogger().info("Container config not found, working on command line arguments");
//            e.printStackTrace();
        }
    }

    @Override
    public Configuration getConfig() {
        return config;
    }
}
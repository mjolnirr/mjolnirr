package com.mjolnirr.lib.properties;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by sk_ on 3/30/14.
 */
public class ContainerPropertiesModel implements Serializable {
    @Expose
    private String hostName;

    @Expose
    private Integer port;

    @Expose
    private String containerName;

    @Expose
    private String jarDir;

    @Expose
    private String mainPage;

    @Expose
    private Boolean isDebugMode;

    @Expose
    private String mode;

    @Expose
    private String configDir;

    @Expose
    private String workspaceDir;

    @Expose
    private String proxyHost;
    @Expose
    private int proxyPort;
    @Expose
    private int queuePort;
    @Expose
    private double memoryQuota;

    public ContainerPropertiesModel(String hostName, Integer port, String containerName, String jarDir, String mainPage, Boolean isDebugMode, String mode, String configDir, String workspaceDir, String proxyHost, int proxyPort, int queuePort, double memoryQuota) {
        this.hostName = hostName;
        this.port = port;
        this.containerName = containerName;
        this.jarDir = jarDir;
        this.mainPage = mainPage;
        this.isDebugMode = isDebugMode;
        this.mode = mode;
        this.configDir = configDir;
        this.workspaceDir = workspaceDir;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.queuePort = queuePort;
        this.memoryQuota = memoryQuota;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getJarDir() {
        return jarDir;
    }

    public void setJarDir(String jarDir) {
        this.jarDir = jarDir;
    }

    public String getMainPage() {
        return mainPage;
    }

    public void setMainPage(String mainPage) {
        this.mainPage = mainPage;
    }

    public Boolean getIsDebugMode() {
        return isDebugMode;
    }

    public void setIsDebugMode(Boolean isDebugMode) {
        this.isDebugMode = isDebugMode;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getConfigDir() {
        return configDir;
    }

    public void setConfigDir(String configDir) {
        this.configDir = configDir;
    }

    public String getWorkspaceDir() {
        return workspaceDir;
    }

    public void setWorkspaceDir(String workspaceDir) {
        this.workspaceDir = workspaceDir;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public int getQueuePort() {
        return queuePort;
    }

    public void setQueuePort(int queuePort) {
        this.queuePort = queuePort;
    }

    public double getMemoryQuota() {
        return memoryQuota;
    }

    public void setMemoryQuota(double memoryQuota) {
        this.memoryQuota = memoryQuota;
    }
}

package com.mjolnirr.lib.properties;

import org.apache.commons.configuration.Configuration;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/28/13
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PropertiesReader {
    Configuration getConfig();
}

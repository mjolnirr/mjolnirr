package com.mjolnirr.lib.properties;

import com.google.inject.Inject;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/28/13
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProxyPropertiesReaderImpl implements PropertiesReader {
    public static final String HIVE_PROXY_PROPERTIES = ".hive/proxy.properties";
    private PropertiesConfiguration config;

    @Inject
    public ProxyPropertiesReaderImpl() {
        try {
            config = new PropertiesConfiguration(HIVE_PROXY_PROPERTIES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Configuration getConfig() {
        return config;
    }
}
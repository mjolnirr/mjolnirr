package com.mjolnirr.lib;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/23/13
 * Time: 9:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class Response {
    /**
     * Statuses of execution of the requests
     */
    public static final int OK = 0;
    public static final int FAIL = 1;

    @Expose
    private final String failureMessage;

    /**
     * Contains the name of the object's class according to the manifest
     */
    @Expose
    private String responseClass;

    /**
     * Container response itself
     */
    @Expose
    private String responseObject;

    /**
     * The address of the application which sent this request
     */
    @Expose
    private String target;

    @Expose
    private boolean isSync;

    /**
     * Status of execution of the request
     */
    @Expose
    private int code;

    /**
     * Cause of failure
     */
    @Expose
    private Throwable failureCause;

    /**
     * Callback for this response
     */
    @Expose
    private String onResponse;

    @Expose
    private String token;

    public Response() {
        this.code = OK;
        this.failureCause = null;
        this.failureMessage = null;
    }

    public Response(String responseClass, String responseObject, String target) {
        this.responseClass = responseClass;
        this.responseObject = responseObject;
        this.target = target;
        this.code = OK;
        this.failureCause = null;
        this.failureMessage = null;
    }

    public Response(Throwable failureCause) {
        this.failureCause = failureCause;
        this.failureMessage = failureCause.getMessage();
        this.code = FAIL;
    }

    public boolean isOkay() {
        return code == OK;
    }

    public String getResponseClass() {
        return responseClass;
    }

    public void setResponseClass(String responseClass) {
        this.responseClass = responseClass;
    }

    public String getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(String responseObject) {
        this.responseObject = responseObject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String source) {
        this.target = source;
    }

    /**
     * Serialize this object into json
     * @return This object in json format
     */
    @Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }

    public Throwable getFailureCause() {
        return failureCause;
    }

    public int getCode() {
        return code;
    }

    public void setOnResponse(String onResponse) {
        this.onResponse = onResponse;
    }

    public String getOnResponse() {
        return onResponse;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public boolean isSync() {
        return isSync;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFailureMessage() {
        return failureMessage;
    }
}

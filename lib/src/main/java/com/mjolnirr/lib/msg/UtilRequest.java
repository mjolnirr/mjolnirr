package com.mjolnirr.lib.msg;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.TypeHelper;
import com.mjolnirr.lib.exceptions.MjolnirrException;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 21:09
 * To change this template use File | Settings | File Templates.
 */
public class UtilRequest {
    private static final int TYPE_REGISTRATION = 0;
    private static final int TYPE_COMPONENT_UPLOAD = 1;
    private static final int TYPE_EXECUTION = 2;

    private Gson serializer;

    @Expose
    private String content;

    @Expose
    private int type;

    private UtilRequest() {
        serializer = new Gson();
    }

    public UtilRequest(ContainerInfo info) {
        this();

        content = serializer.toJson(info);
        type = TYPE_REGISTRATION;
    }

    public UtilRequest(File jarFile) {
        this();

        try {
            content = TypeHelper.fromFile(jarFile);
            type = TYPE_COMPONENT_UPLOAD;
        } catch (IOException e) {
            LoggerFactory.getLogger().error("Failed to serialize JAR content", e);
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    public UtilRequest(Execution execution) {
        this();

        content = serializer.toJson(execution);
        type = TYPE_EXECUTION;
    }

    public boolean isRegistration() {
        return (type == TYPE_REGISTRATION);
    }

    public boolean isComponentUpload() {
        return (type == TYPE_COMPONENT_UPLOAD);
    }

    public boolean isExecution() {
        return (type == TYPE_EXECUTION);
    }

    public Object getContent() throws UnknownMessageType, IOException, MjolnirrException {
        switch (type) {
            case TYPE_REGISTRATION:
                return getRegistrationObject();
            case TYPE_COMPONENT_UPLOAD:
                return getUploadedFile();
            default:
                throw new UnknownMessageType();
        }
    }

    public ContainerInfo getRegistrationObject() {
        return serializer.fromJson(content, ContainerInfo.class);
    }

    public File getUploadedFile() throws IOException, MjolnirrException {
        return TypeHelper.toFile(content);
    }

    public Execution getExecution() {
        return serializer.fromJson(content, Execution.class);
    }
}

package com.mjolnirr.lib.msg;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.mjolnirr.lib.TypeHelper;
import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.lib.msg.UnknownMessageType;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 21:09
 * To change this template use File | Settings | File Templates.
 */
public class ContainerUtilRequest {
    public static final int TYPE_LOAD_COMPONENT = 0;
    public static final int TYPE_UNLOAD_COMPONENT = 1;

    @Expose
    private String jarName;

    @Expose
    private int type;

    @Expose
    private String componentName;

    private ContainerUtilRequest() {
//        serializer = new Gson();
    }

    public ContainerUtilRequest(String componentName, String jarName, int type) {
        this();

        this.componentName = componentName;
        this.jarName = jarName;
        this.type = type;
    }

    public boolean isLoad() {
        return (type == TYPE_LOAD_COMPONENT);
    }

    public boolean isUnload() {
        return (type == TYPE_UNLOAD_COMPONENT);
    }

    public String getComponentName() {
        return componentName;
    }

    public String getJarName() {
        return jarName;
    }
}

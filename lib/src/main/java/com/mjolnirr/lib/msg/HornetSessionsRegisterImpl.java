package com.mjolnirr.lib.msg;

import com.mjolnirr.lib.LoggerFactory;
import org.hornetq.api.core.HornetQException;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.core.client.*;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sk_ on 6/26/14.
 */
public class HornetSessionsRegisterImpl implements HornetSessionsRegister {
    private Map<String, HornetCommunicator.SessionObject> sessions;

    public HornetSessionsRegisterImpl() {
        sessions = new HashMap<String, HornetCommunicator.SessionObject>();
    }

    @Override
    public HornetCommunicator.SessionObject getReceiverSession(String channelName) throws Exception {
        if (!sessions.containsKey(channelName)) {
            LoggerFactory.getLogger().info("Session for channel " + channelName + " wasn't found in the registry, creating new one");

            ServerLocator locator = HornetQClient.createServerLocatorWithoutHA(new TransportConfiguration(NettyConnectorFactory.class.getName(), HornetCommunicator.getConfigMap()));
            ClientSessionFactory factory = locator.createSessionFactory();
            ClientSession session = factory.createSession(false, false, false);
            session.start();

            try {
                session.createQueue(channelName, channelName, false);
                session.commit();
            } catch (HornetQException e) {
                if (e.getCode() != HornetCommunicator.QUEUE_EXISTS) {
                    throw e;
                }
            }

            ClientConsumer consumer = session.createConsumer(channelName);
            sessions.put(channelName, new HornetCommunicator.SessionObject(locator, factory, session, consumer));
        }

        return sessions.get(channelName);
    }

    @Override
    public void killAll() throws HornetQException {
        for (String channelName : sessions.keySet()) {
            sessions.get(channelName).close();
        }
    }
}

package com.mjolnirr.lib.msg;

import com.beust.jcommander.Parameter;
import com.google.gson.annotations.Expose;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.Properties;
import com.mjolnirr.lib.properties.ContainerPropertiesModel;

import java.io.File;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/23/13
 * Time: 9:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class ContainerInfo implements Serializable {
    public static final int REFRESH_TIMEOUT = 30000;

    public static final int TYPE_REGISTER = 0;
    public static final int TYPE_UNREGISTER = 1;
    public static final int TYPE_UPDATE = 2;

    @Expose
    private String containerName;

    /**
     * Contains the name of the object's class according to the manifest
     */
    @Expose
    private String hostName;

    /**
     * Container response itself
     */
    @Expose
    private String containerID;

    /**
     * Is connection request
     * */
    @Expose
    private int type;

    @Expose
    private ContainerPropertiesModel properties;

    private Set<String> loadedComponents;

    private Set<String> componentToLoad;
    private Set<String> componentToUnload;
    private Set<Execution> runningComponents;

    public ContainerInfo() {
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            hostName = "Unknown host";
        }
        containerID = "";
        containerName = "";
        loadedComponents = new HashSet<String>();
        componentToLoad = new HashSet<String>();
        componentToUnload = new HashSet<String>();
        runningComponents = new HashSet<Execution>();
    }

    public ContainerInfo(String containerID, String containerName, boolean isConnect, ContainerPropertiesModel properties) {
        this();
        this.containerID = containerID;
        this.containerName = containerName;
        this.properties = properties;

        if (isConnect) {
            this.type = TYPE_REGISTER;
        } else {
            this.type = TYPE_UNREGISTER;
        }
    }

    public String getHostName() {
        return hostName;
    }

    public String getContainerID() {
        return containerID;
    }

    public String getContainerName() {
        return containerName;
    }

    public boolean isConnect() {
        return (this.type == TYPE_REGISTER);
    }

    public ContainerInfo loadComponent(String componentName) {
        componentToLoad.add(componentName);
        componentToUnload.remove(componentName);

        return this;
    }

    public ContainerInfo unloadComponent(String componentName) {
        componentToUnload.add(componentName);
        componentToLoad.remove(componentName);

        return this;
    }

    public Set<String> getLoadedComponents() {
        return loadedComponents;
    }

    public void commit() throws Exception {
        HornetCommunicator communicator = new HornetCommunicator();

        for (String componentName : componentToLoad) {
            //  Send message to container
            communicator.loadComponent(containerID, componentName);
            loadedComponents.add(componentName);
            LoggerFactory.getLogger().warn("Component " + componentName + " loaded on " + containerID);
        }

        for (String componentName : componentToUnload) {
            //  Send message to container
            communicator.unloadComponent(containerID, componentName);
            loadedComponents.remove(componentName);
            LoggerFactory.getLogger().warn("Component " + componentName + " unloaded on " + containerID);
        }

        rollback();
    }

    public void rollback() {
        componentToLoad.clear();
        componentToUnload.clear();
    }

    public boolean isStateUpdate() {
        return (this.type == TYPE_UPDATE);
    }

    public void setStateUpdate() {
        this.type = TYPE_UPDATE;
    }

    @Override
    public int hashCode() {
        return hostName.hashCode() + containerID.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ContainerInfo)) {
            return false;
        }

        ContainerInfo other = (ContainerInfo) obj;

        return ((this.hostName.equals(other.hostName)) && (this.containerID.equals(other.containerID)));
    }

    @Override
    public String toString() {
        return containerName + ", " + hostName + ":" + containerID + ", to type: " + type;
    }

    public void addRunningComponent(Execution execution) {
        runningComponents.add(execution);
        System.out.println("Currently running components");

        for (Execution e : runningComponents) {
            System.out.println(e.getComponentName() + " from " + e.getInitiatorToken());
        }
    }

    public void removeRunningComponent(Execution execution) {
        runningComponents.remove(execution);

        System.out.println("Currently running components");

        for (Execution e : runningComponents) {
            System.out.println(e.getComponentName() + " from " + e.getInitiatorToken());
        }
    }

    public ContainerPropertiesModel getProperties() {
        return properties;
    }

    public void setProperties(ContainerPropertiesModel properties) {
        this.properties = properties;
    }

    public Representation toRepresentation(String userToken) {
        List<String> ownerExecutions = new ArrayList<String>();

        for (Execution exec : runningComponents) {
            if (exec.getInitiatorToken().equals(userToken)) {
                ownerExecutions.add(exec.getComponentName());
            }
        }

        return new Representation(containerID, runningComponents.size(), ownerExecutions);
    }

    public static class Representation {
        @Expose
        private String containerID;

        @Expose
        private int executionsCount;

        @Expose
        private List<String> ownerExecutions;

        public Representation() {}

        public Representation(String containerID, int executionsCount, List<String> ownerExecutions) {
            this.containerID = containerID;
            this.executionsCount = executionsCount;
            this.ownerExecutions = new ArrayList<String>(ownerExecutions);
        }
    }
}

package com.mjolnirr.lib.msg;

import com.mjolnirr.lib.msg.HornetCommunicator;
import org.hornetq.api.core.HornetQException;

/**
 * Created by sk_ on 6/26/14.
 */
public interface HornetSessionsRegister {
    HornetCommunicator.SessionObject getReceiverSession(String channelName) throws Exception;

    void killAll() throws HornetQException;
}

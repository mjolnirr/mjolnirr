package com.mjolnirr.lib.msg;

import com.google.gson.Gson;
import com.mjolnirr.lib.*;

import com.mjolnirr.lib.component.ComponentContext;
import org.hornetq.api.core.HornetQException;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.core.client.*;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Implementation of accessor to message bus based on HornetQ
 */
public class HornetCommunicator implements Communicator {

    public static final int QUEUE_EXISTS = 101;

    private static String HORNET_HOST = "localhost";
    private static int HORNET_PORT = 5445;

    private static final ScheduledExecutorService scheduler;

    static {
        scheduler = Executors.newScheduledThreadPool(10);
    }

    public static void setHornetHost(String hornetHost) {
        HornetCommunicator.HORNET_HOST = hornetHost;
    }

    public static void setHornetPort(int hornetPort) {
        HornetCommunicator.HORNET_PORT = hornetPort;
    }

    /**
     * Send message to a channel
     *
     * @param componentName To whom you want to send the message
     * @param methodName    What you want from the recipient
     * @param arguments     Arguments for invocation
     * @param callback      Callback
     * @throws Exception
     */
    @Override
    public <T> void send(final ComponentContext context, final String componentName, final String methodName, final List<Object> arguments, Callback<T> callback) throws Exception {
        final String responseAddress = Properties.privateChannelName(context.getComponentName());

        final String requestUID = Request.generateUniqueKey();
        CallbackRegister.addCallback(requestUID, callback);

        scheduler.schedule(new Runnable() {
            @Override
            public void run() {
                try {
                    send(componentName.toLowerCase(),
                            new Request(
                                    componentName,
                                    methodName,
                                    arguments,
                                    responseAddress,
                                    requestUID)
                                    .setToken(context.getToken()));
                } catch (Exception e) {
                    LoggerFactory.getLogger().fatal("Failed to send message", e);
                }
            }
        }, 0, TimeUnit.SECONDS);
    }

    /**
     * Send message to a channel
     *
     * @param channelName Name of a target channel
     * @param message     Request message
     * @throws Exception
     */
    void send(String channelName, Request message) throws Exception {
        send(channelName, message.toString());
    }

    public void registerContainer(ContainerInfo info) throws Exception {
        UtilRequest request = new UtilRequest(info);

        send("util_response_address", new Gson().toJson(request));
    }

    public void uploadComponent(File jarFile) throws Exception {
        UtilRequest request = new UtilRequest(jarFile);

        send("util_response_address", new Gson().toJson(request));
    }

    public void loadComponent(String containerID, String componentName) throws Exception {
        ContainerUtilRequest request = new ContainerUtilRequest(componentName, "origJar.jar", ContainerUtilRequest.TYPE_LOAD_COMPONENT);

        send(containerID, new Gson().toJson(request));
    }

    public void unloadComponent(String containerID, String componentName) throws Exception {
        ContainerUtilRequest request = new ContainerUtilRequest(componentName, "origJar.jar", ContainerUtilRequest.TYPE_UNLOAD_COMPONENT);

        send(containerID, new Gson().toJson(request));
    }

    public void changeExecutionState(Execution execution) throws Exception {
        UtilRequest request = new UtilRequest(execution);

        send("util_response_address", new Gson().toJson(request));
    }

    /**
     * Sends message to a channel
     *
     * @param channelName Name of a target channel
     * @param messageText Text of specific message
     * @throws Exception
     */
    void send(String channelName, String messageText) throws Exception {
        ServerLocator locator = HornetQClient.createServerLocatorWithoutHA(new TransportConfiguration(NettyConnectorFactory.class.getName(), getConfigMap()));
        ClientSessionFactory factory = locator.createSessionFactory();
        ClientSession session = factory.createSession(false, false, false);

        try {
            session.createQueue(channelName, channelName, false);
        } catch (HornetQException e) {
            if (e.getCode() != QUEUE_EXISTS) {
                throw e;
            }
        }

        ClientProducer producer = session.createProducer(channelName);
        ClientMessage message = session.createMessage(true);
        message.getBodyBuffer().writeString(messageText);

        session.start();

        producer.send(message);
        LoggerFactory.getLogger().info(String.format("Message sent to \"%s\".", channelName));

        session.commit();
        session.close();
        factory.close();
        locator.close();
    }

    @Override
    public <T> T sendSync(final ComponentContext context, final String componentName, final String methodName, final List<Object> arguments, Class<T> requiredClass) throws Exception {
        final String requestUID = Request.generateUniqueKey();
        final String responseAddress = Properties.privateChannelName(context.getComponentName());

        Response response = CallbackRegister.addSyncCallback(requestUID, new Runnable() {
            @Override
            public void run() {
                // Send request
                try {
                    Request request = new Request(
                            componentName,
                            methodName,
                            arguments,
                            responseAddress,
                            requestUID)
                            .setToken(context.getToken());
                    request.setSync(true);

                    send(componentName.toLowerCase(), request);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Object result;

        if (TypeHelper.isFile(response.getResponseClass())) {
            //  Put bytes to file
            result = TypeHelper.toFile(new Gson().fromJson(response.getResponseObject(), String.class));
        } else {
            //  Parse as class
            result = new Gson().fromJson(response.getResponseObject(), requiredClass);

            if (result instanceof List) {
                List list = (List) result;
                List fileList = new ArrayList();

                for (int j = 0; j < list.size(); j++) {
                    if (TypeHelper.isTransmittedFile((String) list.get(j))) {
                        fileList.add(TypeHelper.toFile((String) list.get(j)));
                    } else {
                        fileList.add(list.get(j));
                    }
                }

                for (Object l : fileList) {
                    System.out.println(l.getClass());
                }

                result = fileList;
            }
        }

//        return new Gson().fromJson(response.getResponseObject(), requiredClass);
        return (T) result;
    }

    /**
     * Receive message from specific channel
     *
     * @param channelName Name of the source channel
     * @param callback    Custom action to do with received message
     * @param <T>         an extension of Callback<String>
     * @throws Exception
     */
    @Override
    public <T extends Callback<String>> void receive(SessionObject sessionObject, String channelName, Class<T> callback, AcknowledgeCallback acknowledgeCallback) throws Exception {
        //  Get sessions from shared registry
        ClientMessage msgReceived = sessionObject.getConsumer().receive();

        if (!acknowledgeCallback.isAcknowledged(channelName)) {
            LoggerFactory.getLogger().info("Message received, but not acknowledged");
            sessionObject.getSession().rollback();
            return;
        }

        if (msgReceived != null) {
            msgReceived.acknowledge();
            sessionObject.getSession().commit();
            LoggerFactory.getLogger().info("Message received.");
            if (callback != null) {
                callback.newInstance().run(msgReceived.getBodyBuffer().readString());
            }
        } else {
            sessionObject.getSession().rollback();
        }
    }

    public static Map getConfigMap() {
        HashMap configMap = new HashMap();
        configMap.put("host", HORNET_HOST);
        configMap.put("port", HORNET_PORT);

        return configMap;
    }

    public static class SessionObject {
        private final ServerLocator locator;
        private final ClientSessionFactory factory;
        private final ClientSession session;
        private final ClientConsumer consumer;

        public SessionObject(ServerLocator locator, ClientSessionFactory factory, ClientSession session, ClientConsumer consumer) {
            this.locator = locator;
            this.factory = factory;
            this.session = session;
            this.consumer = consumer;
        }

        public void close() throws HornetQException {
            consumer.close();
            session.close();
            factory.close();
            locator.close();
        }

        public ServerLocator getLocator() {
            return locator;
        }

        public ClientSessionFactory getFactory() {
            return factory;
        }

        public ClientSession getSession() {
            return session;
        }

        public ClientConsumer getConsumer() {
            return consumer;
        }
    }
}

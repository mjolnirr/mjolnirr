package com.mjolnirr.lib.msg;

import com.google.gson.annotations.Expose;

/**
 * Created by sk_ on 2/9/14.
 */
public class Execution {
    @Expose
    private String initiatorToken;

    @Expose
    private String componentName;

    @Expose
    private boolean isStarting;

    @Expose
    private String containerName;

    public Execution() {

    }

    public Execution(String containerName, String initiatorToken, String componentName, boolean isStarting) {
        this.containerName = containerName;
        this.initiatorToken = initiatorToken;
        this.componentName = componentName;
        this.isStarting = isStarting;
    }

    public String getInitiatorToken() {
        return initiatorToken;
    }

    public void setInitiatorToken(String initiatorToken) {
        this.initiatorToken = initiatorToken;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public boolean isStarting() {
        return isStarting;
    }

    public void setStarting(boolean isStarting) {
        this.isStarting = isStarting;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Execution)) {
            return false;
        }
        Execution other = (Execution) obj;

        return (initiatorToken.equals(other.initiatorToken) && componentName.equals(other.componentName));
    }

    @Override
    public int hashCode() {
        if (initiatorToken == null) { System.out.println("Initiator IS NULL"); }
        if (componentName == null) { System.out.println("Component IS NULL"); }

        return initiatorToken.hashCode() + componentName.hashCode();
    }
}

package com.mjolnirr.lib.msg;

import com.mjolnirr.lib.Callback;
import com.mjolnirr.lib.Request;
import com.mjolnirr.lib.component.ComponentContext;

import java.util.List;

/**
 * Interface which provides access to message queue
 */
public interface Communicator {
    public interface AcknowledgeCallback {
        public boolean isAcknowledged(String componentName);
    }

    /**
     * Send message to a channel
     * @param context Component context
     * @param componentName To whom you want to send the message
     * @param methodName What you want from the recipient
     * @param arguments Arguments for invocation
     * @param callback Callback
     * @throws Exception
     */
    <T> void send(ComponentContext context, String componentName, String methodName, List<Object> arguments, Callback<T> callback) throws Exception;

    /**
     *  Send and receive message synchronously
     *  @param context Component context
     *  @param componentName To whom you want to send the message
     *  @param methodName What you want from the recipient
     *  @param arguments Arguments for invocation
     *  @param requiredClass Class of the result
     *
     *  @return request result
     * */
    <T> T sendSync(ComponentContext context, String componentName, String methodName, List<Object> arguments, Class<T> requiredClass) throws Exception;

    /**
     * Receive message from specific channel
     * @param channelName Name of the source channel
     * @param callback Custom action to do with received message
     * @param <T> an extension of Callback<String>
     * @throws Exception
     */
    public <T extends Callback<String>> void receive(HornetCommunicator.SessionObject s, String channelName, Class<T> callback, AcknowledgeCallback acknowledgeCallback) throws Exception;
}

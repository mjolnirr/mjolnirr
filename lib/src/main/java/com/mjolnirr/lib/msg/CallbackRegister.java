package com.mjolnirr.lib.msg;

import com.mjolnirr.lib.Callback;
import com.mjolnirr.lib.Response;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 25.10.13
 * Time: 19:53
 * To change this template use File | Settings | File Templates.
 */
public class CallbackRegister {
    private static class ProxyCallback {
        private Response rawResponse;
        private Object syncronizer;

        public ProxyCallback() {
            syncronizer = new Object();
        }

        public Response getRawResponse() {
            return rawResponse;
        }

        public void setRawResponse(Response response) {
            rawResponse = response;
        }

        public void waitForCallback() {
            try {
                syncronizer.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void notifyFinished() {
            syncronizer.notify();
        }

        public Object getSyncronizer() {
            return syncronizer;
        }
    }

    private static Map<String, ProxyCallback> syncCallbacks;
    private static Map<String, Callback> callbacks;

    static {
        syncCallbacks = new HashMap<String, ProxyCallback>();
        callbacks = new HashMap<String, Callback>();
    }

    public static void addCallback(String uid, Callback callback) {
        callbacks.put(uid, callback);
    }

    public static Callback getCallbackInstance(String uid) {
        return callbacks.get(uid);
    }

    public static Response addSyncCallback(String uid, Runnable doBeforeBlock) {
        ProxyCallback callback = new ProxyCallback();
        syncCallbacks.put(uid, callback);

        synchronized (callback.getSyncronizer()) {
            doBeforeBlock.run();
            callback.waitForCallback();
        }

        Response result = syncCallbacks.get(uid).getRawResponse();
        syncCallbacks.remove(uid);

        return result;
    }

    public static void setSyncResponse(String uid, Response response) {
        ProxyCallback currentCallback = syncCallbacks.get(uid);

        currentCallback.setRawResponse(response);
        synchronized (currentCallback.getSyncronizer()) {
            syncCallbacks.get(uid).notifyFinished();
        }
    }
}

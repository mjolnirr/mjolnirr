package com.mjolnirr.lib;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/2/13
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Persons")
public class Person {
    @Id
    @GeneratedValue
    @Column(name = "person_id")     private int id;
    @Column(name = "first_name")    private String firstName;
    @Column(name = "last_name")     private String lastName;

    public Person() {}

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(Person person) {
        firstName = person.firstName;
        lastName = person.lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

package com.mjolnirr.lib;

import com.google.gson.Gson;
import com.mjolnirr.lib.exceptions.MjolnirrException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import sun.tools.jar.resources.jar;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/16/13
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContainerTransport {
    private static final String CONTAINER_NAMESPACE = "http://web.proxy.mjolnirr.org/";
//    private final ProxyWeb proxy;
    private final String host;
    private final int port;

    public ContainerTransport(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public byte[] getInterface(String applicationName, String interfaceName) throws MjolnirrException, IOException {
//        URL url = null;
//        try {
//            url = new URL("https://" + host + ":" + port + "/interface/" + applicationName + "/" + interfaceName);
//            InputStream is = url.openStream();  // throws an IOException
//
//            return IOUtils.toByteArray(is);
//        } catch (Exception e) {
//            throw MjolnirrException.createException(e.getMessage());
//        }

        System.out.println("Getting interface " + interfaceName + " of application " + applicationName);

        String url = "https://" + host + ":" + port + "/interface/" + applicationName + "/" + interfaceName;

        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);

        get.setHeader("User-Agent", "Mjolnirr-Enterprise-Browser");

        HttpResponse response = client.execute(get);

        return IOUtils.toByteArray(response.getEntity().getContent());

//        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//
//        StringBuffer result = new StringBuffer();
//        String line;
//        while ((line = rd.readLine()) != null) {
//            result.append(line);
//        }
//
//        return result.toString();
    }

    public byte[] getResourceFile(String applicationName, String resourceName) throws Exception {
        System.out.println("Getting resource " + resourceName + " of application " + applicationName);

        URL url = null;
        try {
            url = new URL("https://" + host + ":" + port + "/static/" + applicationName + "/" + resourceName);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });

            InputStream is = conn.getInputStream();  // throws an IOException

            return IOUtils.toByteArray(is);
        } catch (Exception e) {
            LoggerFactory.getLogger().fatal("Failed to download resource", e);

            throw MjolnirrException.createException(e.getMessage());
        }
    }

    public Response processRequest(String applicationName, String methodName, List<Object> args) throws Exception {
        Request req = new Request(applicationName, methodName, args);

        String url = "https://" + host + ":" + port + "/request";

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        post.setHeader("User-Agent", "Mjolnirr-Enterprise-Browser");

        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("request", req.toString()));

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        HttpResponse response = client.execute(post);

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        return new Gson().fromJson(result.toString(), Response.class);
    }

    public byte[] loadByURL(String filename) throws IOException {
        System.out.println("Getting resource by URL " + filename);

        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(filename);

        get.setHeader("User-Agent", "Mjolnirr-Enterprise-Browser");

        HttpResponse response = client.execute(get);

        return IOUtils.toByteArray(response.getEntity().getContent());
    }

    public long getResourceModificationDate(String componentName, String jarName) throws MjolnirrException {
        try {
            URL url = new URL("https://" + host + ":" + port + "/static/" + componentName + "/" + jarName);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession sslSession) {
                    return true;
                }
            });
            conn.connect();

            return conn.getLastModified();
        } catch (Exception e) {
            LoggerFactory.getLogger().fatal("Failed to get resource modification date", e);

            throw MjolnirrException.createException(e.getMessage());
        }
    }
}
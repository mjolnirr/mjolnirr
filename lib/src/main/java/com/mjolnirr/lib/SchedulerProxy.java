package com.mjolnirr.lib;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class SchedulerProxy {

    /**
     * Start new scheduler for job
     * @param jobClass Class of the job
     * @param <T> Type of the scheduling job
     * @throws SchedulerException
     */
    public static <T extends Job> void scheduleJob(Class<T> jobClass, String groupName, int timer) throws SchedulerException {
        JobDetail job = JobBuilder
                .newJob(jobClass)
                .withIdentity(jobClass.getName() + "_Job", groupName)
                .build();

        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity(jobClass.getName() + "_Trigger", groupName)
                .withSchedule(SimpleScheduleBuilder
                        .simpleSchedule()
                        .withIntervalInSeconds(timer)
                        .repeatForever())
                .build();

        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.start();
        scheduler.scheduleJob(job, trigger);
    }
}

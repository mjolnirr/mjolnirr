# Mjolnirr platform #

Mjolnirr is a private cloud platform with a simple and developer-friendly API. It was designed as a private replacement of public cloud hosting solutions like AppEngine to make your computing safe and controllable. With Mjolnirr you can easily deploy your own PaaS cloud and use there your applications or applications from the Store (coming soon).

Mjolnirr makes you to follow MVC in your project and to distribute the logic onto different components to scale it. It also makes your application code clear and understandable.

How does it work? See `Getting started` section

Here is a short reference, for the full documentation see the Wiki pages.

## Getting started ##

### Project creation ###

In order to get started you can create a sample project from the `Maven` archetype:

    mvn archetype:generate -DarchetypeCatalog=http://mjolnirr.com/archiva/repository/mjolnirr
    
This command will create the sample which contains ready-to-deploy `Calculator` application. You can build this sample using simple command

    mvn clean install
    
This will produce ready-to-deploy JARs in `target` subfolders of `app` and `module` respectively.

Also you can use this archetype for the fast project strucure generator - in contains one Application, one Module and one `lib` project - there must be shared classes and some other stuff.

### Hosting and testing ###

It's easy - just get browser installer and container bundle (TODO - coming soon) and place your JARs into the configured container's JAR directory (`jar` by default) and then

    ./container.sh
    
Container will handle all the components inside JAR directory. Now you can run the browser.
Type the following URL in the address string:

    hive://localhost/calculator/calculator
    
This page contains one of the `Calculator` application pages. In this URL `calculator/calculator` means `app_name/page_name`, and if there is no page specified, container will return default page (configurable, `main` in default config)
If your proxy has non-standard (8091) port number, you can specify it in URL.

## Building ##

Project uses JavaFX2 maven module, so in order to build the project you must have Java 7 with JavaFX libraries installed. Before you'll try to build the project, you must perform following command:

    mvn com.zenjava:javafx-maven-plugin:2.0:fix-classpath

It enables `Mavem` to find all the classes it needs.

 And after that, in the project directory

     mvn clean install

It'll build all the nessessary JARs.

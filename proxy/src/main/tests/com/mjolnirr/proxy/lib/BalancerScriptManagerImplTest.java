package com.mjolnirr.proxy.lib;

import com.mjolnirr.proxy.models.BalancerScriptModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by sk_ on 5/10/14.
 */
public class BalancerScriptManagerImplTest {
    private static final String TEST_SCRIPT_NAME = "test_script";
    private static final String TEST_SCRIPT_CONTENT = "TEST_SCRIPT_CONTENT";
    private int testScriptID;

    @Before
    public void setUp() throws Exception {
        testScriptID = createNewScript();
    }

    @After
    public void tearDown() throws Exception {
        BalancerScriptManagerImpl balancerScriptManager = new BalancerScriptManagerImpl(1);
        balancerScriptManager.removeScript("" + testScriptID);
    }

    public int createNewScript() throws Exception {
        BalancerScriptManagerImpl balancerScriptManager = new BalancerScriptManagerImpl(1);

        int scriptID = balancerScriptManager.createNewScript(TEST_SCRIPT_NAME, TEST_SCRIPT_CONTENT);

        balancerScriptManager.setActiveScript("" + scriptID);

        return scriptID;
    }

    @Test
    public void testCreateNewScript() throws Exception {
    }

    @Test
    public void testGetAllScripts() throws Exception {
        BalancerScriptManagerImpl balancerScriptManager = new BalancerScriptManagerImpl(1);

        boolean testScriptFound = false;

        for (BalancerScriptModel balancerScript : balancerScriptManager.getAllScripts()) {
            if (balancerScript.getScriptName().equals(TEST_SCRIPT_NAME)) {
                testScriptFound = true;
            }
        }

        assert testScriptFound;
    }

    @Test
    public void testGetCurrentActiveScript() throws Exception {
        BalancerScriptManagerImpl balancerScriptManager = new BalancerScriptManagerImpl(1);

        assert balancerScriptManager.getCurrentActiveScript().equals(TEST_SCRIPT_CONTENT);
    }

    @Test
    public void testFindScript() throws Exception {
        BalancerScriptManagerImpl balancerScriptManager = new BalancerScriptManagerImpl(1);

        assert balancerScriptManager.findScript("" + testScriptID).getScriptContent().equals(TEST_SCRIPT_CONTENT);
    }

    @Test
    public void testUpdateScript() throws Exception {
        BalancerScriptManagerImpl balancerScriptManager = new BalancerScriptManagerImpl(1);

        balancerScriptManager.updateScript("" + testScriptID, TEST_SCRIPT_NAME, TEST_SCRIPT_CONTENT + "/" + TEST_SCRIPT_CONTENT);
        assert balancerScriptManager.findScript("" + testScriptID).getScriptContent().equals(TEST_SCRIPT_CONTENT + "/" + TEST_SCRIPT_CONTENT);
    }

    @Test
    public void testForkScript() throws Exception {
        BalancerScriptManagerImpl balancerScriptManager = new BalancerScriptManagerImpl(1);

        int forkedScriptID = balancerScriptManager.forkScript("" + testScriptID);
        assert balancerScriptManager.findScript("" + forkedScriptID).getScriptContent().equals(TEST_SCRIPT_CONTENT);

        balancerScriptManager.removeScript("" + forkedScriptID);
    }
}

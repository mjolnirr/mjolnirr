package com.mjolnirr.proxy.lib;

import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.proxy.models.UserModel;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.math.BigInteger;

/**
 * Created by sk_ on 5/10/14.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UsersManagerImplTest {
    private static final String SIMPLE_USER_NAME = "simple_user";
    private static final String ADMIN_USER_NAME = "admin_user";

    private BigInteger simpleUserSerial;
    private BigInteger adminUserSerial;
    private Object initializeSemaphor;

    @Before
    public void setUp() throws Exception {
        simpleUserSerial = BigInteger.valueOf(1);
        adminUserSerial = BigInteger.valueOf(2);

        testAddUser();
    }

    @After
    public void tearDown() throws Exception {
        UsersManagerImpl manager = new UsersManagerImpl(null);

        manager.removeUser(simpleUserSerial.toString());
        manager.removeUser(adminUserSerial.toString());
    }

    @Test
    public void testAddUser() throws Exception {
        UsersManagerImpl manager = new UsersManagerImpl(null);

        UserModel simpleUserModel = new UserModel(SIMPLE_USER_NAME);
        simpleUserModel.setRole("user");
        simpleUserModel.setSerial(simpleUserSerial);
        UserModel adminUserModel = new UserModel(ADMIN_USER_NAME);
        adminUserModel.setRole("admin");
        adminUserModel.setSerial(adminUserSerial);

        manager.addUser(simpleUserModel);
        manager.addUser(adminUserModel);
    }

    @Test
    public void testFindUser() throws Exception {
        UsersManagerImpl manager = new UsersManagerImpl(null);

        UserModel simpleUserModel = manager.findUser(simpleUserSerial);
        UserModel adminUserModel = manager.findUser(adminUserSerial);

        assert simpleUserModel.getUsername().equals(SIMPLE_USER_NAME);
        assert adminUserModel.getUsername().equals(ADMIN_USER_NAME);
    }

    @Test
    public void testGetUsers() throws Exception {
        UsersManagerImpl manager = new UsersManagerImpl(null);

        boolean adminFound = false;
        boolean userFound = false;

        for (UserRepresentation user : manager.getUsers()) {
            if (user.getUsername().equals(SIMPLE_USER_NAME)) {
                userFound = true;
            }

            if (user.getUsername().equals(ADMIN_USER_NAME)) {
                adminFound = true;
            }
        }

        assert userFound == true;
        assert adminFound == false;
    }

    @Test
    public void testGetAllUsers() throws Exception {
        UsersManagerImpl manager = new UsersManagerImpl(null);

        boolean adminFound = false;
        boolean userFound = false;

        for (UserRepresentation user : manager.getAllUsers()) {
            if (user.getUsername().equals(SIMPLE_USER_NAME)) {
                userFound = true;
            }

            if (user.getUsername().equals(ADMIN_USER_NAME)) {
                adminFound = true;
            }
        }

        assert userFound == true;
        assert adminFound == true;
    }

    @Test
    public void testGetAdmins() throws Exception {
        UsersManagerImpl manager = new UsersManagerImpl(null);

        boolean adminFound = false;
        boolean userFound = false;

        for (UserRepresentation user : manager.getAdmins()) {
            if (user.getUsername().equals(SIMPLE_USER_NAME)) {
                userFound = true;
            }

            if (user.getUsername().equals(ADMIN_USER_NAME)) {
                adminFound = true;
            }
        }

        assert userFound == false;
        assert adminFound == true;
    }

    @Test
    public void testIsSerialUnique() throws Exception {
        UsersManagerImpl manager = new UsersManagerImpl(null);

        assert !manager.isSerialUnique(simpleUserSerial);
        assert !manager.isSerialUnique(adminUserSerial);
    }
}

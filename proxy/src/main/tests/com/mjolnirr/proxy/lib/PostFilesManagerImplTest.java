package com.mjolnirr.proxy.lib;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by sk_ on 5/11/14.
 */
public class PostFilesManagerImplTest {
    private static final String FILE_CONTENT = "file content file content";

    private File tmpFilesDirectory;

    static String readFile(File path) throws IOException {
        byte[] encoded = Files.readAllBytes(path.toPath());
        return new String(encoded, Charset.defaultCharset());
    }

    @Test
    public void testCreateTempFile() throws Exception {
        tmpFilesDirectory = new File("tmpfiles");
        PostFilesManagerImpl postFilesManager = new PostFilesManagerImpl(tmpFilesDirectory);

        File tmpFile = new File("filename.txt");
        PrintWriter out = new PrintWriter(tmpFile);
        out.write(FILE_CONTENT);
        out.close();

        String postFileName = postFilesManager.createTempFile(tmpFile);
        tmpFile.delete();

        tmpFilesDirectory = new File("tmpfiles");

        File postedFile = postFilesManager.getContent(postFileName);

        assert readFile(postedFile).equals(FILE_CONTENT);
    }
}

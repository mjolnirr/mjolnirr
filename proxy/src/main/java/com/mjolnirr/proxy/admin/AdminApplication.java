package com.mjolnirr.proxy.admin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Injector;
import com.mjolnirr.lib.*;
import com.mjolnirr.lib.models.CertificateRepresentation;
import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.lib.msg.HornetCommunicator;
import com.mjolnirr.proxy.ProxyModule;
import com.mjolnirr.proxy.balancer.Balancer;
import com.mjolnirr.proxy.lib.*;
import com.mjolnirr.proxy.models.BalancerScriptModel;
import com.mjolnirr.proxy.models.ComponentModel;
import com.mjolnirr.proxy.models.ContainerCertificate;
import com.mjolnirr.proxy.models.UserModel;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import org.apache.commons.lang3.StringEscapeUtils;
import org.bouncycastle.crypto.CryptoException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

/**
 * Created by sk_ on 1/23/14.
 */
public class AdminApplication {
    public static Map<String, Object> invokeInterface(String pageName, Map<String, String> params) {
        if (pageName.equalsIgnoreCase("users")) {
            return new HashMap<String, Object>() {{
                put("users", getUsers());
            }};
        } else if (pageName.equalsIgnoreCase("components")) {
            return new HashMap<String, Object>() {{
                put("components", getComponents());
            }};
        } else if (pageName.equalsIgnoreCase("balancer")) {
            return new HashMap<String, Object>() {{
                put("scripts", getBalancerScripts());
            }};
        } else if (pageName.equalsIgnoreCase("containers")) {
            return new HashMap<String, Object>() {{
                put("certs", getCerts());
                put("containers", getContainers());
            }};
        }

        return null;
    }

    private static List<BalancerScriptModel> getBalancerScripts() {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        BalancerScriptManager balancerScriptManager = injector.getInstance(BalancerScriptManager.class);

        List<BalancerScriptModel> result = balancerScriptManager.getAllScripts();

        for (BalancerScriptModel model : result) {
            model.setScriptName(StringEscapeUtils.escapeEcmaScript(model.getScriptName()));
            model.setScriptContent(StringEscapeUtils.escapeEcmaScript(model.getScriptContent()));
        }

        return result;
    }

    public static Response invoke(Request request) throws Exception {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        PostFilesManager tmpFileManager = injector.getInstance(PostFilesManager.class);

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        String responseClass = null;
        Object response = null;

        if (request.getMethodName().equalsIgnoreCase("createUser")) {
            //  Create new user
            createUser(gson.fromJson(request.getArgs().get(0), String.class), gson.fromJson(request.getArgs().get(1), String.class));

            responseClass = "java.lang.String";
            response = "";
        } else if (request.getMethodName().equalsIgnoreCase("generateCertificate")) {
            //  Create new user
            generateCertificate();

            responseClass = "java.lang.String";
            response = "";
        } else if (request.getMethodName().equalsIgnoreCase("createBalancerScript")) {
            //  Create new user

            responseClass = "java.lang.String";
            response = createBalancerScript(
                    gson.fromJson(request.getArgs().get(0), String.class),
                    gson.fromJson(request.getArgs().get(1), String.class)
            );
        } else if (request.getMethodName().equalsIgnoreCase("updateBalancerScript")) {
            //  Create new user
            updateBalancerScript(
                    gson.fromJson(request.getArgs().get(0), String.class),
                    gson.fromJson(request.getArgs().get(1), String.class),
                    gson.fromJson(request.getArgs().get(2), String.class)
            );

            responseClass = "java.lang.String";
            response = "";
        } else if (request.getMethodName().equalsIgnoreCase("setActiveBalancerScript")) {
            //  Create new user
            setActiveBalancerScript(
                    gson.fromJson(request.getArgs().get(0), String.class)
            );

            responseClass = "java.lang.String";
            response = "";
        } else if (request.getMethodName().equalsIgnoreCase("forkBalancerScript")) {
            //  Create new user
            forkBalancerScript(
                    gson.fromJson(request.getArgs().get(0), String.class)
            );

            responseClass = "java.lang.String";
            response = "";
        } else if (request.getMethodName().equalsIgnoreCase("deleteBalancerScript")) {
            //  Create new user
            deleteBalancerScript(
                    gson.fromJson(request.getArgs().get(0), String.class)
            );

            responseClass = "java.lang.String";
            response = "";
        } else if (request.getMethodName().equalsIgnoreCase("testBalancerScript")) {
            responseClass = "java.lang.String";
            response = testBalancerScript(
                    gson.fromJson(request.getArgs().get(0), String.class)
            );

        } else if (request.getMethodName().equalsIgnoreCase("getAllUsers")) {
            //  Get users list

            responseClass = "java.util.ArrayList";
            response = getUsers();
        } else if (request.getMethodName().equalsIgnoreCase("downloadCertificate")) {
            //  Get user certificate

            responseClass = "java.io.File";
            response = tmpFileManager.createTempFile(dumpCertificate(gson.fromJson(request.getArgs().get(0), String.class)));
        } else if (request.getMethodName().equalsIgnoreCase("downloadContainerCertificate")) {
            //  Get user certificate

            responseClass = "java.io.File";
            response = tmpFileManager.createTempFile(dumpContainerCertificate(gson.fromJson(request.getArgs().get(0), String.class)));
        } else if (request.getMethodName().equalsIgnoreCase("removeUser")) {
            //  Remove user
            removeUser(gson.fromJson(request.getArgs().get(0), String.class));

            responseClass = "java.lang.String";
            response = "";
        } else if (request.getMethodName().equalsIgnoreCase("removeComponent")) {
            //  Remove user
            removeComponent(gson.fromJson(request.getArgs().get(0), String.class));

            responseClass = "java.lang.String";
            response = "";
        } else if (request.getMethodName().equalsIgnoreCase("deleteContainerCertificate")) {
            //  Remove user
            removeCertificate(gson.fromJson(request.getArgs().get(0), String.class));

            responseClass = "java.lang.String";
            response = "";
        } else if (request.getMethodName().equalsIgnoreCase("getContainersState")) {
            responseClass = "java.lang.Object";
            response = getContainersState(request.getToken());
        } else if (request.getMethodName().equalsIgnoreCase("uploadComponent")) {
            responseClass = "java.lang.String";
            response = "";
            handleComponentUpload(tmpFileManager.getContent(gson.fromJson(request.getArgs().get(0), String.class), "jar"));
        } else if (request.getMethodName().equalsIgnoreCase("unloadComponent")) {
            responseClass = "java.lang.String";
            response = "";

            unloadComponent(gson.fromJson(request.getArgs().get(0), String.class),
                    gson.fromJson(request.getArgs().get(1), String.class));
        }

        return new Response(responseClass, gson.toJson(response), request.getSource());
    }

    private static String createBalancerScript(String scriptName, String scriptContent) {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        BalancerScriptManager register = injector.getInstance(BalancerScriptManager.class);

        return "" + register.createNewScript(scriptName, scriptContent);
    }

    private static void updateBalancerScript(String scriptID, String scriptName, String scriptContent) {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        BalancerScriptManager register = injector.getInstance(BalancerScriptManager.class);

        register.updateScript(scriptID, scriptName, scriptContent);
    }

    private static void setActiveBalancerScript(String scriptID) {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        BalancerScriptManager register = injector.getInstance(BalancerScriptManager.class);

        register.setActiveScript(scriptID);
    }

    private static void forkBalancerScript(String scriptID) {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        BalancerScriptManager register = injector.getInstance(BalancerScriptManager.class);

        register.forkScript(scriptID);
    }

    private static void deleteBalancerScript(String scriptID) {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        BalancerScriptManager register = injector.getInstance(BalancerScriptManager.class);

        register.removeScript(scriptID);
    }

    private static String testBalancerScript(String scriptID) {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        final ContainerRegister register = injector.getInstance(ContainerRegister.class);
        BalancerScriptManager scriptManager = injector.getInstance(BalancerScriptManager.class);
        final ComponentManager componentManager = injector.getInstance(ComponentManager.class);

        final ArrayList<ContainerInfo> workingContainers = new ArrayList<ContainerInfo>() {{
            addAll(register.getRegisteredContainers());
        }};
        final ArrayList<ComponentModel.Representation> deployedComponents = new ArrayList<ComponentModel.Representation>() {
            {
                addAll(componentManager.getComponents());
            }
        };

        String scriptContent = scriptManager.findScript(scriptID).getScriptContent();

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        Binding binding = new Binding();
        binding.setVariable("workingContainers", workingContainers);
        binding.setVariable("deployedComponents", deployedComponents);
        binding.setProperty("out", new PrintStream(output));
        GroovyShell shell = new GroovyShell(binding);

        shell.evaluate(scriptContent);

        return new String(output.toByteArray());
    }

    private static void unloadComponent(String containerID, String componentName) throws Exception {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        final ContainerRegister register = injector.getInstance(ContainerRegister.class);
        register.unloadComponent(containerID, componentName);
    }

    private static void generateCertificate() throws IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, InvalidKeyException, SignatureException, CryptoException, KeyStoreException, NoSuchProviderException, InvalidKeySpecException {
        ContainerCertificate certificate = new ContainerCertificate(0);
        certificate.getSerial();
        certificate.generateCertificate();

        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        CertificateManager manager = injector.getInstance(CertificateManager.class);

        manager.addContainerCertificate(certificate);
    }

    private static void removeUser(String serial) {
        UsersManager manager = getManager();

        manager.removeUser(serial);
    }

    private static void removeComponent(String componentName) throws IOException {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        ComponentManager componentManager = injector.getInstance(ComponentManager.class);

        componentManager.removeComponent(componentName);

        runBalancer(componentManager);
    }

    private static void removeCertificate(String serial) {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        CertificateManager manager = injector.getInstance(CertificateManager.class);

        manager.removeCertificate(serial);
    }

    private static void handleComponentUpload(File component) throws ParserConfigurationException, SAXException, IOException {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        ComponentManager componentManager = injector.getInstance(ComponentManager.class);

        componentManager.deployComponent(component);
        runBalancer(componentManager);

        component.delete();
    }

    private static Object getContainersState(String requestor) {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        ContainerRegister register = injector.getInstance(ContainerRegister.class);
        List<ContainerInfo> systemState = register.getRegisteredContainers();
        List<ContainerInfo.Representation> representations = new ArrayList<ContainerInfo.Representation>();

        for (ContainerInfo info : systemState) {
            representations.add(info.toRepresentation(requestor));
        }

        return representations;
    }

    private static File dumpCertificate(String userSerial) throws IOException {
        BigInteger serial = new BigInteger(userSerial);

        UsersManager manager = getManager();
        UserModel currentUser = manager.findUser(serial);

        File tempFile = File.createTempFile("tempfile", ".tmp");
        currentUser.writeCertificateToFile(tempFile);

        return tempFile;
    }

    private static File dumpContainerCertificate(String userSerial) throws IOException {
        BigInteger serial = new BigInteger(userSerial);

        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        CertificateManager manager = injector.getInstance(CertificateManager.class);
        ContainerCertificate currentUser = manager.findCertificate(serial);

        File tempFile = File.createTempFile("tempfile", ".tmp");
        currentUser.writeCertificateToFile(tempFile);

        return tempFile;
    }

    private static List<UserRepresentation> getUsers() {
        UsersManager manager = getManager();

        return manager.getAllUsers();
    }

    private static List<CertificateRepresentation> getCerts() {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        CertificateManager manager = injector.getInstance(CertificateManager.class);

        return manager.getCertificates();
    }

    private static List<ContainerInfo> getContainers() {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        final ContainerRegister register = injector.getInstance(ContainerRegister.class);

        return register.getRegisteredContainers();
    }

    private static Set<ComponentModel.Representation> getComponents() {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        final ComponentManager register = injector.getInstance(ComponentManager.class);

        return register.getComponents();
    }

    private static void createUser(String username, String role) throws NoSuchAlgorithmException, CertificateException, NoSuchProviderException, InvalidKeyException, SignatureException, IOException, KeyStoreException, UnrecoverableKeyException, InvalidKeySpecException, CryptoException {
        UserModel user = new UserModel(username);
        user.setRole(role);
        user.getSerial();
        user.generateCertificate();

        UsersManager manager = getManager();

        manager.addUser(user);
    }

    private static UsersManager getManager() {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        return injector.getInstance(UsersManager.class);
    }

    private static void runBalancer(final ComponentManager componentManager) {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        Balancer balancer = injector.getInstance(Balancer.class);
        final ContainerRegister register = injector.getInstance(ContainerRegister.class);

        balancer.balance(new ArrayList<ContainerInfo>() {{
                             addAll(register.getRegisteredContainers());
                         }}, new ArrayList<ComponentModel.Representation>() {{
                             addAll(componentManager.getComponents());
                         }}
        );
    }
}

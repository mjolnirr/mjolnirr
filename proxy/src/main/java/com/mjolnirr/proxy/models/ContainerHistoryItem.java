package com.mjolnirr.proxy.models;

import com.mjolnirr.lib.msg.ContainerInfo;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 13:16
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "containers_history")
public class ContainerHistoryItem {
    private static final int TYPE_CONNECT = 0;
    private static final int TYPE_DISCONNECT = 1;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "info_id")           private int infoID;
    @Column(name = "host")              private String hostName;
    @Column(name = "container_id")      private String containerID;
    @Column(name = "time")              private long time;
    @Column(name = "type")              private int type;

    public ContainerHistoryItem(ContainerInfo info) {
        this.hostName = info.getHostName();
        this.containerID = info.getContainerID();
        this.time = System.currentTimeMillis();

        if (info.isConnect()) {
            type = TYPE_CONNECT;
        } else {
            type = TYPE_DISCONNECT;
        }
    }
}

package com.mjolnirr.proxy.models;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 13:16
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "balancer_scripts")
public class BalancerScriptModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "script_id")
    private int balancerScriptID;
    @Column(name = "script_name")
    private String scriptName;
    @Column(name = "is_active")
    private boolean active;
    @Column(name = "is_modifiable")
    private boolean modifiable;
    @Column(length = 5 * 1024 * 1024, name = "script_content")
    private String scriptContent;

    public BalancerScriptModel() {
    }

    public BalancerScriptModel(String scriptName, String scriptContent, boolean modifiable) {
        this.scriptName = scriptName;
        this.scriptContent = scriptContent;
        this.modifiable = modifiable;
    }

    public int getScriptID() {
        return balancerScriptID;
    }

    public void setScriptID(int scriptID) {
        this.balancerScriptID = scriptID;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getScriptContent() {
        return scriptContent;
    }

    public void setScriptContent(String scriptContent) {
        this.scriptContent = scriptContent;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean isActive) {
        this.active = isActive;
    }

    public boolean getModifiable() {
        return modifiable;
    }

    public void setModifiable(boolean modifiable) {
        this.modifiable = modifiable;
    }
}

package com.mjolnirr.proxy.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.mjolnirr.lib.component.ComponentInterface;
import com.mjolnirr.lib.exceptions.ManifestParsingException;
import com.mjolnirr.lib.exceptions.WrongSuperclassException;
import org.apache.commons.lang.WordUtils;
import org.hibernate.id.CompositeNestedGeneratedValueGenerator;

import javax.persistence.*;
import java.net.MalformedURLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 13:16
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "components")
public class ComponentModel {
    @Id
    @Column(name = "component_name")    private String componentName;
    @Column(name = "jar_name")          private String jarName;
    @Column(name = "is_deployed")       private boolean isDeployed;
    @Column(name = "is_module")         private boolean isModule;
    @Column(length = 5 * 1024 * 1024, name = "interface")
    private String interfaceString;

    @Transient
    private ComponentInterface componentInterface;
    @Transient
    private Gson serializer;

    public ComponentModel() {
        this.serializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();;
    }

    public ComponentModel(String jarName, String componentName) throws WrongSuperclassException, ManifestParsingException, MalformedURLException {
        reinitialize(jarName, componentName);
    }

    public void reinitialize(String jarName, String componentName) throws MalformedURLException {
        this.jarName = jarName;
        this.componentName = componentName;
        this.serializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();;

//        setComponentInterface(new ComponentInterface(jarName));
        List<ComponentInterface> components = ComponentInterface.parsePackage(jarName);

        for (ComponentInterface component : components) {
            if (component.getComponentName().equalsIgnoreCase(componentName)) {
                setComponentInterface(component);
            }
        }
    }

    public boolean isModule() {
        return isModule;
    }

    public void setModule(boolean isModule) {
        this.isModule = isModule;
    }

    public boolean isDeployed() {
        return isDeployed;
    }

    public void setDeployed(boolean deployed) {
        isDeployed = deployed;
    }

    public String getComponentName() {
        return componentName;
    }

    public String getJarName() {
        return jarName;
    }

    public ComponentInterface getComponentInterface() {
        if (componentInterface == null) {
            componentInterface = serializer.fromJson(interfaceString, ComponentInterface.class);
        }

        return componentInterface;
    }

//    public Representation toRepresentation() {
//        return new Representation(componentName);
//    }

    public void setComponentInterface(ComponentInterface componentInterface) {
        this.componentInterface = componentInterface;
        this.interfaceString = serializer.toJson(componentInterface);
    }

    public static class Representation {
        private String componentName;
        private String displayName;
        private int minInstancesCount;
        private int maxInstancesCount;
        private long memoryVolume;

        public Representation(ComponentModel model) {
            componentName = model.getComponentName();
            displayName = WordUtils.capitalize(model.getComponentName());

            minInstancesCount = model.getComponentInterface().getMinInstancesCount();
            maxInstancesCount = model.getComponentInterface().getMaxInstancesCount();
            memoryVolume = model.getComponentInterface().getMemoryVolume();
        }

        public String getComponentName() {
            return componentName;
        }

        public void setComponentName(String componentName) {
            this.componentName = componentName;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public int getMinInstancesCount() {
            return minInstancesCount;
        }

        public void setMinInstancesCount(int minInstancesCount) {
            this.minInstancesCount = minInstancesCount;
        }

        public int getMaxInstancesCount() {
            return maxInstancesCount;
        }

        public void setMaxInstancesCount(int maxInstancesCount) {
            this.maxInstancesCount = maxInstancesCount;
        }

        public long getMemoryVolume() {
            return memoryVolume;
        }

        public void setMemoryVolume(long memoryVolume) {
            this.memoryVolume = memoryVolume;
        }
    }
}

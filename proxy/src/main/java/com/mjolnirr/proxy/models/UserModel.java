package com.mjolnirr.proxy.models;

import javax.persistence.*;
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

import com.google.inject.Injector;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.lib.security.CertificatesGenerator;
import com.mjolnirr.proxy.Main;
import com.mjolnirr.proxy.ProxyModule;
import com.mjolnirr.proxy.lib.UsersManager;
import org.bouncycastle.crypto.CryptoException;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 13:16
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "users")
public class UserModel {
    private static final int SERIAL_LENGTH = 64;

    @Id
    @Column(precision = SERIAL_LENGTH, scale = 2, name = "serial")
    private BigInteger serial;
    @Column(name = "username")
    private String username;
    @Column(name = "role")
    private String role;
    @Lob
    @Column(length = 5 * 1024 * 1024, name = "certificate")
    private byte[] certificate;

    public UserModel() {

    }

    public UserModel(String username) {
        this.username = username;
        generateSerial();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void generateCertificate() throws NoSuchProviderException, NoSuchAlgorithmException, CertificateException, SignatureException, InvalidKeyException, IOException, KeyStoreException, UnrecoverableKeyException, CryptoException, InvalidKeySpecException {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        PropertiesReader properties = injector.getInstance(PropertiesReader.class);

        String keystoreLocation = properties.getConfig().getString(Main.KEYSTORE_KEY);
        String keystorePassword = properties.getConfig().getString(Main.KEYSTORE_PASSWORD_KEY);
        String truststoreLocation = properties.getConfig().getString(Main.TRUSTSTORE_KEY);
        String truststorePassword = properties.getConfig().getString(Main.TRUSTSTORE_PASSWORD_KEY);

        certificate = new CertificatesGenerator(keystoreLocation, keystorePassword, truststoreLocation, truststorePassword)
                .createClientCertificate(keystoreLocation, keystorePassword, CertificatesGenerator.SERVER_ALIAS, username, false, "localhost", 30 * 12, "", "PKCS12", serial);
    }

    private static InputStream fullStream(String fname) throws IOException {
        FileInputStream fis = new FileInputStream(fname);
        DataInputStream dis = new DataInputStream(fis);
        byte[] bytes = new byte[dis.available()];
        dis.readFully(bytes);
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        return bais;
    }

    private void generateSerial() {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        UsersManager manager = injector.getInstance(UsersManager.class);

        do {
            serial = new BigInteger(SERIAL_LENGTH, new Random());
        } while (!manager.isSerialUnique(serial));
    }

    public void writeCertificateToFile(File pem) throws IOException {
        if (!pem.exists()) {
            pem.createNewFile();
        }

        FileOutputStream out = new FileOutputStream(pem);
        out.write(certificate);
        out.flush();
    }

    public BigInteger getSerial() {
        return serial;
    }

    public void setSerial(BigInteger serial) {
        this.serial = serial;
    }

    public byte[] getCertificate() {
        return certificate;
    }

    public boolean isAdmin() {
        return role.equalsIgnoreCase("admin");
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public UserRepresentation toRepresentation() {
        return new UserRepresentation(username, serial, role);
    }
}

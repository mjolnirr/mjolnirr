package com.mjolnirr.proxy.lib;

import com.google.inject.Key;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 02.12.13
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
public interface JacksonMixin {
    @JsonIgnore
    <V> Key<V> getRoot();
}

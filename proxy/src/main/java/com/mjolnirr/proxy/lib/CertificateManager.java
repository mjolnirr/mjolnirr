package com.mjolnirr.proxy.lib;

import com.mjolnirr.lib.models.CertificateRepresentation;
import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.proxy.models.ContainerCertificate;
import com.mjolnirr.proxy.models.UserModel;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by sk_ on 3/12/14.
 */
public interface CertificateManager {
    public List<CertificateRepresentation> getCertificates();

    void addContainerCertificate(ContainerCertificate certificate);

    ContainerCertificate findCertificate(BigInteger serial);

    void removeCertificate(String serial);
}

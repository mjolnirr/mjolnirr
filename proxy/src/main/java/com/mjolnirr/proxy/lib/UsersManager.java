package com.mjolnirr.proxy.lib;

import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.proxy.models.UserModel;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by sk_ on 1/23/14.
 */
public interface UsersManager {
    void addUser(UserModel user);

    UserModel findUser(BigInteger userSerial);

    List<UserRepresentation> getUsers();

    List<UserRepresentation> getAdmins();

    boolean isSerialUnique(BigInteger serial);

    void removeUser(String serial);

    List<UserRepresentation> getAllUsers();
}

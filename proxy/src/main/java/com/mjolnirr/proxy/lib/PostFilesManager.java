package com.mjolnirr.proxy.lib;

import com.google.inject.Inject;
import com.mjolnirr.lib.exceptions.MjolnirrException;
import com.mjolnirr.lib.properties.PropertiesReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by sk_ on 1/31/14.
 */
public interface PostFilesManager {
    String createTempFile(String content) throws IOException, MjolnirrException;

    String createTempFile(File content) throws IOException;

    File getContent(String filename) throws IOException;

    File getContent(String filename, String extension) throws IOException;
}

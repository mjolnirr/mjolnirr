package com.mjolnirr.proxy.lib;

import com.google.inject.Inject;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.util.FileHelper;
import com.mjolnirr.lib.util.JarReader;
import com.mjolnirr.proxy.models.BalancerScriptModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by sk_ on 3/24/14.
 */
public class BalancerScriptManagerImpl implements BalancerScriptManager {
    private SessionFactory sessionFactory;

    public static String PREFERRED_BALANCER = "Knapsack";

    @Inject
    BalancerScriptManagerImpl() {
        sessionFactory = MjolnirrSessionFactory.getSessionFactory();

        //  Bootstrap the unmodifiable built-in scripts

        if (getAllScripts().size() == 0) {
            // Database is empty
            // Read all the static scripts...
            Map<String, File> bootstrap = JarReader.getFilesFromDirectory(
                    new File(BalancerScriptManager.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsolutePath(),
                    "bootstrap/balancer_scripts"
            );

            // And write each to database
            for (String scriptName : bootstrap.keySet()) {
                try {
                    int scriptID = createNewBootstrapScript(scriptName, FileHelper.readFile(bootstrap.get(scriptName)));

                    if (scriptName.equalsIgnoreCase(PREFERRED_BALANCER)) {
                        setActiveScript("" + scriptID);
                    }
                } catch (IOException e) {
                    LoggerFactory.getLogger().fatal("Failed to bootstrap script", e);
                }
            }
        }
    }

    BalancerScriptManagerImpl(int d) {
        sessionFactory = MjolnirrSessionFactory.getSessionFactory();
    }

    @Override
    public int createNewScript(String scriptName, String scriptContent) {
        return createNewScript(scriptName, scriptContent, true);
    }


    @Override
    public int createNewBootstrapScript(String scriptName, String scriptContent) {
        return createNewScript(scriptName, scriptContent, false);
    }

    private int createNewScript(String scriptName, String scriptContent, boolean isModifiable) {
        Session session = sessionFactory.openSession();
        BalancerScriptModel model = new BalancerScriptModel(scriptName, scriptContent, isModifiable);

        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.save(model);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return model.getScriptID();
    }

    @Override
    public List<BalancerScriptModel> getAllScripts() {
        List<BalancerScriptModel> result = new ArrayList<BalancerScriptModel>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List users = session.createSQLQuery("SELECT * FROM balancer_scripts").addEntity(BalancerScriptModel.class).list();
            for (Iterator iterator = users.iterator(); iterator.hasNext(); ) {
                BalancerScriptModel description = (BalancerScriptModel) iterator.next();

                result.add(description);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }

    @Override
    public String getCurrentActiveScript() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List users = session.createSQLQuery("SELECT * FROM balancer_scripts WHERE is_active = TRUE").addEntity(BalancerScriptModel.class).list();
            for (Iterator iterator = users.iterator(); iterator.hasNext(); ) {
                BalancerScriptModel description = (BalancerScriptModel) iterator.next();

                return description.getScriptContent();
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return "";
    }

    @Override
    public BalancerScriptModel findScript(String scriptID) {
        Session session = sessionFactory.openSession();

        int scriptIDNum = Integer.parseInt(scriptID);

        session.beginTransaction();
        BalancerScriptModel description = (BalancerScriptModel) session.get(BalancerScriptModel.class, scriptIDNum);
        session.getTransaction().commit();

        return description;
    }

    @Override
    public void removeScript(String scriptID) {
        Session session = sessionFactory.openSession();

        int scriptIDNum = Integer.parseInt(scriptID);

        session.beginTransaction();
        BalancerScriptModel description = (BalancerScriptModel) session.get(BalancerScriptModel.class, scriptIDNum);

        if (description.getModifiable()) {
            session.delete(description);
        }
        session.getTransaction().commit();
    }

    @Override
    public void updateScript(String scriptID, String scriptName, String scriptContent) {
        Session session = sessionFactory.openSession();

        Transaction transaction = null;

        try {
            BalancerScriptModel desc = findScript(scriptID);
            if (desc.getModifiable()) {
                desc.setScriptName(scriptName);
                desc.setScriptContent(scriptContent);
            }

            transaction = session.beginTransaction();

            session.update(desc);

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void setActiveScript(String scriptID) {
        Session session = sessionFactory.openSession();

        Transaction transaction = null;

        try {
            BalancerScriptModel desc = findScript(scriptID);

            desc.setActive(true);

            transaction = session.beginTransaction();

            session.createSQLQuery("UPDATE balancer_scripts SET is_active = FALSE").executeUpdate();
            session.update(desc);

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public int forkScript(String scriptID) {
        BalancerScriptModel parentScript = findScript(scriptID);

        return createNewScript(parentScript.getScriptName() + " fork", parentScript.getScriptContent());
    }
}

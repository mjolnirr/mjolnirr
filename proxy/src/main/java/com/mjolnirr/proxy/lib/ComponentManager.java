package com.mjolnirr.proxy.lib;

import com.mjolnirr.lib.component.ComponentInterface;
import com.mjolnirr.proxy.models.ComponentModel;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 20:49
 * To change this template use File | Settings | File Templates.
 */
public interface ComponentManager {
    void deployComponent(File jarLocation) throws IOException, ParserConfigurationException, SAXException;

    void checkDeployment() throws FileNotFoundException;

    ComponentInterface getComponentInterface(String componentName);

    Set<String> getComponentNames();

    Set<ComponentModel.Representation> getComponents();

    Set<ComponentModel.Representation> getApplications();

    void removeComponent(String componentName) throws IOException;
}

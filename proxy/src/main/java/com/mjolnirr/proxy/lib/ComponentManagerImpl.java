package com.mjolnirr.proxy.lib;

import com.google.inject.Inject;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.component.ComponentInterface;
import com.mjolnirr.lib.exceptions.ManifestParsingException;
import com.mjolnirr.lib.exceptions.WrongSuperclassException;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.proxy.models.ComponentModel;
import com.mjolnirr.proxy.models.UserModel;
import com.mjolnirr.proxy.rest.InjectServerResource;
import org.apache.commons.io.FileUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 20:52
 * To change this template use File | Settings | File Templates.
 */
public class ComponentManagerImpl implements ComponentManager {
    private static final String JARS_PATH_KEY = "jars_path";
    public static final String CONFIG_ASSETS_PATH = "assets_path";
    protected static final int BUFFER = 2048;

    /**
     * Files to be excluded from static folder. Must be filled in lower case
     */
    List<String> badNames = new ArrayList<String>() {{
        //  TODO: add all files that couldn't be served as static
        add("pom.xml");
        add("pom.properties");
        add("manifest.mf");
    }};

    private SessionFactory sessionFactory;
    private Map<String, ComponentModel> deployedComponents;
    //    private Map<String, String> deployedApplications;
    private File assetsDir;

    @Inject
    private PropertiesReader propertiesReader;

    @Inject
    ComponentManagerImpl(PropertiesReader propertiesReader) {
        sessionFactory = MjolnirrSessionFactory.getSessionFactory();
        deployedComponents = new HashMap<String, ComponentModel>();
//        deployedApplications = new HashMap<String, String>();
        assetsDir = new File(propertiesReader.getConfig().getString(CONFIG_ASSETS_PATH));

        if (!assetsDir.exists()) {
            assetsDir.mkdirs();
        }

        try {
            loadComponentsFromDatabase();
        } catch (WrongSuperclassException e) {
            e.printStackTrace();
        } catch (ManifestParsingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deployComponent(File jarLocation) throws IOException, ParserConfigurationException, SAXException {
        List<ComponentInterface> componentNames = getComponentName(jarLocation);

        for (ComponentInterface component : componentNames) {
            if (component.getComponentName().equalsIgnoreCase(InjectServerResource.ADMIN_APPLICATION_NAME)) {
                LoggerFactory.getLogger().warn("Admin application rewrite attempt!");

                return;
            }

            File jarsDirectory = new File(propertiesReader.getConfig().getString(JARS_PATH_KEY));

            if (!(jarsDirectory.exists())) {
                jarsDirectory.mkdirs();
            }

            System.out.println("Copying from " + jarLocation + " to " + jarsDirectory);
            FileUtils.copyFileToDirectory(jarLocation, jarsDirectory);
            File newJarLocation = new File(jarsDirectory, jarLocation.getName());

            //  Save component info to database
            try {
                LoggerFactory.getLogger().info("Got component " + component.getComponentName());

                saveToDatabase(component.getComponentName(), newJarLocation.getAbsolutePath(), true, component.isModule());
//                deployedComponents.put(component.getComponentName(), newJarLocation.getAbsolutePath());
                ComponentModel componentModel = new ComponentModel(newJarLocation.getAbsolutePath(), component.getComponentName());
                componentModel.setModule(component.isModule());
                componentModel.setComponentInterface(component);

                deployedComponents.put(component.getComponentName(), componentModel);

//                if (!component.isModule()) {
//                    deployedApplications.put(component.getComponentName(), newJarLocation.getAbsolutePath());
//                }

                checkDeployment();
            } catch (Exception e) {
                LoggerFactory.getLogger().error("Manifest not found or couldn't be processed. EXTERMINATE! EXTERMINATE! EXTERMINATE!", e);
            }
        }
    }

    private List<ComponentInterface> getComponentName(File jarLocation) throws ParserConfigurationException, SAXException, MalformedURLException {
        return ComponentInterface.parsePackage(jarLocation.getAbsolutePath());
    }

    @Override
    public void checkDeployment() throws FileNotFoundException {
        Set<String> components = deployedComponents.keySet();

        for (String componentName : components) {
            File componentDir = new File(assetsDir, componentName);

            LoggerFactory.getLogger().info("Deployment from " + deployedComponents.get(componentName));

            componentDir.mkdirs();
            deployComponentJAR(new File(deployedComponents.get(componentName).getJarName()), componentDir);
        }
    }

    @Override
    public ComponentInterface getComponentInterface(String componentName) {
        if (findComponentInfo(componentName) == null) {
            System.out.println("COMPONENT INFO IS NULL");
        }

        return findComponentInfo(componentName).getComponentInterface();
    }

    @Override
    public Set<String> getComponentNames() {
        return deployedComponents.keySet();
    }

    @Override
    public Set<ComponentModel.Representation> getComponents() {
        Set<ComponentModel.Representation> result = new HashSet<ComponentModel.Representation>();

        for (String name : deployedComponents.keySet()) {
            result.add(new ComponentModel.Representation(deployedComponents.get(name)));
        }

        return result;
    }

    @Override
    public Set<ComponentModel.Representation> getApplications() {
        Set<ComponentModel.Representation> result = new HashSet<ComponentModel.Representation>();

        for (String name : deployedComponents.keySet()) {
            if (!deployedComponents.get(name).isModule()) {
                result.add(new ComponentModel.Representation(deployedComponents.get(name)));
            }
        }

        return result;
    }

    @Override
    public void removeComponent(String componentName) throws IOException {
        File componentDir = new File(assetsDir, componentName);

        if (componentDir.exists()) {
            FileUtils.deleteDirectory(componentDir);
        }

        ComponentModel component = findComponentInfo(componentName);

        File componentJar = new File(component.getJarName());
        if (componentJar.exists()) {
            componentJar.delete();
        }

        Session session = sessionFactory.openSession();

        session.beginTransaction();
        ComponentModel description = (ComponentModel) session.get(ComponentModel.class, componentName);
        session.delete(description);
        session.getTransaction().commit();


        deployedComponents.remove(componentName);
    }

    private void loadComponentsFromDatabase() throws WrongSuperclassException, ManifestParsingException, MalformedURLException {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List employees = session.createSQLQuery("SELECT * FROM components WHERE is_deployed = TRUE").addEntity(ComponentModel.class).list();
            for (Iterator iterator = employees.iterator(); iterator.hasNext(); ) {
                ComponentModel description = (ComponentModel) iterator.next();

//                deployedComponents.put(description.getComponentName(), description.getJarName());
                deployedComponents.put(description.getComponentName(), description);

//                if (!description.isModule()) {
//                    deployedApplications.put(description.getComponentName(), description.getJarName());
//                }
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        LoggerFactory.getLogger().info("" + deployedComponents.size() + " loaded from database");
    }

    private void saveToDatabase(String componentName, String jarLocation, boolean isDeployment, boolean isModule) {
        Session session = sessionFactory.openSession();

        Transaction transaction = null;

        try {
            ComponentModel componentInfo = findComponentInfo(componentName);

            transaction = session.beginTransaction();

            if (componentInfo == null) {
                try {
                    componentInfo = new ComponentModel(jarLocation, componentName);
                    componentInfo.setDeployed(isDeployment);
                    componentInfo.setModule(isModule);
                    session.save(componentInfo);
                } catch (Exception e) {
                    LoggerFactory.getLogger().fatal("Failed to parse manifest", e);
                }
            } else {
                try {
                    componentInfo.setDeployed(isDeployment);
                    componentInfo.setModule(isModule);
                    componentInfo.reinitialize(jarLocation, componentName);
                    session.update(componentInfo);
                } catch (Exception e) {
                    LoggerFactory.getLogger().fatal("Failed to parse manifest", e);
                }
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }

            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    private ComponentModel findComponentInfo(String componentName) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        ComponentModel componentModel = (ComponentModel) session.get(ComponentModel.class, componentName);
        session.getTransaction().commit();

        session.close();

        return componentModel;
    }

    private void deployComponentJAR(File originalJar, File destinationDir) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(originalJar);

        try {
            BufferedOutputStream destination;
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(inputStream));

            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                if ((entry.getName().endsWith(".class")) || isBad(entry.getName())) {
                    continue;
                }

                if (entry.isDirectory()) {
                    File dirFile = new File(destinationDir, entry.getName());

                    deleteDir(dirFile);
                    (dirFile).mkdir();
                    continue;
                }

                int count;
                byte data[] = new byte[BUFFER];
                FileOutputStream fos = new FileOutputStream(new File(destinationDir, entry.getName()));
                destination = new BufferedOutputStream(fos, BUFFER);

                while ((count = zis.read(data, 0, BUFFER)) != -1) {
                    destination.write(data, 0, count);
                }

                destination.flush();
                destination.close();
            }

            zis.close();

            //  Place original JAR there to use as classpath
            FileUtils.copyFile(originalJar, new File(destinationDir, "origJar.jar"));
//            FileOutputStream outputStream = new FileOutputStream(new File(destinationDir, "origJar.jar"));

//            int read = 0;
//            byte[] bytes = new byte[1024];

//            inputStream.reset();
//            while ((read = inputStream.read(bytes)) != -1) {
//                outputStream.write(bytes, 0, read);
//            }
        } catch (java.io.IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        deleteEmptyFolders(destinationDir);
    }

    private boolean isBad(String filename) {
        String[] pathSegments = filename.split("/");

        return badNames.contains(pathSegments[pathSegments.length - 1].toLowerCase());
    }

    private static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    private static void deleteEmptyFolders(File folder) {
        File aStartingDir = folder;
        List<File> emptyFolders = new ArrayList<File>();
        findEmptyFoldersInDir(aStartingDir, emptyFolders);
        List<String> fileNames = new ArrayList<String>();
        for (File f : emptyFolders) {
            String s = f.getAbsolutePath();
            fileNames.add(s);
        }

        for (File f : emptyFolders) {
            f.delete();
        }
    }

    private static boolean findEmptyFoldersInDir(File folder, List<File> emptyFolders) {
        boolean isEmpty = false;
        File[] filesAndDirs = folder.listFiles();
        List<File> filesDirs = Arrays.asList(filesAndDirs);
        if (filesDirs.size() == 0) {
            isEmpty = true;
        }
        if (filesDirs.size() > 0) {
            boolean allDirsEmpty = true;
            boolean noFiles = true;
            for (File file : filesDirs) {
                if (!file.isFile()) {
                    boolean isEmptyChild = findEmptyFoldersInDir(file, emptyFolders);
                    if (!isEmptyChild) {
                        allDirsEmpty = false;
                    }
                }
                if (file.isFile()) {
                    noFiles = false;
                }
            }
            if (noFiles == true && allDirsEmpty == true) {
                isEmpty = true;
            }
        }
        if (isEmpty) {
            emptyFolders.add(folder);
        }
        return isEmpty;
    }
}

package com.mjolnirr.proxy.lib;

import com.google.inject.Inject;
import com.mjolnirr.lib.TypeHelper;
import com.mjolnirr.lib.exceptions.MjolnirrException;
import com.mjolnirr.lib.properties.PropertiesReader;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.math.BigInteger;
import java.util.Random;

/**
 * Created by sk_ on 1/31/14.
 */
public class PostFilesManagerImpl implements PostFilesManager {
    private static final String TMP_PATH_KEY = "tmp_path";
    private final File tempPath;
    @Inject
    private PropertiesReader propertiesReader;

    @Inject
    PostFilesManagerImpl(PropertiesReader propertiesReader) {
        tempPath = new File(propertiesReader.getConfig().getString(TMP_PATH_KEY));

        if (!tempPath.exists()) {
            tempPath.mkdirs();
        }
    }

    PostFilesManagerImpl(File tempPath) {
        this.tempPath = tempPath;

        if (!tempPath.exists()) {
            tempPath.mkdirs();
        }
    }

    @Override
    public String createTempFile(File content) throws IOException {
        String randomName = new BigInteger(256, new Random()).toString();
        FileUtils.moveFile(content, new File(tempPath, randomName));

        return randomName;
    }

    @Override
    public String createTempFile(String content) throws IOException, MjolnirrException {
        File tmpFile = TypeHelper.toFile(content);
        String randomName = new BigInteger(256, new Random()).toString();
        FileUtils.moveFile(tmpFile, new File(tempPath, randomName));

        return randomName;
    }

    @Override
    public File getContent(String filename) throws IOException {
        return new File(tempPath, filename);
    }

    @Override
    public File getContent(String filename, String extension) throws IOException {
        File targetFile = new File(tempPath, filename);
        File result = File.createTempFile("hive", "." + extension);

        FileInputStream inStream = new FileInputStream(targetFile);
        FileOutputStream outStream = new FileOutputStream(result);

        byte[] buffer = new byte[1024];

        int length;
        //copy the file content in bytes
        while ((length = inStream.read(buffer)) > 0) {

            outStream.write(buffer, 0, length);

        }

        inStream.close();
        outStream.close();

        targetFile.delete();

        return result;
    }
}

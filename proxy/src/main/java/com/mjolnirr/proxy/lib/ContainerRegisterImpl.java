package com.mjolnirr.proxy.lib;

import com.google.inject.Inject;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.proxy.models.ContainerHistoryItem;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 13:35
 * To change this template use File | Settings | File Templates.
 */
public class ContainerRegisterImpl implements ContainerRegister {
    private SessionFactory sessionFactory;
    private Cache registeredContainers;
    private Runnable onUpdate;

    @Inject
    ContainerRegisterImpl() {
        sessionFactory = MjolnirrSessionFactory.getSessionFactory();
//        registeredContainers = new ArrayList<ContainerInfo>();
        CacheManager.getInstance().addCache("containers");
        registeredContainers = CacheManager.getInstance().getCache("containers");

        new Thread() {
            public void run() {
                while (true) {
                    if (registeredContainers.getSize() > registeredContainers.getKeysWithExpiryCheck().size()) {
                        //  Expired elements found
                        registeredContainers.evictExpiredElements();
                        registeredContainers.flush();

                        if (onUpdate != null) {
                            onUpdate.run();
                        }
                    }

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        LoggerFactory.getLogger().error("Error happened while container cache checking", e);
                    }
                }
            }
        }.start();
    }

    @Override
    public void setOnUpdate(Runnable callback) {
        onUpdate = callback;
    }

    @Override
    public void addContainer(ContainerInfo info) {
        registeredContainers.put(new Element(info, info, (int) 1.5 * ContainerInfo.REFRESH_TIMEOUT, (int) 1.5 * ContainerInfo.REFRESH_TIMEOUT));
        saveToHistory(info);

        LoggerFactory.getLogger().warn("Container " + info + " added");
        LoggerFactory.getLogger().warn("Available memory: " + info.getProperties().getMemoryQuota());
    }

    @Override
    public void removeContainer(ContainerInfo info) {
        if (!registeredContainers.isKeyInCache(info)) {
            LoggerFactory.getLogger().warn("Container " + info + " not found");
            return;
        }

        registeredContainers.remove(info);
        saveToHistory(info);

        LoggerFactory.getLogger().warn("Container " + info + " removed");
    }

    private void saveToHistory(ContainerInfo info) {
        Session session = sessionFactory.openSession();

        Transaction transaction = null;

        try {
            ContainerHistoryItem historyItem = new ContainerHistoryItem(info);

            transaction = session.beginTransaction();
            session.save(historyItem);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }

            LoggerFactory.getLogger().error("Failed to save container history", e);
        } finally {
            session.close();
        }
    }

    @Override
    public List<ContainerInfo> getRegisteredContainers() {
        return registeredContainers.getKeysWithExpiryCheck();
    }

    @Override
    public List<ContainerHistoryItem> getHistory() {
        return null;
    }

    @Override
    public void updateContainer(ContainerInfo registrationObject) {
        addContainer(registrationObject);
    }

    @Override
    public ContainerInfo getContainer(String containerName) {
        List<ContainerInfo> containers = getRegisteredContainers();

        for (ContainerInfo info : containers) {
            if (info.getContainerID().equals(containerName)) {
                return info;
            }
        }
        return null;
    }

    @Override
    public void unloadComponent(String containerID, String componentName) throws Exception {
        getContainer(containerID).unloadComponent(componentName).commit();
    }
}

package com.mjolnirr.proxy.lib;

import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.proxy.models.ContainerHistoryItem;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 03.12.13
 * Time: 13:23
 * To change this template use File | Settings | File Templates.
 */
public interface ContainerRegister {
    public void setOnUpdate(Runnable callback);

    public void addContainer(ContainerInfo info);

    public void removeContainer(ContainerInfo info);

    public List<ContainerInfo> getRegisteredContainers();

    public List<ContainerHistoryItem> getHistory();

    void updateContainer(ContainerInfo registrationObject);

    ContainerInfo getContainer(String containerName);

    void unloadComponent(String containerID, String componentName) throws Exception;
}

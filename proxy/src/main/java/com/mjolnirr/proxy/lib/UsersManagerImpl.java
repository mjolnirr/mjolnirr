package com.mjolnirr.proxy.lib;

import com.google.inject.Inject;
import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.proxy.models.UserModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by sk_ on 1/23/14.
 */
public class UsersManagerImpl implements UsersManager {
    private SessionFactory sessionFactory;

    @Inject
    UsersManagerImpl(PropertiesReader propertiesReader) {
        sessionFactory = MjolnirrSessionFactory.getSessionFactory();
    }

    public void addUser(UserModel user) {
        Session session = sessionFactory.openSession();
        BigInteger projectId = user.getSerial();

        Transaction transaction = null;

        try {
            UserModel desc = findUser(projectId);
            transaction = session.beginTransaction();

            if (desc == null) {
                session.save(user);
            } else {
                session.update(user);
            }

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) { transaction.rollback(); }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public UserModel findUser(BigInteger userSerial) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        UserModel description = (UserModel) session.get(UserModel.class, userSerial);
        session.getTransaction().commit();

        return description;
    }

    @Override
    public List<UserRepresentation> getUsers() {
        List<UserRepresentation> result = new ArrayList<UserRepresentation>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List users = session.createSQLQuery("SELECT * FROM users WHERE role != 'admin'").addEntity(UserRepresentation.class).list();
            for (Iterator iterator = users.iterator(); iterator.hasNext(); ) {
                UserRepresentation description = (UserRepresentation) iterator.next();
                description.setSerialString(description.getSerial().toString());

                result.add(description);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }

    @Override
    public List<UserRepresentation> getAllUsers() {
        List<UserRepresentation> result = new ArrayList<UserRepresentation>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List users = session.createSQLQuery("SELECT * FROM users ORDER BY role").addEntity(UserRepresentation.class).list();
            for (Iterator iterator = users.iterator(); iterator.hasNext(); ) {
                UserRepresentation description = (UserRepresentation) iterator.next();
                description.setSerialString(description.getSerial().toString());

                result.add(description);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }

    @Override
    public List<UserRepresentation> getAdmins() {
        List<UserRepresentation> result = new ArrayList<UserRepresentation>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List users = session.createSQLQuery("SELECT * FROM users WHERE role = 'admin'").addEntity(UserRepresentation.class).list();
            for (Iterator iterator = users.iterator(); iterator.hasNext(); ) {
                UserRepresentation description = (UserRepresentation) iterator.next();
                description.setSerialString(description.getSerial().toString());

                result.add(description);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }

    @Override
    public boolean isSerialUnique(BigInteger serial) {
        return findUser(serial) == null;
    }

    @Override
    public void removeUser(String serial) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        UserModel description = (UserModel) session.get(UserModel.class, new BigInteger(serial));
        session.delete(description);
        session.getTransaction().commit();
    }
}

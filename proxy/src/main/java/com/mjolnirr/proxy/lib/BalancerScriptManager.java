package com.mjolnirr.proxy.lib;

import com.mjolnirr.proxy.models.BalancerScriptModel;

import java.util.List;

/**
 * Created by sk_ on 3/24/14.
 */
public interface BalancerScriptManager {
    int createNewScript(String scriptName, String scriptContent);

    List<BalancerScriptModel> getAllScripts();

    String getCurrentActiveScript();

    BalancerScriptModel findScript(String scriptID);

    void removeScript(String scriptID);

    void updateScript(String scriptID, String scriptName, String scriptContent);

    void setActiveScript(String scriptID);

    int forkScript(String scriptID);

    int createNewBootstrapScript(String scriptName, String s);
}

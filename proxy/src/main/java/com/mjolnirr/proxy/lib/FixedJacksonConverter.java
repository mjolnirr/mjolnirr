package com.mjolnirr.proxy.lib;

import org.restlet.data.MediaType;
import org.restlet.engine.resource.VariantInfo;
import org.restlet.ext.jackson.JacksonConverter;
import org.restlet.ext.jackson.JacksonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 02.12.13
 * Time: 16:27
 * To change this template use File | Settings | File Templates.
 */
public class FixedJacksonConverter extends JacksonConverter {
    private static final VariantInfo VARIANT_JSON = new VariantInfo(MediaType.APPLICATION_JSON);

    @Override
    protected <T> JacksonRepresentation<T> create(MediaType mediaType, T source) {
        return new FixedJacksonRepresentation<T>(mediaType, source);
    }

    @Override
    protected <T> JacksonRepresentation<T> create(Representation source, Class<T> objectClass) {
        return new FixedJacksonRepresentation<T>(source, objectClass);
    }

    @Override
    public List<Class<?>> getObjectClasses(Variant source) {
        List<Class<?>> result = null;
        if (VARIANT_JSON.isCompatible(source)) {
            result = addObjectClass(result, Object.class);
            result = addObjectClass(result, FixedJacksonRepresentation.class);
        }
        return result;
    }
}

package com.mjolnirr.proxy.lib;

import com.google.inject.Inject;
import com.mjolnirr.lib.models.CertificateRepresentation;
import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.proxy.models.ContainerCertificate;
import com.mjolnirr.proxy.models.UserModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by sk_ on 3/12/14.
 */
public class CertificateManagerImpl implements CertificateManager {
    private SessionFactory sessionFactory;

    @Inject
    CertificateManagerImpl() {
        sessionFactory = MjolnirrSessionFactory.getSessionFactory();
    }

    @Override
    public void addContainerCertificate(ContainerCertificate user) {
        Session session = sessionFactory.openSession();
        BigInteger projectId = user.getSerial();

        Transaction transaction = null;

        try {
            ContainerCertificate desc = findCertificate(projectId);
            transaction = session.beginTransaction();

            if (desc == null) {
                session.save(user);
            } else {
                session.update(user);
            }

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) { transaction.rollback(); }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public List<CertificateRepresentation> getCertificates() {
        List<CertificateRepresentation> result = new ArrayList<CertificateRepresentation>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List users = session.createSQLQuery("SELECT * FROM container_certificates").addEntity(CertificateRepresentation.class).list();
            for (Iterator iterator = users.iterator(); iterator.hasNext(); ) {
                CertificateRepresentation description = (CertificateRepresentation) iterator.next();
                description.setSerialString(description.getSerial().toString());

                result.add(description);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }

    @Override
    public ContainerCertificate findCertificate(BigInteger userSerial) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        ContainerCertificate description = (ContainerCertificate) session.get(ContainerCertificate.class, userSerial);
        session.getTransaction().commit();

        return description;
    }

    @Override
    public void removeCertificate(String serial) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        ContainerCertificate description = (ContainerCertificate) session.get(ContainerCertificate.class, new BigInteger(serial));
        session.delete(description);
        session.getTransaction().commit();
    }
}

package com.mjolnirr.proxy.helpers;

/**
 * Created by sk_ on 6/2/14.
 */
public class FormatHelper {
    public String format(String formatString, Object ... objects) {
        return String.format(formatString, objects);
    }
}

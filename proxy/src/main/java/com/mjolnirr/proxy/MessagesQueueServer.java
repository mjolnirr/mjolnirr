package com.mjolnirr.proxy;

import org.hornetq.core.config.impl.FileConfiguration;
import org.hornetq.jms.server.JMSServerManager;
import org.hornetq.jms.server.impl.JMSServerManagerImpl;

/**
 * Messages server controller
 */
public class MessagesQueueServer {
    /**
     * Path to configuration file of the hornetQ (something like hornetq-conf.xml)
     */
    private String hornetConfigPath;

    /**
     * Path to configuration file of the JMS (may be null if hornetQ config is defined)
     */
    private String jmsConfigPath;

    /**
     * JMS Server accessor
     */
    private JMSServerManager jmsServerManager;

    /**
     * Instance of queue server implementation
     */
    private static MessagesQueueServer queueServer;

    static {
        queueServer = new MessagesQueueServer();
    }

    private MessagesQueueServer() {}

    /**
     * Accessor for instance of message server controls
     * @return
     */
    public static MessagesQueueServer getInstance() {
        return queueServer;
    }

    public void start() throws Exception {
        FileConfiguration configuration = new FileConfiguration();
        configuration.setConfigurationUrl(hornetConfigPath);
        configuration.start();

        org.hornetq.core.server.HornetQServer server = org.hornetq.core.server.HornetQServers.newHornetQServer(configuration);
        jmsServerManager = new JMSServerManagerImpl(server, jmsConfigPath);

        jmsServerManager.setContext(null);
        jmsServerManager.start();
    }

    public void stop() throws Exception {
        if (jmsServerManager != null) {
            jmsServerManager.stop();
            jmsServerManager = null;
        }
    }

    public void restart() throws Exception {
        stop();
        start();
    }

    public void setHornetConfigPath(String newHornetConfigPath) {
        this.hornetConfigPath = newHornetConfigPath;
    }

    public void setJmsConfigPath(String newJmsConfigPath) {
        this.jmsConfigPath = newJmsConfigPath;
    }
}

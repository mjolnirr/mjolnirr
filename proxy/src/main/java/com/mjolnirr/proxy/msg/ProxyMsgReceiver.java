package com.mjolnirr.proxy.msg;

import com.google.inject.Injector;
import com.mjolnirr.lib.Callback;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.msg.Communicator;
import com.mjolnirr.lib.msg.HornetCommunicator;
import com.mjolnirr.lib.msg.HornetSessionsRegister;
import com.mjolnirr.proxy.ProxyModule;
import org.hornetq.api.core.HornetQException;

/**
 * Implementation of messages receiver
 */
public class ProxyMsgReceiver extends Thread {
    private final HornetSessionsRegister register;
    private Communicator communicator;

    private String channel;
    /**
     * Flag shows that system need or needn't any messages
     */
    private boolean alive;

    Class callback;

    public <T extends Callback<String>> ProxyMsgReceiver(String channel, Class<T> callback) {
        communicator = new HornetCommunicator();
        this.channel = channel;
        this.callback = callback;

        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        register = injector.getInstance(HornetSessionsRegister.class);
    }

    /**
     * Notify receiver that system needn't any messages
     */
    public void kill() {
        alive = false;
    }

    @Override
    public void run() {
        alive = true;
        while (alive) {
            try {
                LoggerFactory.getLogger().info("Waiting for message on channel \"" + channel + "\".");
                HornetCommunicator.SessionObject currentSession = register.getReceiverSession(channel);
                communicator.receive(currentSession, channel, callback, new Communicator.AcknowledgeCallback() {
                    @Override
                    public boolean isAcknowledged(String componentName) {
                        return true;
                    }
                });
            } catch (Exception e) {
                String message = String.format("Can't receive message from channel \"%s\"!", channel);
                LoggerFactory.getLogger().error(message, e);
            }
        }
    }

}
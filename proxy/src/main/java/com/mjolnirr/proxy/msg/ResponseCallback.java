package com.mjolnirr.proxy.msg;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mjolnirr.lib.Callback;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.Response;
import com.mjolnirr.lib.msg.CallbackRegister;

public class ResponseCallback implements Callback<String> {
    private static Gson gson = new Gson();

    public ResponseCallback() {
    }

    @Override
    public void run(String result) {
        if (result != null) {
            try {
                Response response = gson.fromJson(result, Response.class);
                processResponse(response);
            } catch (JsonSyntaxException ex) {
                LoggerFactory.getLogger().warn("Can't process message. It is not json serialization of com.mjolnirr.lib.Response.");
            }
        }
    }

    private void processResponse(Response response) {
//        System.out.println("Received response with UID " + response.getOnResponse());
//        SemaphoreHelper.release(response.getToken());
        CallbackRegister.setSyncResponse(response.getOnResponse(), response);
    }
}
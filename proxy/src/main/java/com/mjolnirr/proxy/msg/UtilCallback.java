package com.mjolnirr.proxy.msg;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.mjolnirr.lib.Callback;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.lib.msg.Execution;
import com.mjolnirr.proxy.ProxyModule;
import com.mjolnirr.proxy.balancer.Balancer;
import com.mjolnirr.proxy.lib.ComponentManager;
import com.mjolnirr.proxy.lib.ContainerRegister;
import com.mjolnirr.lib.msg.UtilRequest;
import com.mjolnirr.proxy.models.ComponentModel;

import java.util.ArrayList;

public class UtilCallback implements Callback<String> {
    private static Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    @Inject
    private ContainerRegister register;

    @Inject
    private ComponentManager componentManager;

    @Inject
    private Balancer balancer;

    public UtilCallback() {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        injector.injectMembers(this);

        register.setOnUpdate(new Runnable() {
            @Override
            public void run() {
                runBalancer();
            }
        });
    }

    @Override
    public void run(String result) {
        if (result != null) {
            try {
                UtilRequest request = gson.fromJson(result, UtilRequest.class);

                if (request.isRegistration()) {
                    ContainerInfo registrationObject = request.getRegistrationObject();

                    if (registrationObject.isConnect()) {
                        LoggerFactory.getLogger().info("Container registration");

                        register.addContainer(request.getRegistrationObject());
                        runBalancer();
                    } else if (registrationObject.isStateUpdate()) {
                        LoggerFactory.getLogger().info("Container state update");

                        register.updateContainer(request.getRegistrationObject());
                    } else {
                        LoggerFactory.getLogger().info("Container unregistration");

                        register.removeContainer(request.getRegistrationObject());
                        runBalancer();
                    }
                } else if (request.isExecution()) {
                    LoggerFactory.getLogger().info("Changing container state");

                    Execution execution = request.getExecution();

                    if (execution.isStarting()) {
                        LoggerFactory.getLogger().info("Container " + execution.getContainerName() + " started execution of component " + execution.getComponentName());
                        register.getContainer(execution.getContainerName()).addRunningComponent(execution);
                    } else {
                        LoggerFactory.getLogger().info("Container " + execution.getContainerName() + " stopped execution of component " + execution.getComponentName());
                        register.getContainer(execution.getContainerName()).removeRunningComponent(execution);
                    }
                }
            } catch (JsonSyntaxException ex) {
                LoggerFactory.getLogger().error("Failed to parse message", ex);
            }
        }
    }

    private void runBalancer() {
        balancer.balance(new ArrayList<ContainerInfo>() {{
                             addAll(register.getRegisteredContainers());
                         }}, new ArrayList<ComponentModel.Representation>() {{
                             addAll(componentManager.getComponents());
                         }}
        );
    }
}
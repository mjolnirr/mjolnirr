package com.mjolnirr.proxy;

import com.mjolnirr.lib.ApplicationInstanceManager;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 11/19/13
 * Time: 3:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class WindowsServiceStopper {
    public static void main(String[] args) {
        if (!ApplicationInstanceManager.registerInstance(args, Main.PROXY_PORT)) {
            // instance already running.
            System.out.println("Signal passed to proxy.");
            System.exit(0);
        }

        System.exit(0);
    }
}

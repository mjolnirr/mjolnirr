package com.mjolnirr.proxy.balancer;

import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.proxy.models.ComponentModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.12.13
 * Time: 15:37
 * To change this template use File | Settings | File Templates.
 */
public class RoundRobinBalancerImpl implements Balancer {
    @Override
    public void balance(List<ContainerInfo> workingContainers, ArrayList<ComponentModel.Representation> deployedComponents) {
        LoggerFactory.getLogger().warn("Beginning");

        LoggerFactory.getLogger().info("Containers count: " + workingContainers.size());
        LoggerFactory.getLogger().info("Components count: " + deployedComponents.size());

        if (workingContainers.size() == 0) {
            LoggerFactory.getLogger().warn("No containers online");

            return;
        }

        for (ContainerInfo c : workingContainers) {
            for (String component : c.getLoadedComponents()) {
                c.unloadComponent(component);
            }
        }

        if ((workingContainers.size() == 0) || (deployedComponents.size() == 0)) {
            return;
        }

        int containerPointer = 0;
        int componentPointer = 0;

        if (deployedComponents.size() > workingContainers.size()) {
            for (componentPointer = 0; componentPointer < deployedComponents.size(); componentPointer++) {
                workingContainers.get(containerPointer).loadComponent(deployedComponents.get(componentPointer).getComponentName());

                containerPointer++;
                if (containerPointer == workingContainers.size()) {
                    containerPointer = 0;
                }
            }
        } else {
            for (containerPointer = 0; containerPointer < workingContainers.size(); containerPointer++) {
                workingContainers.get(containerPointer).loadComponent(deployedComponents.get(componentPointer).getComponentName());

                componentPointer++;
                if (componentPointer == deployedComponents.size()) {
                    componentPointer = 0;
                }
            }
        }

        try {
            for (ContainerInfo c : workingContainers) {
                c.commit();
            }
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Failed to commit balancing", e);

            for (ContainerInfo c : workingContainers) {
                c.rollback();
            }
        }
    }
}

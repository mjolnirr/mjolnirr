package com.mjolnirr.proxy.balancer;

import com.google.inject.Inject;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.proxy.lib.BalancerScriptManager;
import com.mjolnirr.proxy.models.ComponentModel;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sk_ on 3/11/14.
 */
public class ScriptBalancerImpl implements Balancer {
    @Inject
    private BalancerScriptManager scriptManager;

    @Override
    public void balance(List<ContainerInfo> workingContainers, ArrayList<ComponentModel.Representation> deployedComponents) {
        LoggerFactory.getLogger().info("Scripting balancer");

        //  Do some magic there...
        String scriptContent = scriptManager.getCurrentActiveScript();

        Binding binding = new Binding();
        binding.setVariable("workingContainers", workingContainers);
        binding.setVariable("deployedComponents", deployedComponents);
        GroovyShell shell = new GroovyShell(binding);

        shell.evaluate(scriptContent);

        workingContainers = (List<ContainerInfo>) binding.getVariable("workingContainers");

        //  And then commit it
        try {
            for (ContainerInfo c : workingContainers) {
                c.commit();
            }
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Failed to commit balancing", e);

            for (ContainerInfo c : workingContainers) {
                c.rollback();
            }
        }
    }
}

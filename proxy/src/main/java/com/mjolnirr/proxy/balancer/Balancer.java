package com.mjolnirr.proxy.balancer;

import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.proxy.models.ComponentModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.12.13
 * Time: 15:36
 * To change this template use File | Settings | File Templates.
 */
public interface Balancer {
    void balance(List<ContainerInfo> workingContainers, ArrayList<ComponentModel.Representation> deployedComponents);
}

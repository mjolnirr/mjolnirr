package com.mjolnirr.proxy.balancer;

import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.proxy.models.ComponentModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sk_ on 3/11/14.
 */
public class AllToAllBalancerImpl implements Balancer {
    @Override
    public void balance(List<ContainerInfo> workingContainers, ArrayList<ComponentModel.Representation> deployedComponents) {
        LoggerFactory.getLogger().warn("Beginning");

        LoggerFactory.getLogger().info("Containers count: " + workingContainers.size());
        LoggerFactory.getLogger().info("Components count: " + deployedComponents.size());

        if (workingContainers.size() == 0) {
            LoggerFactory.getLogger().warn("No containers online");

            return;
        }

        for (ContainerInfo c : workingContainers) {
            for (String component : c.getLoadedComponents()) {
                c.unloadComponent(component);
            }
        }

        if ((workingContainers.size() == 0) || (deployedComponents.size() == 0)) {
            return;
        }

        for (ContainerInfo container : workingContainers) {
            for (ComponentModel.Representation component : deployedComponents) {
                container.loadComponent(component.getComponentName());
            }
        }

        try {
            for (ContainerInfo c : workingContainers) {
                c.commit();
            }
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Failed to commit balancing", e);

            for (ContainerInfo c : workingContainers) {
                c.rollback();
            }
        }
    }
}

package com.mjolnirr.proxy;

import com.google.inject.Injector;
import com.mjolnirr.lib.ApplicationInstanceManager;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.TypeHelper;
import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.lib.security.CertificatesGenerator;
import com.mjolnirr.proxy.lib.ComponentManager;
import com.mjolnirr.proxy.lib.UsersManager;
import com.mjolnirr.proxy.models.UserModel;
import com.mjolnirr.proxy.msg.ProxyMsgReceiver;
import com.mjolnirr.proxy.msg.ResponseCallback;
import com.mjolnirr.proxy.msg.UtilCallback;
import com.mjolnirr.proxy.rest.ProxyWeb;
import org.bouncycastle.crypto.CryptoException;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.restlet.ext.servlet.ServerServlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

public class Main {
    public static final int PROXY_PORT = 21690;
    public static final int UTIL_THREADS_COUNT = 10;

    public static final String CERT_DIRECTORY_KEY = "certificates_path";
    public static final String CONFIG_HOST_KEY = "web_host";
    public static final String CONFIG_PORT_KEY = "web_port";
    public static final String HORNET_CONFIG_KEY = "hornet_config";
    public static final String KEYSTORE_KEY = "keystore";
    public static final String KEYSTORE_PASSWORD_KEY = "keystore_password";
    public static final String TRUSTSTORE_KEY = "truststore";
    public static final String TRUSTSTORE_PASSWORD_KEY = "truststore_password";
    public static final String ALLOW_ANONIMOUS_KEY = "allow_anon";

    public static void main(String[] args) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        String Name = "BC";
        if (Security.getProvider(Name) == null) {
            System.out.println("not installed");
        } else {
            System.out.println("installed");
        }

        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);

        PropertiesReader properties = injector.getInstance(PropertiesReader.class);
        ComponentManager componentManager = injector.getInstance(ComponentManager.class);

        if (!ApplicationInstanceManager.registerInstance(args, PROXY_PORT)) {
            // instance already running.
            System.out.println("Another instance of this application is already running.  Exiting.");
            System.exit(0);
        }

        ApplicationInstanceManager.setApplicationInstanceListener(new ApplicationInstanceManager.ApplicationInstanceListener() {
            public void newInstanceCreated(String[] argsPassed) {
                LoggerFactory.getLogger().info("Received shutdown signal. Stopping.");

                System.exit(0);
            }
        });

        TypeHelper.PROXY_HOST = "localhost";
        TypeHelper.PROXY_PORT = properties.getConfig().getInt(CONFIG_PORT_KEY);

        try {
            MessagesQueueServer.getInstance().setHornetConfigPath(getHornetConfigPath(properties));
            MessagesQueueServer.getInstance().setJmsConfigPath(null);
            MessagesQueueServer.getInstance().start();
            LoggerFactory.getLogger().info("JMS server started!");
        } catch (Exception e) {
            LoggerFactory.getLogger().fatal("Something went wrong!", e);
            System.exit(1);
        }

        String keystoreLocation = properties.getConfig().getString(KEYSTORE_KEY);
        String keystorePassword = properties.getConfig().getString(KEYSTORE_PASSWORD_KEY);
        String truststoreLocation = properties.getConfig().getString(TRUSTSTORE_KEY);
        String truststorePassword = properties.getConfig().getString(TRUSTSTORE_PASSWORD_KEY);
        boolean allowAnonimous = properties.getConfig().getBoolean(ALLOW_ANONIMOUS_KEY);

        try {
            checkKeystore(keystoreLocation, keystorePassword, truststoreLocation, truststorePassword);
            checkAdminPresence(properties);
        } catch (Exception e) {
            LoggerFactory.getLogger().fatal("Failed to generate admin certificate", e);
        }

        try {
            publishEndPoint(properties, keystoreLocation, keystorePassword, truststoreLocation, truststorePassword, allowAnonimous);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            componentManager.checkDeployment();
        } catch (FileNotFoundException e) {
            LoggerFactory.getLogger().error("Failed to deploy missing statics", e);
        }
    }

    private static void checkKeystore(String keystoreLocation, String keystorePassword, String truststoreLocation, String truststorePassword) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, InvalidKeyException, SignatureException, NoSuchProviderException, InvalidKeySpecException, CryptoException, UnrecoverableKeyException {
        File keystore = new File(keystoreLocation);

        if (!keystore.exists()) {
            LoggerFactory.getLogger().info("Server keystore not found. Creating new...");

            CertificatesGenerator generator = new CertificatesGenerator(keystoreLocation, keystorePassword, truststoreLocation, truststorePassword);
            generator.generateServerKeystores("CN=Mjolnirr Proxy", keystorePassword, truststorePassword);
        }
    }

    private static void checkAdminPresence(PropertiesReader properties) throws NoSuchAlgorithmException, CertificateException, SignatureException, NoSuchProviderException, InvalidKeyException, IOException, KeyStoreException, UnrecoverableKeyException, InvalidKeySpecException, CryptoException {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        UsersManager manager = injector.getInstance(UsersManager.class);

        List<UserRepresentation> admins = manager.getAdmins();

        if (admins.size() == 0) {
            LoggerFactory.getLogger().warn("No admins found, creating new");

            UserModel admin = new UserModel("admin");
            admin.setRole("admin");
            admin.generateCertificate();

            manager.addUser(admin);

            File certDirectory = new File(properties.getConfig().getString(CERT_DIRECTORY_KEY));
            if (!certDirectory.exists()) {
                certDirectory.mkdirs();
            }

            admin.writeCertificateToFile(new File(certDirectory, "admin.p12"));
        }
    }

    public static String getHornetConfigPath(PropertiesReader properties) {
        String hornetConfigPath = properties.getConfig().getString(HORNET_CONFIG_KEY);

        if ((hornetConfigPath == null) || (hornetConfigPath.equals(""))) {
            return "hornetq-conf.xml";
        } else {
            return new File(hornetConfigPath).toURI().toString();
        }
    }

    private static void publishEndPoint(
            PropertiesReader properties,
            String keystoreLocation,
            String keystorePassword,
            String truststoreLocation,
            String truststorePassword,
            boolean allowAnonimous) throws
            NoSuchAlgorithmException, KeyManagementException, IOException, KeyStoreException, CertificateException, UnrecoverableKeyException, SignatureException, InvalidKeyException {
        int portNumber = properties.getConfig().getInt(CONFIG_PORT_KEY);

        QueuedThreadPool threadPool = new QueuedThreadPool(1000, 10);
        Server server = new Server(threadPool);

        /*******************/

        ServletContextHandler restletContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
        restletContext.setContextPath("/");

        ServerServlet serverServlet = new ServerServlet();
        ServletHolder servletHolder = new ServletHolder(serverServlet);
        servletHolder.setInitParameter("org.restlet.application", ProxyWeb.class.getCanonicalName());
        restletContext.addServlet(servletHolder, "/*");

        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{restletContext});

        HttpConfiguration https = new HttpConfiguration();
        https.addCustomizer(new SecureRequestCustomizer());

        SslContextFactory sslContextFactory = new SslContextFactory(keystoreLocation);
        sslContextFactory.setKeyStorePassword(keystorePassword);
        sslContextFactory.setTrustStorePath(truststoreLocation);
        sslContextFactory.setTrustStorePassword(truststorePassword);
        sslContextFactory.setNeedClientAuth(!allowAnonimous);

        ServerConnector sslConnector = new ServerConnector(server,
                new SslConnectionFactory(sslContextFactory, "http/1.1"),
                new HttpConnectionFactory(https));
        sslConnector.setPort(portNumber);

        server.setConnectors(new Connector[]{sslConnector});

        server.setHandler(handlers);

        privateChannelSubscribe();
        utilChannelSubscribe();

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void privateChannelSubscribe() {
        ProxyMsgReceiver privateChannelReceiver = new ProxyMsgReceiver(ProxyWeb.PROXY_RESPONSE_ADDRESS, ResponseCallback.class);
        privateChannelReceiver.start();
    }

    private static void utilChannelSubscribe() {
        for (int i = 0; i < UTIL_THREADS_COUNT; i++) {
            ProxyMsgReceiver privateChannelReceiver = new ProxyMsgReceiver(ProxyWeb.UTIL_RESPONSE_ADDRESS, UtilCallback.class);
            privateChannelReceiver.start();
        }
    }
}

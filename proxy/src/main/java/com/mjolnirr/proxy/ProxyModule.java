package com.mjolnirr.proxy;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.mjolnirr.lib.msg.HornetSessionsRegister;
import com.mjolnirr.lib.msg.HornetSessionsRegisterImpl;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.lib.properties.ProxyPropertiesReaderImpl;
import com.mjolnirr.proxy.balancer.AllToAllBalancerImpl;
import com.mjolnirr.proxy.balancer.Balancer;
import com.mjolnirr.proxy.balancer.ScriptBalancerImpl;
import com.mjolnirr.proxy.lib.*;

public class ProxyModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(PropertiesReader.class).to(ProxyPropertiesReaderImpl.class);
//        bind(Balancer.class).to(RoundRobinBalancerImpl.class);
//        bind(Balancer.class).to(AllToAllBalancerImpl.class);
        bind(Balancer.class).to(ScriptBalancerImpl.class);

        HornetSessionsRegister sessionsRegister = new HornetSessionsRegisterImpl();

        bind(HornetSessionsRegister.class).toInstance(sessionsRegister);

        bind(BalancerScriptManager.class).to(BalancerScriptManagerImpl.class).in(Singleton.class);
        bind(ContainerRegister.class).to(ContainerRegisterImpl.class).in(Singleton.class);
        bind(ComponentManager.class).to(ComponentManagerImpl.class).in(Singleton.class);
        bind(UsersManager.class).to(UsersManagerImpl.class).in(Singleton.class);
        bind(CertificateManager.class).to(CertificateManagerImpl.class).in(Singleton.class);
        bind(PostFilesManager.class).to(PostFilesManagerImpl.class).in(Singleton.class);
    }
}

package com.mjolnirr.proxy.rest;

import com.google.inject.Injector;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.proxy.ProxyModule;
import com.mjolnirr.proxy.lib.ComponentManagerImpl;
import com.mjolnirr.proxy.lib.FixedJacksonConverter;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.data.MediaType;
import org.restlet.engine.Engine;
import org.restlet.engine.converter.ConverterHelper;
import org.restlet.ext.jackson.JacksonConverter;
import org.restlet.resource.Directory;
import org.restlet.routing.Router;
import org.restlet.routing.Template;
import org.restlet.routing.TemplateRoute;
import org.restlet.routing.Variable;
import org.restlet.util.RouteList;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by sk_ on 1/25/14.
 */
public class ProxyWeb extends Application {
    public static final String ADMIN_APPLICATION_NAME = "admin";
    private static final String CONFIG_DEFAULT_PAGE = "default_page";
    public static final String PROXY_RESPONSE_ADDRESS = "proxy_response_address";
    public static final String UTIL_RESPONSE_ADDRESS = "util_response_address";

    @Override
    public Restlet createInboundRoot() {
        Router router = new Router(getContext());

        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        PropertiesReader properties = injector.getInstance(PropertiesReader.class);

//        File assetsDir = new File(properties.getConfig().getString(ComponentManagerImpl.CONFIG_ASSETS_PATH));

        getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);
        replaceConverter(JacksonConverter.class, new FixedJacksonConverter());

        TemplateRoute route = router.attach("/static/{application}/{resource}", StaticResource.class);
        router.attach("/request", RequestResource.class);
        router.attach("/tmp", TempFilesResource.class);
        router.attach("/tmp/{filename}", TempFilesResource.class);
        router.attach("/{application}", InterfaceResource.class);
        router.attach("/{application}/{page}", InterfaceResource.class);

        route.getTemplate().setMatchingMode(Template.MODE_STARTS_WITH);
        Map<String, Variable> routeVariables = route.getTemplate().getVariables();
        routeVariables.put("resource", new Variable(Variable.TYPE_URI_PATH));

        return router;
    }

    static void replaceConverter(Class<? extends ConverterHelper> converterClass, ConverterHelper newConverter) {

        ConverterHelper oldConverter = null;
        List<ConverterHelper> converters = Engine.getInstance().getRegisteredConverters();
        for (ConverterHelper converter : converters) {
            if (converter.getClass().equals(converterClass)) {
                converters.remove(converter);
                oldConverter = converter;
                break;
            }
        }

        converters.add(newConverter);
        if (oldConverter == null) {
            System.err.println("Added Converter to Restlet Engine: " + newConverter.getClass().getName());
        } else {
            System.err.println("Replaced Converter " + oldConverter.getClass().getName() + " with " +
                    newConverter.getClass().getName() + " in Restlet Engine");
        }
    }
}

package com.mjolnirr.proxy.rest;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.Request;
import com.mjolnirr.lib.Response;
import com.mjolnirr.lib.msg.CallbackRegister;
import com.mjolnirr.lib.msg.ProxyCommunicator;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.proxy.Main;
import com.mjolnirr.proxy.admin.AdminApplication;
import com.mjolnirr.proxy.lib.PostFilesManager;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by sk_ on 1/25/14.
 */
public class RequestResource extends InjectServerResource {
    @Inject
    private PostFilesManager postFilesManager;

    @Inject
    private PropertiesReader propertiesReader;

    @Post
    public Object getResource(Representation entity) throws Exception {
        final Request request;

        if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true)) {
            List<FileItem> items = new RestletFileUpload(new DiskFileItemFactory()).parseRepresentation(entity);

            String applicationName = null;
            String methodName = null;
            List<Object> args = new ArrayList<Object>();

            for (final Iterator<FileItem> it = items.iterator(); it.hasNext(); ) {
                FileItem fi = it.next();
                String fieldName = fi.getFieldName();

                if (fieldName.equals("applicationName")) {
                    applicationName = fi.getString();
                } else if (fieldName.equals("methodName")) {
                    methodName = fi.getString();
                } else {
                    File file = File.createTempFile("hive", "");
                    fi.write(file);

                    args.add(postFilesManager.createTempFile(file));
                }
            }

            request = new Request(applicationName, methodName, args);
        } else {
            Form form = new Form(entity);
            String applicationName = form.getFirstValue("applicationName");
            String methodName = form.getFirstValue("methodName");
            List args = new Gson().fromJson(form.getFirstValue("args"), List.class);

            request = new Request(applicationName, methodName, args);
        }

        boolean allowAnonimous = properties.getConfig().getBoolean(Main.ALLOW_ANONIMOUS_KEY);
        request.setCurrentUser(getCurrentUser(allowAnonimous));

        request.setToken(getRequest().getClientInfo().getAddress());
        LoggerFactory.getLogger().info("Incoming request to app " + request.getApplicationName() + " method " + request.getMethodName());

        Response response;
        if ((!allowAnonimous) && (request.getApplicationName().equalsIgnoreCase(ADMIN_APPLICATION_NAME))) {
//        if ((request.getApplicationName().equalsIgnoreCase(ADMIN_APPLICATION_NAME))) {
            //  This is admin request
            if (request.getCurrentUser().isAdmin()) {
                try {
                    response = AdminApplication.invoke(request);
                } catch (Exception e) {
                    response = new Response(e);
                }
            } else {
                response = new Response();
            }
        } else {
            final ProxyCommunicator communicator = new ProxyCommunicator();

            String requestUID = Request.generateUniqueKey();
            final Request internalRequest = new Request(request, PROXY_RESPONSE_ADDRESS, requestUID);
            internalRequest.setCurrentUser(request.getCurrentUser());

            response = CallbackRegister.addSyncCallback(requestUID, new Runnable() {
                @Override
                public void run() {
                    // Send request
                    try {
                        communicator.send(request.getApplicationName().toLowerCase(), internalRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        if (!response.isOkay()) {
            setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        } else if (response.getResponseClass().equals("java.io.File")) {
            //  Place filename instead of content
//            try {
            Gson serializer = new Gson();

//                response.setResponseObject(serializer.toJson(postFilesManager.createTempFile(serializer.fromJson(response.getResponseObject(), String.class))));
            response.setResponseObject(response.getResponseObject());
//            } catch (IOException e) {
//                LoggerFactory.getLogger().fatal("Failed to save the content to the temp file", e);
//            }
        }

        return response;
    }
}

package com.mjolnirr.proxy.rest;

import com.google.inject.Inject;
import com.mjolnirr.lib.DynamicByteRepresentation;
import com.mjolnirr.lib.DynamicFileRepresentation;
import com.mjolnirr.lib.exceptions.StaticFileNotFoundException;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.proxy.Main;
import org.apache.commons.io.IOUtils;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Date;

/**
 * Created by sk_ on 1/25/14.
 */
public class StaticResource extends InjectServerResource {
    @Inject
    private PropertiesReader propertiesReader;

    @Get
    public Representation getStatic() throws StaticFileNotFoundException {
        String applicationName = (String) getRequest().getAttributes().get("application");
        String resourceName = (String) getRequest().getAttributes().get("resource");

        try {
            byte[] info;

            boolean allowAnonimous = properties.getConfig().getBoolean(Main.ALLOW_ANONIMOUS_KEY);
            long modificationDate = 0;
            if ((!allowAnonimous) && (applicationName.equalsIgnoreCase(ADMIN_APPLICATION_NAME))) {
                InputStream fileStream = RequestResource.class.getClassLoader().getResource("admin/" + resourceName).openStream();
                info = IOUtils.toByteArray(fileStream);
            } else if (applicationName.equalsIgnoreCase(UTIL_NAME)) {
                InputStream fileStream = RequestResource.class.getClassLoader().getResource("util/" + resourceName).openStream();
                info = IOUtils.toByteArray(fileStream);
            } else {
                info = IOUtils.toByteArray(readStaticFile(applicationName + "/" + resourceName).getInputStream());
                modificationDate = getStaticFileModificationDate(applicationName + "/" + resourceName);
            }

            MediaType type = MediaType.ALL;

            if (resourceName.endsWith("css")) {
                type = MediaType.TEXT_CSS;
            } else if (resourceName.endsWith("js")) {
                type = MediaType.TEXT_JAVASCRIPT;
            }

            DynamicByteRepresentation result = new DynamicByteRepresentation(type, info.length, info);
            result.setModificationDate(new Date(modificationDate));

            return result;
        } catch (MalformedURLException e) {
            e.printStackTrace();

            throw new StaticFileNotFoundException("", null);
        } catch (java.io.IOException e) {
            e.printStackTrace();

            throw new StaticFileNotFoundException("", null);
        }
    }
}

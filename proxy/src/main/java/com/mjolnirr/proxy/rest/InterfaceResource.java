package com.mjolnirr.proxy.rest;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.mjolnirr.lib.DynamicByteRepresentation;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.Request;
import com.mjolnirr.lib.assets.AssetsPreprocessors;
import com.mjolnirr.lib.assets.Preprocessor;
import com.mjolnirr.lib.exceptions.StaticFileNotFoundException;
import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.lib.msg.CallbackRegister;
import com.mjolnirr.lib.msg.ProxyCommunicator;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.proxy.Main;
import com.mjolnirr.proxy.admin.AdminApplication;
import com.mjolnirr.proxy.helpers.FormatHelper;
import com.mjolnirr.proxy.lib.ComponentManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jetty.http.HttpStatus;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.w3c.dom.Document;

import javax.activation.DataHandler;
import javax.ws.rs.core.Response;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sk_ on 1/25/14.
 */
public class InterfaceResource extends InjectServerResource {
    @Inject
    private ComponentManager componentManager;

    @Inject
    private PropertiesReader propertiesReader;

    @Get
    public Representation getInterface() throws StaticFileNotFoundException {
        final String applicationName = (String) getRequest().getAttributes().get("application");
        String page = (String) getRequest().getAttributes().get("page");

        String requestedPage = page;

        if ((requestedPage == null) || (requestedPage.isEmpty())) {
            requestedPage = getDefaultPage();
        }

        LoggerFactory.getLogger().info("Getting interface file " + page + " from application " + applicationName);

        boolean allowAnonimous = properties.getConfig().getBoolean(Main.ALLOW_ANONIMOUS_KEY);
        try {
            InputStream response;

            if ((!allowAnonimous) && (applicationName.equalsIgnoreCase(ADMIN_APPLICATION_NAME))) {
                if (getCurrentUser(allowAnonimous).isAdmin()) {
                    Map<String, Object> model = AdminApplication.invokeInterface(page, getQuery().getValuesMap());

                    if (model == null) {
                        model = new HashMap<String, Object>();
                    }

                    Preprocessor preprocessor = AssetsPreprocessors.findPreprocessorByOwnExtension("jade");

                    model.put("currentUser", getCurrentUser(allowAnonimous));

                    response = preprocessor.processContent(
                            IOUtils.toByteArray(InterfaceResource.class.getClassLoader().getResource("admin/" + page.replace(".", "") + ".jade").openStream()),
                            model
                    ).getInputStream();
                } else {
                    //  TODO: render error pages
                    response = null;
                }
            } else {
                Pair<Integer, String> fileType = getStaticFileType(applicationName + "/interface/" + requestedPage);

                UserRepresentation currentUser = getCurrentUser(allowAnonimous);
                ArrayList<String> allowedRoles = componentManager.getComponentInterface(applicationName).getAllowedRoles(requestedPage);

                boolean rolesFound = currentUser.getRole().equalsIgnoreCase("admin");

                for (String user : allowedRoles) {
                    if (currentUser.getRole().equalsIgnoreCase(user)) {
                        rolesFound = true;
                    }
                }

                if (rolesFound) {
                    if (fileType.getLeft() != TEMPLATE_PAGE) {
                        LoggerFactory.getLogger().info("Found static page");
                        response = readStaticFile(applicationName + "/interface/" + requestedPage + "." + fileType.getRight()).getInputStream();
                    } else {
                        LoggerFactory.getLogger().info("Found template page");
                        Preprocessor preprocessor = AssetsPreprocessors.findPreprocessorByOwnExtension(fileType.getRight());

                        if (componentManager.getComponentInterface(applicationName).hasMethod(page)) {
                            //  Invoke method
                            final ProxyCommunicator communicator = new ProxyCommunicator();
                            final Map<String, String> values = getQuery().getValuesMap();

                            String requestUID = Request.generateUniqueKey();
                            final Request internalRequest = new Request(applicationName, page, new ArrayList<Object>() {{
                                add(values);
                            }}, PROXY_RESPONSE_ADDRESS, requestUID);
                            internalRequest.setCurrentUser(currentUser);

                            Map<String, Object> model = new Gson().fromJson(CallbackRegister.addSyncCallback(requestUID, new Runnable() {
                                @Override
                                public void run() {
                                    // Send request
                                    try {
                                        communicator.send(applicationName.toLowerCase(), internalRequest);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).getResponseObject(), Map.class);

                            model.put("currentUser", currentUser);
                            model.put("formatHelper", new FormatHelper());
                            model.putAll(values);

                            for (String key : model.keySet()) {
                                System.out.println(key + " -> " + model.get(key));
                            }

                            response = preprocessor.processContent(
                                    IOUtils.toByteArray(readStaticFile(applicationName + "/interface/" + requestedPage + "." + fileType.getRight()).getInputStream()),
                                    model
                            ).getInputStream();
                        } else {
                            response = preprocessor.processContent(
                                    IOUtils.toByteArray(readStaticFile(applicationName + "/interface/" + requestedPage + "." + fileType.getRight()).getInputStream())
                            ).getInputStream();
                        }
                    }
                } else {
                    setStatus(Status.CLIENT_ERROR_FORBIDDEN);

                    Preprocessor preprocessor = AssetsPreprocessors.findPreprocessorByOwnExtension("jade");

                    response = preprocessor.processContent(
                            IOUtils.toByteArray(InterfaceResource.class.getClassLoader().getResource("util/pages/access_denied.jade").openStream())
                    ).getInputStream();
                }
            }

            Preprocessor jadeProcessor = AssetsPreprocessors.findPreprocessorByOwnExtension("jade");
            Map<String, Object> model = new HashMap<String, Object>();
            model.put("currentUser", getCurrentUser(allowAnonimous));
            model.put("appName", applicationName);
            model.put("components", componentManager.getApplications());
            model.put("body", new String(IOUtils.toByteArray(response)));
            model.put("formatHelper", new FormatHelper());

            String layoutPage;

            if ((getCurrentUser(allowAnonimous) != null) && (getCurrentUser(allowAnonimous).isAdmin())) {
                layoutPage = "admin-layout.jade";
            } else {
                layoutPage = "layout.jade";
            }

            DataHandler processedPage = jadeProcessor.processContent(IOUtils.toByteArray(InterfaceResource.class.getClassLoader().getResource(layoutPage).openStream()),
                    model);

            byte[] result = IOUtils.toByteArray(processedPage.getInputStream());
            return new DynamicByteRepresentation(MediaType.TEXT_HTML, result.length, result);
        } catch (MalformedURLException e) {
            LoggerFactory.getLogger().fatal(e);

            throw new StaticFileNotFoundException("", null);
        } catch (java.io.IOException e) {
            LoggerFactory.getLogger().fatal(e);

            throw new StaticFileNotFoundException("", null);
        } catch (UnrecoverableKeyException e) {
            LoggerFactory.getLogger().fatal(e);

            throw new StaticFileNotFoundException("", null);
        } catch (NoSuchAlgorithmException e) {
            LoggerFactory.getLogger().fatal(e);

            throw new StaticFileNotFoundException("", null);
        } catch (KeyStoreException e) {
            LoggerFactory.getLogger().fatal(e);

            throw new StaticFileNotFoundException("", null);
        } catch (KeyManagementException e) {
            LoggerFactory.getLogger().fatal(e);

            throw new StaticFileNotFoundException("", null);
        }
    }

    private byte[] documentToBytes(Document document) throws TransformerException {
        StringWriter sw = new StringWriter();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "html");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

        transformer.transform(new DOMSource(document), new StreamResult(sw));
        return sw.toString().getBytes();
    }
}

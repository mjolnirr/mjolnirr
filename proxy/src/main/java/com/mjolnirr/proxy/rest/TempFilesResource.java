package com.mjolnirr.proxy.rest;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.mjolnirr.lib.*;
import com.mjolnirr.lib.exceptions.StaticFileNotFoundException;
import com.mjolnirr.lib.msg.CallbackRegister;
import com.mjolnirr.lib.msg.ProxyCommunicator;
import com.mjolnirr.proxy.Main;
import com.mjolnirr.proxy.admin.AdminApplication;
import com.mjolnirr.proxy.lib.PostFilesManagerImpl;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.restlet.data.*;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by sk_ on 1/25/14.
 */
public class TempFilesResource extends InjectServerResource {
    @Inject
    private PostFilesManagerImpl tmpFilesManager;

    @Post
    public String uploadFile(Representation entity) throws Exception {
        if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true)) {
            List<FileItem> items = new RestletFileUpload(new DiskFileItemFactory()).parseRepresentation(entity);

            for (final Iterator<FileItem> it = items.iterator(); it.hasNext(); ) {
                FileItem fi = it.next();

                File file = File.createTempFile("hive", "");
                fi.write(file);

                return tmpFilesManager.createTempFile(file);
            }
        }

        return null;
    }

    @Get
    public Representation getStatic() throws StaticFileNotFoundException {
        String filename = (String) getRequest().getAttributes().get("filename");

        try {
            File info = tmpFilesManager.getContent(filename);

            if (!info.exists()) {
                setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                return null;
            }

            DynamicFileRepresentation result = new DynamicFileRepresentation(MediaType.MULTIPART_ALL, info, !getRequest().getMethod().equals(Method.HEAD));
            result.setDisposition(new Disposition("attachment; filename=" + filename));
            return result;
        } catch (java.io.IOException e) {
            e.printStackTrace();

            throw new StaticFileNotFoundException("", null);
        }
    }
}

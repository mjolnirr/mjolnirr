package com.mjolnirr.proxy.rest;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.assets.AssetsPreprocessors;
import com.mjolnirr.lib.assets.Preprocessor;
import com.mjolnirr.lib.exceptions.StaticFileNotFoundException;
import com.mjolnirr.lib.models.UserRepresentation;
import com.mjolnirr.lib.properties.PropertiesReader;
import com.mjolnirr.lib.util.StringExtensions;
import com.mjolnirr.proxy.ProxyModule;
import com.mjolnirr.proxy.lib.ComponentManagerImpl;
import com.mjolnirr.proxy.lib.UsersManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.restlet.resource.ServerResource;

import javax.activation.DataHandler;
import java.io.File;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;

/**
 * Created by sk_ on 1/25/14.
 */
public abstract class InjectServerResource extends ServerResource {
    public static final String PROXY_RESPONSE_ADDRESS = "proxy_response_address";
    public static final String ADMIN_APPLICATION_NAME = "admin";
    public static final String UTIL_NAME = "mjolnirr-util";
    private static final String CONFIG_DEFAULT_PAGE = "default_page";

    public static int STATIC_PAGE = 0;
    public static int TEMPLATE_PAGE = 1;
    public static int UNKNOWN_FILE = 2;

    @Inject
    PropertiesReader properties;

    File assetsDir;
    private final String defaultPage;

    public InjectServerResource() {
        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        injector.injectMembers(this);

        assetsDir = new File(properties.getConfig().getString(ComponentManagerImpl.CONFIG_ASSETS_PATH));
        defaultPage = properties.getConfig().getString(CONFIG_DEFAULT_PAGE);
    }

    public String getDefaultPage() {
        return defaultPage;
    }

    protected Pair<Integer, String> getStaticFileType(final String filename) {
        Collection<File> files = FileUtils.listFiles(assetsDir, new IOFileFilter() {
            @Override
            public boolean accept(File file) {
                return file.getAbsolutePath().contains(filename);
            }

            @Override
            public boolean accept(File file, String s) {
                return true;
            }
        }, TrueFileFilter.INSTANCE);

        boolean hasStatic = false;
        boolean hasTemplate = false;
        String foundExtension = "";

        for (File file : files) {
            String extension = FilenameUtils.getExtension(file.getAbsolutePath());

            if (AssetsPreprocessors.staticExtensions.contains(extension)) {
                hasStatic = true;
                foundExtension = extension;

                break;
            }
            if (AssetsPreprocessors.templateExtensions.contains(extension)) {
                foundExtension = extension;
                hasTemplate = true;
            }
        }

        if (hasStatic) {
            return new ImmutablePair<Integer, String>(STATIC_PAGE, foundExtension);
        } else if (hasTemplate) {
            return new ImmutablePair<Integer, String>(TEMPLATE_PAGE, foundExtension);
        } else {
            return new ImmutablePair<Integer, String>(UNKNOWN_FILE, foundExtension);
        }
    }

    protected long getStaticFileModificationDate(String filename) throws
            MalformedURLException,
            StaticFileNotFoundException {
        File fileLocation = new File(assetsDir, filename);

        try {
            if (isSubdirectory(assetsDir, new File(assetsDir.getAbsolutePath(), filename))) {
                if (!fileLocation.exists()) {
                    return 0;
                }

                return fileLocation.lastModified();
            } else {
                throw new StaticFileNotFoundException("", null);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();

            throw new StaticFileNotFoundException("", null);
        } catch (StaticFileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (java.io.IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return 0;
    }

    protected DataHandler readStaticFile(String filename) throws
            MalformedURLException,
            StaticFileNotFoundException {
        String extension = StringExtensions.getExtension(filename);
        Preprocessor preprocessor = AssetsPreprocessors.findPreprocessorByExtension(extension);

        LoggerFactory.getLogger().info("Reading " + filename + " with extension " + extension);

        File fileLocation = new File(assetsDir, filename);

        File templateLocation = null;
        if (preprocessor != null) {
            templateLocation = new File(assetsDir, StringExtensions.replaceLast(filename, extension, preprocessor.getTemplateExtension()));
        }

        try {
            if (isSubdirectory(assetsDir, new File(assetsDir.getAbsolutePath(), filename))) {
                if ((!fileLocation.exists()) && (preprocessor != null)) {
                    LoggerFactory.getLogger().info("Static file not found. Generating from template...");

                    preprocessor.processContent(templateLocation.getAbsolutePath(), fileLocation);
                }

                return new DataHandler(fileLocation.toURI().toURL());
            } else {
                throw new StaticFileNotFoundException("", null);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();

            throw new StaticFileNotFoundException("", null);
        } catch (StaticFileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (java.io.IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return null;
    }

    public static boolean isSubdirectory(File staticFilesPath, File requestedPath) throws URISyntaxException, java.io.IOException {
        File staticFilesDir = new File(staticFilesPath.toURI().normalize().getPath());
        File requestedFile = new File(requestedPath.toURI().normalize().getPath());
        File currentFile = requestedFile;

        while (currentFile != null) {
            if (staticFilesDir.getCanonicalPath().equals(currentFile.getCanonicalPath())) {
                return true;
            }

            currentFile = currentFile.getParentFile();
        }

        return false;
    }

    UserRepresentation getCurrentUser(boolean allowAnon) {
        if (allowAnon) {
            return null;
        }

        Injector injector = InjectorSingleton.getInjector(ProxyModule.class);
        UsersManager manager = injector.getInstance(UsersManager.class);

        List<X509Certificate> certs = (List<X509Certificate>) getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        BigInteger userSerial = certs.get(0).getSerialNumber();

        return manager.findUser(userSerial).toRepresentation();
    }
}

if (workingContainers.size() == 0) {
    println("No containers online");

    return;
}

for (c in workingContainers) {
    for (component in c.getLoadedComponents()) {
        c.unloadComponent(component);
    }
}

if ((workingContainers.size() == 0) || (deployedComponents.size() == 0)) {
    return;
}

for (container in workingContainers) {
    for (component in deployedComponents) {
        container.loadComponent(component);
    }
}
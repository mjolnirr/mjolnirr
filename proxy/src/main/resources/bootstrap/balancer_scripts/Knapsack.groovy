println "Knapsack balancer"

def balanceForNode(components, node) {
    N = 0    //  number of items

    workingComponents = []
    for (component in components) {
        if (component["node"] == null) {
            workingComponents.add(component)
        }
    }

    workingComponents.unique{ it["name"] }
    N = workingComponents.size

    W = node.properties.memoryQuota                //  node capacity

    profit = [];
    weight = [];

    for (i = 0; i < N; i++) {
        profit.add(N - i);
        weight.add(workingComponents.get(i).memoryVolume);
    }

    opt = [];
    sol = [];

    for (n = 0 ; n <= (N + 1) ; n++) {
        optRow = []
        solRow = []

        for (w = 0 ; w <= (W + 1) ; w++) {
            optRow.add(0)
            solRow.add(0)
        }

        opt.add(optRow)
        sol.add(solRow)
    }

    for (n = 0; n < N; n++) {
        for (w = 0; w <= W; w++) {
            // don't take item n
            option1 = opt[n-1][w];

            // take item n
            option2 = Integer.MIN_VALUE;
            if (weight[n] <= w) {
                option2 = profit[n] + opt[n-1][w - (int) weight[n]];
            }

            // select better of two options
            opt[n][w] = Math.max(option1, option2);
            sol[n][w] = (option2 >= option1);
        }
    }

    // determine which items to take
    w = W;

    for (n = (N - 1); n >= 0; n--) {
        if (sol[n][(int) w]) {
            workingComponents[n]["node"] = node.containerID
            w = w - weight[n];
        }
    }

    return components
}

//  Main
if (workingContainers.size() == 0) {
    println("No containers online");

    return;
}

nodesMap = [:]
for (c in workingContainers) {
    nodesMap[c.containerID] = [ node: c, components: [] ]
    for (component in c.getLoadedComponents()) {
        c.unloadComponent(component);
    }
}

if ((workingContainers.size() == 0) || (deployedComponents.size() == 0)) {
    return;
}

components = []
for (component in deployedComponents) {
    for (i = 0 ; i < component.minInstancesCount ; i++) {
        components.add([ "name": component.componentName, "memoryVolume": component.memoryVolume, "node": null, "entriesCount": component.minInstancesCount ])
    }
}
components.sort{-1 * it["entriesCount"]}

//  Initial distribution
capacityUsed = 0
for (container in workingContainers) {
    components = balanceForNode(components, container)
}

for (component in components) {
    if (component["node"] == null) {
        println "ERROR! Not enough memory for components deployment!"
        break
    }

    nodesMap[component["node"]]["components"].add(component["name"])
    capacityUsed += component["memoryVolume"]
}

oldCapacity = 0
additionalInstances = 1
isGood = true
while (oldCapacity != capacityUsed) {
    oldCapacity = capacityUsed
    capacityUsed = 0

    componentCandidates = []
    for (component in deployedComponents) {
        entriesCount = Math.min(component.maxInstancesCount, component.minInstancesCount + additionalInstances)

        for (i = 0 ; i < entriesCount ; i++) {
            componentCandidates.add([ "name": component.componentName, "memoryVolume": component.memoryVolume, "node": null, "entriesCount": entriesCount ])
        }
    }

    componentCandidates.sort{-1 * it["entriesCount"]}

    for (container in workingContainers) {
        componentCandidates = balanceForNode(componentCandidates, container)
    }

    for (com in componentCandidates) {
        println(com)
    }

    for (component in componentCandidates) {
        if (component["node"] == null) {
            isGood = false
            oldCapacity = capacityUsed
            break
        }
    }

    if (isGood) {
        components = componentCandidates
    } else {
        break
    }

    nodesMap.each() {
        key, value -> value["components"] = []
    }

    for (component in componentCandidates) {
        nodesMap[component["node"]]["components"].add(component["name"])
        capacityUsed += component["memoryVolume"]
    }

    additionalInstances++
}

for (container in workingContainers) {
    nodesMap[container.containerID]["components"].each {
        container.loadComponent(it);
    }
}
function callRemoteMethod(params) {
    var callback = params["onSuccess"];
    var errorCallback = typeof params["onError"] !== "undefined" ? params["onError"] : function(err) {
        console.log(err);
    };

    params = formatParams(params);

    params["success"] = function(data){
        var responseObject = JSON.parse(data.responseObject);

        if (data.responseClass == "java.io.File") {
            //  Download the file
            $("body").append("<iframe src='/tmp/" + responseObject + "' style='display: none;' ></iframe>")

            return;
        }

        callback(responseObject);
    };
    params["error"] = errorCallback;

    $.ajax(params);
}

function callRemoteMethodSync(params) {
    params = formatParams(params);
    params["async"] = false;

    var response = $.ajax(params);

    if (response.responseJSON.failureMessage !== null) {
        throw response.responseJSON.failureMessage;
    }

    var responseObject = JSON.parse(response.responseJSON.responseObject);

    if (response.responseJSON.responseClass == "java.io.File") {
        //  Download the file
        $("body").append("<iframe src='/tmp/" + responseObject + "' style='display: none;' ></iframe>")

        return;
    }

    return responseObject;
}

function formatParams(params) {
    var moduleName = params["moduleName"];
    var applicationName = typeof params["appName"] !== "undefined" ? params["appName"] : (typeof moduleName !== "undefined" ? moduleName : globalApplicationName);
    var methodName = params["method"];
    var arguments = params["args"];

    var formatted = {
        type: 'POST'
        , url: '/request'
        , data: {
            applicationName: applicationName
            , methodName: methodName
            , args: JSON.stringify(arguments)
        }
    };

    return formatted;
}
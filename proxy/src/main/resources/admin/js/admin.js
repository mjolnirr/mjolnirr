function unloadComponent(containerID, componentName) {
    callRemoteMethod({
        method: "unloadComponent"
        , args: [ containerID, componentName ]
        , onSuccess: function (result) {
            location.reload();
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function downloadCertificate(serial) {
    callRemoteMethod({
        method: "downloadCertificate"
        , args: [ serial ]
        , onSuccess: function (result) {
            console.log(result);
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function downloadContainerCertificate(serial) {
    callRemoteMethod({
        method: "downloadContainerCertificate"
        , args: [ serial ]
        , onSuccess: function (result) {
            console.log(result);
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function deleteUser(serial) {
    callRemoteMethod({
        method: "removeUser"
        , args: [ serial ]
        , onSuccess: function (result) {
            location.reload();
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function removeComponent(componentName) {
    callRemoteMethod({
        method: "removeComponent"
        , args: [ componentName ]
        , onSuccess: function (result) {
            location.reload();
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function deleteContainerCertificate(serial) {
    callRemoteMethod({
        method: "deleteContainerCertificate"
        , args: [ serial ]
        , onSuccess: function (result) {
            location.reload();
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function createUser() {
    callRemoteMethod({
        method: "createUser"
        , args: [ prompt("please specify username"), prompt("please specify role") ]
        , onSuccess: function (result) {
            location.reload();
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function generateCertificate() {
    callRemoteMethod({
        method: "generateCertificate"
        , args: [ ]
        , onSuccess: function (result) {
            location.reload();
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function createBalancerScript() {
    var editor = ace.edit("editor");

    var scriptID = $("#script-id").val("");
    var scriptName = $("#script-name").val("");
    editor.getSession().setValue("");
}

function saveBalancerScript() {
    var editor = ace.edit("editor");

    var scriptID = $("#script-id").val();
    var scriptName = $("#script-name").val();
    var code = editor.getSession().getValue();

    if (scriptID === "") {
        createNewBalancerScript(scriptName, code);
    } else {
        updateBalancerScript(scriptID, scriptName, code);
    }
}

function createNewBalancerScript(scriptName, scriptContent, callback) {
    if(typeof(callback)==='undefined') callback = function(result) {location.reload();};

    callRemoteMethod({
        method: "createBalancerScript"
        , args: [ scriptName, scriptContent ]
        , onSuccess: callback
        , onError: function (error) {
//            Do nothing
        }
    });
}

function updateBalancerScript(scriptID, scriptName, scriptContent, callback) {
    if(typeof(callback)==='undefined') callback = function(result) {location.reload();};

    callRemoteMethod({
        method: "updateBalancerScript"
        , args: [ scriptID, scriptName, scriptContent ]
        , onSuccess: callback
        , onError: function (error) {
//            Do nothing
        }
    });
}

function showScript(scriptID, scriptName, scriptContent, isModifiable) {
    var editor = ace.edit("editor");

    if (isModifiable) {
        $("#delete-script-button").removeAttr("disabled");
        $("#save-script-button").removeAttr("disabled");
    } else {
        $("#delete-script-button").attr("disabled", "disabled");
        $("#save-script-button").attr("disabled", "disabled");
    }

    $("li.script").removeClass("selected");
    $("li.script#" + scriptID).addClass("selected");

    var scriptID = $("#script-id").val(scriptID);
    var scriptName = $("#script-name").val(scriptName);
    editor.getSession().setValue(scriptContent);
}

function setActive() {
    console.log($("#script-id").val());

    callRemoteMethod({
        method: "setActiveBalancerScript"
        , args: [ $("#script-id").val() ]
        , onSuccess: function (result) {
            location.reload();
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function fork() {
    console.log($("#script-id").val());

    callRemoteMethod({
        method: "forkBalancerScript"
        , args: [ $("#script-id").val() ]
        , onSuccess: function (result) {
            location.reload();
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function deleteBalancerScript() {
    callRemoteMethod({
        method: "deleteBalancerScript"
        , args: [ $("#script-id").val() ]
        , onSuccess: function (result) {
            location.reload();
        }
        , onError: function (error) {
//            Do nothing
        }
    });
}

function testBalancerScript() {
    var editor = ace.edit("editor");

    var scriptID = $("#script-id").val();
    var scriptName = $("#script-name").val();
    var code = editor.getSession().getValue();

    nextStepFunction = function(result) {
        callRemoteMethod({
                method: "testBalancerScript"
                , args: [ $("#script-id").val() ]
                , onSuccess: function (result) {
                    bootbox.alert(result.replace(/\n/g, '<br />'));
                }
                , onError: function (error) {
                    //  TODO: print whole stacktrace
                    bootbox.alert(error.responseJSON.failureMessage.replace(/\n/g, '<br />'));
                }
            });
    }

    if (scriptID === "") {
        createNewBalancerScript(scriptName, code, nextStepFunction);
    } else {
        updateBalancerScript(scriptID, scriptName, code, nextStepFunction);
    }
}
package com.mjolnirr.container.lib;

import com.google.inject.Inject;
import com.mjolnirr.lib.Properties;
import com.mjolnirr.lib.properties.PropertiesReader;
import org.apache.commons.io.FileUtils;
import org.hsqldb.lib.FileUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.12.13
 * Time: 13:38
 * To change this template use File | Settings | File Templates.
 */
public class JarCacheManagerImpl implements JarCacheManager {
    private File cacheDir;

    @Inject
    private PropertiesReader propertiesReader;

    @Inject
    JarCacheManagerImpl(PropertiesReader propertiesReader) {
        cacheDir = new File(Properties.JAR_DIR);
    }

    @Override
    public boolean hasInCache(String componentName) {
        return new File(cacheDir, componentName + ".jar").exists();
    }

    @Override
    public long lastModified(String componentName) {
        return new File(cacheDir, componentName + ".jar").lastModified();
    }

    @Override
    public void saveToCache(String componentName, File jar) throws IOException {
        File destination = new File(cacheDir, componentName + ".jar");

        if (destination.exists()) {
            destination.delete();
        }

        FileUtils.copyFile(jar, new File(cacheDir, componentName + ".jar"));
    }

    @Override
    public String getComponentJAR(String componentName) {
        return new File(cacheDir, componentName + ".jar").getAbsolutePath();
    }
}

package com.mjolnirr.container.lib;

import com.google.inject.Inject;
import com.mjolnirr.lib.*;
import com.mjolnirr.lib.msg.MessagesController;
import com.mjolnirr.lib.exceptions.ApplicationNotFoundException;
import com.mjolnirr.lib.exceptions.ModuleAccessException;
import com.mjolnirr.lib.exceptions.StaticFileCannotBeReadException;
import com.mjolnirr.lib.exceptions.StaticFileNotFoundException;
import org.hornetq.api.core.HornetQException;
import org.xml.sax.SAXException;

import javax.activation.DataHandler;
import javax.xml.parsers.ParserConfigurationException;
import java.util.*;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.repeatSecondlyForever;

/**
 * Implementation of the ComponentRegister
 * */
public class ComponentRegisterImpl implements ComponentRegister {
    /**
     * Component name-to-instance map
     * */
    private Map<String, Component> register;

    /**
     * Guice-compatible constructor
     * */
    @Inject
    public ComponentRegisterImpl() {
        register = new HashMap<String, Component>();
    }

    /**
     * Add component to register
     *
     * @param jarLocation Location of the component JAR
     * @param componentName*/
    @Override
    public void addComponent(String jarLocation, String componentName) {
        ComponentController.getInstance().addComponentToLoadQueue(jarLocation, componentName);
    }

    /**
     * Add component to register
     *
     * @param component Component wrapper
     * */
    @Override
    public void addComponent(Component component) {
        register.put(component.getComponentName().toLowerCase(), component);

        try {
            MessagesController.getInstance().startReceiver(component.getComponentName());
        } catch (Exception e) {
            LoggerFactory.getLogger().fatal("Failed to lod component!", e);
        }
    }

    /**
     * Returns XML (JavaFX-compatible) layout of application
     *
     * @param name Component name (not class name!)
     * @param page Name of the page
     *
     * @return XML layout as a DataHandler
     *
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws StaticFileNotFoundException
     * @throws StaticFileCannotBeReadException
     * @throws ApplicationNotFoundException
     * @throws ModuleAccessException Thrown when you try to get interface from Module
     * */
    @Override
    public DataHandler getApplicationInterface(String name, String page)
            throws
            SAXException,
            ParserConfigurationException,
            StaticFileNotFoundException,
            StaticFileCannotBeReadException,
            ApplicationNotFoundException,
            ModuleAccessException {
        Component app = register.get(name.toLowerCase());

        if (app == null) {
            String message = String.format("Application \"%s\" is not found.", name);
            LoggerFactory.getLogger().error(message);

            throw new ApplicationNotFoundException();
        }

        return app.getInterface(page, Component.APPLICATION_INTERFACE_XML);
    }

    /**
     * Get an instance of the AbstractComponent object from registry
     * @param name Name of the application
     * @return An instance of the application
     */
    @Override
    public Component getInstance(String name) {
        return register.get(name.toLowerCase());
    }

    /**
     * Get static file content
     *
     * @param componentName Name of the component
     * @param resourceName Name of the static file to read
     *
     * @return Static file content
     *
     * @throws ApplicationNotFoundException
     * @throws StaticFileNotFoundException
     * @throws StaticFileCannotBeReadException
     * @throws ModuleAccessException
     * */
    @Override
    public DataHandler getApplicationResource(String componentName, String resourceName)
            throws
            ApplicationNotFoundException,
            StaticFileNotFoundException,
            StaticFileCannotBeReadException,
            ModuleAccessException {
        Component app = register.get(componentName.toLowerCase());

        if (app == null) {
            String message = String.format("Application \"%s\" is not found.", componentName);
            LoggerFactory.getLogger().error(message);

            throw new ApplicationNotFoundException();
        }

        return app.getResource(resourceName);
    }

    /**
     * Reload the component in container
     * @param componentName Name of the component
     */
    @Override
    public void reloadComponent(String jarLocation, String componentName) {
        ComponentController.getInstance().addComponentToReloadQueue(jarLocation, componentName.toLowerCase());
    }

    /**
     * Unload the application from container
     * @param appName Name of the application
     */
    @Override
    public void unloadComponent(String appName) {
        ComponentController.getInstance().addComponentToUnloadQueue(appName.toLowerCase());
    }

    /**
     * Remove application from registry without unload
     * @param appName Name of the application
     */
    @Override
    public void removeComponent(String appName) throws HornetQException, InterruptedException {
        register.remove(appName.toLowerCase());

        MessagesController messagesController = MessagesController.getInstance();
        messagesController.stopReceiver(appName);
    }

    /**
     * Returns list of applications loaded into container
     * @return List of applications names
     */
    @Override
    public List<String> getComponentsNames() {
        return new ArrayList<String>(register.keySet());
    }

    @Override
    public boolean hasComponent(String componentName) {
        return register.containsKey(componentName);
    }
}

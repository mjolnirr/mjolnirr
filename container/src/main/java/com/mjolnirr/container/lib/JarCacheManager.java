package com.mjolnirr.container.lib;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.12.13
 * Time: 13:35
 * To change this template use File | Settings | File Templates.
 */
public interface JarCacheManager {
    boolean hasInCache(String componentName);
    long lastModified(String componentName);
    void saveToCache(String componentName, File jar) throws IOException;

    String getComponentJAR(String componentName);
}

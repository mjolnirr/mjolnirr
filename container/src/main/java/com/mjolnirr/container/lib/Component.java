package com.mjolnirr.container.lib;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;
import com.google.common.util.concurrent.UncheckedTimeoutException;
import com.google.inject.Injector;
import com.mjolnirr.container.ContainerModule;
import com.mjolnirr.container.ProxyClient;
import com.mjolnirr.lib.*;
import com.mjolnirr.lib.component.ComponentContext;
import com.mjolnirr.lib.component.ComponentContextImpl;
import com.mjolnirr.lib.component.ComponentInterface;
import com.mjolnirr.lib.exceptions.*;
import com.mjolnirr.lib.manifest.MjolnirrMethod;
import com.mjolnirr.lib.util.JarReader;
import org.xeustechnologies.jcl.JarClassLoader;
import org.xeustechnologies.jcl.JclObjectFactory;

import javax.activation.DataHandler;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * Mjolnirr component wrapper.
 * Holds component instance and some other util things
 */
public class Component {
    /**
     * Interface type constants
     */
    public static final int APPLICATION_INTERFACE_XML = 0;
    public static final int APPLICATION_INTERFACE_HTML = 1;
    private ComponentInterface componentInterface;
    private File appConfigs;
    private File workspace;

    /**
     * JarClassLoader context with target JAR loaded
     */
    private JarClassLoader jcl;

    /**
     * Instance of the main class (specified as a `classname` in manifest)
     */
    private Object instance;
    /**
     * JAR location in the filesystem
     */
    private String jarLocation;

    /**
     * Is it Mjolnirr module?
     *
     * @return true if it is module
     */
    public boolean isModule() {
        return componentInterface.isModule();
    }

    /**
     * Constructs new Component instance
     *
     * @param jarLocation Location of the JAR file to load,
     * @throws ManifestParsingException Thrown if manifest couldn't be parsed
     * @throws WrongSuperclassException Thrown if Component not subclass of AbstractComponent
     */
    public Component(String jarLocation, String componentName) throws ManifestParsingException, WrongSuperclassException, MalformedURLException {
        this.jarLocation = jarLocation;

        //  JCL init
        jcl = new JarClassLoader();
        jcl.add(this.jarLocation);

        jcl.getLocalLoader().setOrder(1);

        jcl.getThreadLoader().setOrder(2);
        jcl.getSystemLoader().setOrder(3);
        jcl.getCurrentLoader().setOrder(4);
        jcl.getParentLoader().setOrder(5);

        this.componentInterface = null;
        List<ComponentInterface> components = ComponentInterface.parsePackage(jarLocation);

        for (ComponentInterface component : components) {
            if (component.getComponentName().equalsIgnoreCase(componentName)) {
                this.componentInterface = component;
                break;
            }
        }

        //  Extracting class from the JAR
        JclObjectFactory factory = JclObjectFactory.getInstance();

        //  This is application object
        instance = factory.create(jcl, componentInterface.getClassName());

        //  Extract configs of the applications
        appConfigs = extractConfigs();
        //  Create workspace for the application
        workspace = createWorkspace();

        initializeInstance(appConfigs, workspace, null);

        if ((!componentInterface.isModule()) && (!Properties.isDevelopmentMode())) {
            try {
                //  Send JAR to the proxy to deploy static files (FXML, CSS etc)
                deployStatic();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sends JAR to the Mjolnirr Proxy to deploy statis files like FXML, CSS and other
     *
     * @throws IOException
     * @throws FileNotFoundException
     */
    private void deployStatic() throws IOException, FileNotFoundException {
        Injector injector = InjectorSingleton.getInjector(ContainerModule.class);
        ContainerTransport proxy = injector.getInstance(ProxyClient.class).getProxy();

//        proxy.deployStatic(componentName, IOUtils.toByteArray(new FileInputStream(jarLocation)));
    }

    /**
     * Passes ComponentContext to the instance
     *
     * @param configDir Configs dir location
     * @param workspace Workspace dir location
     * @return True if component was successfully initialized
     */
    private ComponentContext initializeInstance(File configDir, File workspace, Request request) throws WrongSuperclassException {
        try {
            ComponentContext context;

            if (request != null) {
                context = new ComponentContextImpl(request.getToken());
                context.initialize(componentInterface.getComponentName(), configDir, workspace, request.getCurrentUser(), jcl);
            } else {
                context = new ComponentContextImpl();
                context.initialize(componentInterface.getComponentName(), configDir, workspace, null, jcl);
            }

            Method method = instance.getClass().getMethod("initialize", ComponentContext.class);
            method.invoke(instance, context);

            return context;
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Failed to initialize component instance", e);

            throw new WrongSuperclassException();
        }
    }

    /**
     * Extracts all the configs from JAR
     * Configs are located in `resource/config` directory of JAR
     *
     * @return Location of the config directory
     */
    private File extractConfigs() {
        File configDir = new File(Properties.CONFIG_DIR);
        File appConfigs = new File(configDir, componentInterface.getComponentName());

        if (!configDir.exists()) {
            configDir.mkdirs();
        }

        if (!appConfigs.exists()) {
            appConfigs.mkdirs();
        }

        JarReader.deployConfigs(appConfigs, jarLocation);

        return appConfigs;
    }

    /**
     * Creates workspace directory for the component
     *
     * @return Location of the workspace directory
     */
    private File createWorkspace() {
        File workspaceDir = new File(Properties.WORKSPACE_DIR);
        File appWorkspace = new File(workspaceDir, componentInterface.getComponentName());

        if (!workspaceDir.exists()) {
            workspaceDir.mkdirs();
        }

        if (!appWorkspace.exists()) {
            appWorkspace.mkdirs();
        }

        return appWorkspace;
    }

    /**
     * Get the name of the component
     *
     * @return Component name
     */
    public String getComponentName() {
        return componentInterface.getComponentName();
    }

    /**
     * Get static interface file from JAR
     *
     * @param page Page name
     * @param type Type of the page (FXML or HTML)
     * @throws StaticFileNotFoundException
     * @throws StaticFileCannotBeReadException
     * @throws ModuleAccessException           Thrown when you try to get the interface from the module
     */
    public DataHandler getInterface(String page, int type) throws StaticFileNotFoundException, StaticFileCannotBeReadException, ModuleAccessException {
        if (isModule()) {
            //  Go fuck yourself
            throw new ModuleAccessException();
        }

        String suffix = null;

        switch (type) {
            case APPLICATION_INTERFACE_XML:
                suffix = ".fxml";
                break;
            case APPLICATION_INTERFACE_HTML:
                suffix = ".html";
                break;
        }

        return JarReader.readFile(jarLocation, "interface/" + page + suffix);
    }

    /**
     * Get specific method
     *
     * @param name Name of the method to get
     * @return Desired method
     */
    public MjolnirrMethod getComponentMethod(String name) throws NoSuchMethodException {
        for (MjolnirrMethod applicationMethod : componentInterface.getApplicationMethods()) {
            if (applicationMethod.getName().equals(name)) {
                return applicationMethod;
            }
        }

        String message = String.format("Method \"%s\" is not found!", name);
        LoggerFactory.getLogger().error(message);

        throw new NoSuchMethodException(message);
    }

    /**
     * Invocation of the method from the application
     *
     * @param name The componentName of the method
     * @param args An arguments of the method
     * @return Response of the invoked method
     */
    public Object invokeMethod(String name, boolean isDebugMode, Request request, final Object... args) throws NoSuchMethodException, ClassNotFoundException, ApplicationErrorException {
        //  Looking for the method
        MjolnirrMethod mjolnirrMethod = getComponentMethod(name);

        //  Extraction of the method
        final Method method;
        try {
            method = mjolnirrMethod.getReflectionMethod(jcl, instance);
        } catch (NoSuchMethodException e) {
            LoggerFactory.getLogger().error("Can't reflect method!", e);
            throw e;
        } catch (ClassNotFoundException e) {
            LoggerFactory.getLogger().error("Can't reflect method!", e);
            throw e;
        }

        //  Invocation of the method
        try {
            initializeInstance(appConfigs, workspace, request);

            TimeLimiter limiter = new SimpleTimeLimiter();
            return limiter.callWithTimeout(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    Thread.currentThread().setContextClassLoader(jcl);

                    return method.invoke(instance, args);
                }
            }, mjolnirrMethod.getMaxExecutionTime(), TimeUnit.SECONDS, true);
        } catch (UncheckedTimeoutException e) {
            LoggerFactory.getLogger().error("Time is up!", e);

            throw new ApplicationErrorException("Too long execution. Time is up.");
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Can't invoke method!", e);

            throw new ApplicationErrorException(e.getCause().getMessage(), (Exception) e.getCause());
        }
    }

    /**
     * Get the instance of the component
     *
     * @return Component instance
     */
    public Object getInstance() {
        return instance;
    }

    /**
     * Get static resource file from the JAR
     *
     * @param resourceName Name of the resource file to look for
     */
    public DataHandler getResource(String resourceName) throws StaticFileNotFoundException, StaticFileCannotBeReadException, ModuleAccessException {
        if (isModule()) {
            throw new ModuleAccessException();
        }

        return JarReader.readFile(jarLocation, resourceName);
    }

    /**
     * Get the return type classname
     *
     * @param methodName Name of the method to analyse
     * @return Method return classname
     */
    public String getReturnClassName(String methodName) throws NoSuchMethodException {
        return getComponentMethod(methodName).getReturnClassName();
    }

    /**
     * Get JCL classloader of the current component
     *
     * @return Class loader of the current component
     */
    public JarClassLoader getClassLoader() {
        return jcl;
    }

    /**
     * Unload all classes of the application
     */
    public synchronized void unloadClasses() {
        for (Class clazz : jcl.getLoadedClasses().values()) {
            jcl.unloadClass(clazz.getName());
        }
    }

    /**
     * Get class from component JAR by name
     *
     * @param className Name of the class to load
     * @return Class from component JAR
     * @throws ClassNotFoundException
     */
    public Class findClass(String className) throws ClassNotFoundException {
        return jcl.loadClass(className);
    }

    /**
     * Get object from component by name
     *
     * @param className
     * @return
     * @throws ClassNotFoundException
     */
    public Object findObject(String className) throws ClassNotFoundException {
        JclObjectFactory factory = JclObjectFactory.getInstance();

        return factory.create(jcl, className);
    }
}
package com.mjolnirr.container.lib;

import com.google.inject.Injector;
import com.mjolnirr.container.ContainerModule;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.SchedulerProxy;
import com.mjolnirr.lib.exceptions.ManifestParsingException;
import com.mjolnirr.lib.exceptions.WrongSuperclassException;
import org.hornetq.api.core.HornetQException;
import org.quartz.*;

import javax.inject.Inject;
import java.net.MalformedURLException;
import java.util.ConcurrentModificationException;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Component controller to use with Quartz
 * */
public class ComponentController {
    /**
     * Job running timeout
     * */
    private static final int SCHEDULER_TIMER = 5;

    /**
     * Queue of components for load to container
     */
    private static ConcurrentLinkedQueue<ComponentLoadPair> loadQueue;

    /**
     * Queue of components for unload from container
     */
    private static ConcurrentLinkedQueue<String> unloadQueue;

    /**
     * Queue of the component to reload
     * */
    private static ConcurrentLinkedQueue<ComponentLoadPair> reloadQueue;

    /**
     * Component controller singleton
     * */
    private static ComponentController instance;


    static {
        loadQueue = new ConcurrentLinkedQueue<ComponentLoadPair>();
        unloadQueue = new ConcurrentLinkedQueue<String>();
        reloadQueue = new ConcurrentLinkedQueue<ComponentLoadPair>();

        try {
            instance = new ComponentController();
        } catch (SchedulerException e) {
            LoggerFactory.getLogger().fatal("Can't start applications controller!", e);
            System.exit(1);
        }
    }

    /**
     * Private constructor for singleton usage
     * */
    private ComponentController() throws SchedulerException {
        SchedulerProxy.scheduleJob(AppReloader.class, this.getClass().getName(), SCHEDULER_TIMER);
        SchedulerProxy.scheduleJob(AppUnloader.class, this.getClass().getName(), SCHEDULER_TIMER);
        SchedulerProxy.scheduleJob(AppLoader.class, this.getClass().getName(), SCHEDULER_TIMER);
    }

    /**
     * Get the controller instance (standard singleton method)
     *
     * @return Controller instance
     * */
    public static ComponentController getInstance() {
        return instance;
    }

    /**
     * Load component and add to load queue
     *
     * @param jarLocation JAR file location in filesystem
     * @param componentName*/
    public void addComponentToLoadQueue(String jarLocation, String componentName) {
        loadQueue.add(new ComponentLoadPair(jarLocation, componentName));
    }

    /**
     * Add component with specified name to reload queue
     *
     * @param componentName Name of the component to reload
     * */
    public void addComponentToReloadQueue(String jarLocation, String componentName) {
        reloadQueue.add(new ComponentLoadPair(jarLocation, componentName));
    }

    /**
     * Add component with specified name to unload queue
     *
     * @param componentName Name of the component to unload
     * */
    public void addComponentToUnloadQueue(String componentName) {
        unloadQueue.add(componentName);
    }

    public static class ComponentLoadPair {
        private String jarLocation;
        private String componentName;

        public ComponentLoadPair(String jarLocation, String componentName) {
            this.jarLocation = jarLocation;
            this.componentName = componentName;
        }

        public String getJarLocation() {
            return jarLocation;
        }

        public void setJarLocation(String jarLocation) {
            this.jarLocation = jarLocation;
        }

        public String getComponentName() {
            return componentName;
        }

        public void setComponentName(String componentName) {
            this.componentName = componentName;
        }
    }

    /**
     * Implementation of Quartz job for applications loading
     */
    public static class AppLoader implements Job {
        /**
         * Instance of the application register
         */
        @Inject
        private ComponentRegister register;

        @Inject
        public AppLoader() {
            Injector injector = InjectorSingleton.getInjector(ContainerModule.class);
            injector.injectMembers(this);
        }

        @Override
        public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
            while (!loadQueue.isEmpty()) {
                ComponentLoadPair componentLoadInfo = loadQueue.poll();

                LoggerFactory.getLogger().info("Loading component " + componentLoadInfo.getComponentName() + " from JAR " + componentLoadInfo.getJarLocation());

                Component newComponent;
                try {
                    newComponent = new Component(componentLoadInfo.getJarLocation(), componentLoadInfo.getComponentName());
                } catch (ManifestParsingException e) {
                    String message = String.format("Can't load component! Can't parse manifest of the \"%s\"", componentLoadInfo);
                    LoggerFactory.getLogger().error(message);

                    continue;
                } catch (WrongSuperclassException e) {
                    String message = String.format("Wrong superclass! Looks like the main class of app `%s` doesn't extend `AbstractComponent`", componentLoadInfo);
                    LoggerFactory.getLogger().error(message);

                    continue;
                } catch (MalformedURLException e) {
                    String message = String.format("Wrong superclass! Looks like the main class of app `%s` doesn't extend `AbstractComponent`", componentLoadInfo);
                    LoggerFactory.getLogger().error(message);

                    continue;
                }

                register.addComponent(newComponent);

                String message = String.format("Component \"%s\" loaded successfully.", newComponent.getComponentName());
                LoggerFactory.getLogger().info(message);
            }
        }

    }

    /**
     * Implementation of Quartz job for applications loading
     */
    public static class AppReloader implements Job {
        /**
         * Instance of the application register
         */
        @Inject
        private ComponentRegister register;

        @Inject
        public AppReloader() {
            Injector injector = InjectorSingleton.getInjector(ContainerModule.class);
            injector.injectMembers(this);
        }

        @Override
        public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
            while (!reloadQueue.isEmpty()) {
                ComponentLoadPair componentLoadInfo = reloadQueue.poll();

                LoggerFactory.getLogger().info("Reloading component " + componentLoadInfo.getComponentName() + " from JAR " + componentLoadInfo.getJarLocation());

                Component app = register.getInstance(componentLoadInfo.getComponentName());

                if (app == null) {
                    String message = String.format("Can't unload application! Component \"%s\" is not found.", componentLoadInfo.getComponentName());
                    LoggerFactory.getLogger().error(message);
                    continue;
                }

                try {
                    app.unloadClasses();
                } catch (ConcurrentModificationException ex) {
                    String message = String.format("Can't unload application! Classes of the application \"%s\" are busy.", componentLoadInfo.getComponentName());
                    LoggerFactory.getLogger().warn(message);
                    reloadQueue.add(componentLoadInfo);
                    continue;
                }

                try {
                    register.removeComponent(componentLoadInfo.getComponentName());
                } catch (Exception e) {
                    LoggerFactory.getLogger().fatal("Failed to stop receiver for channel " + componentLoadInfo.getComponentName(), e);
                }

                LoggerFactory.getLogger().info(String.format("Component \"%s\" unloaded successfully.", componentLoadInfo.getComponentName()));

                Component newComponent;
                try {
                    newComponent = new Component(componentLoadInfo.getJarLocation(), componentLoadInfo.getComponentName());
                } catch (ManifestParsingException e) {
                    LoggerFactory.getLogger().error(String.format("Can't load component! Can't parse manifest of the \"%s\"", componentLoadInfo));

                    continue;
                } catch (WrongSuperclassException e) {
                    LoggerFactory.getLogger().error(String.format("Wrong superclass! Looks like the main class of app `%s` doesn't extend `AbstractComponent`", componentLoadInfo));

                    continue;
                } catch (MalformedURLException e) {
                    LoggerFactory.getLogger().error(String.format("Wrong superclass! Looks like the main class of app `%s` doesn't extend `AbstractComponent`", componentLoadInfo));

                    continue;
                }

                register.addComponent(newComponent);

                String message = String.format("Component \"%s\" loaded successfully.", newComponent.getComponentName());
                LoggerFactory.getLogger().info(message);
            }
        }

    }

    /**
     * Implementation of Quartz job for applications unloading
     */
    public static class AppUnloader implements Job {
        /**
         * Instance of the application register
         */
        @Inject
        private ComponentRegister register;

        @Inject
        public AppUnloader() {
            Injector injector = InjectorSingleton.getInjector(ContainerModule.class);
            injector.injectMembers(this);
        }

        @Override
        public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
            while (!unloadQueue.isEmpty()) {
                String appName = unloadQueue.poll().toLowerCase();

                Component app = register.getInstance(appName);

                if (app == null) {
                    String message = String.format("Can't unload application! Component \"%s\" is not found.", appName);
                    LoggerFactory.getLogger().error(message);
                    continue;
                }

                try {
                    app.unloadClasses();
                } catch (ConcurrentModificationException ex) {
                    String message = String.format("Can't unload application! Classes of the application \"%s\" are busy.", appName);
                    LoggerFactory.getLogger().warn(message);
                    unloadQueue.add(appName);
                    continue;
                }

                try {
                    register.removeComponent(appName);
                } catch (Exception e) {
                    LoggerFactory.getLogger().fatal("Failed to stop receiver for channel " + appName, e);
                }

                String message = String.format("Component \"%s\" unloaded successfully.", appName);
                LoggerFactory.getLogger().info(message);
            }
        }

    }
}
package com.mjolnirr.container.lib;

import com.google.inject.Singleton;
import com.mjolnirr.lib.exceptions.ApplicationNotFoundException;
import com.mjolnirr.lib.exceptions.ModuleAccessException;
import com.mjolnirr.lib.exceptions.StaticFileCannotBeReadException;
import com.mjolnirr.lib.exceptions.StaticFileNotFoundException;
import org.hornetq.api.core.HornetQException;
import org.xml.sax.SAXException;

import javax.activation.DataHandler;
import javax.xml.parsers.ParserConfigurationException;
import java.util.List;

/**
 * Register which contains all the information about loaded components
 * */

public interface ComponentRegister {
    /**
     * Add component to register
     *
     * @param jarLocation Location of the component JAR
     * @param componentName*/
    public void addComponent(String jarLocation, String componentName);
    /**
     * Add component to register
     *
     * @param app Component wrapper
     * */
    public void addComponent(Component app);

    /**
     * Returns XML (JavaFX-compatible) layout of application
     *
     * @param name Component name (not class name!)
     * @param page Name of the page
     *
     * @return XML layout as a DataHandler
     *
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws StaticFileNotFoundException
     * @throws StaticFileCannotBeReadException
     * @throws ApplicationNotFoundException
     * @throws ModuleAccessException Thrown when you try to get interface from Module
     * */
    DataHandler getApplicationInterface(String name, String page)
        throws
            SAXException,
            ParserConfigurationException,
            StaticFileNotFoundException,
            StaticFileCannotBeReadException,
            ApplicationNotFoundException,
            ModuleAccessException;

    /**
     * Get an instance of the AbstactComponent object from registry
     * @param name Name of the application
     * @return An instance of the application
     */
    Component getInstance(String name);

    /**
     * Reload the component
     * @param jarLocation JAR location
     * @param componentName Name of the component to reload
     * */
    void reloadComponent(String jarLocation, String componentName);

    /**
     * Unload the application from container
     * @param appName Name of the application
     */
    void unloadComponent(String appName);

    /**
     * Remove application from registry without unload
     * @param appName Name of the application
     */
    void removeComponent(String appName) throws HornetQException, InterruptedException;

    /**
     * Get static file content
     *
     * @param componentName Name of the component
     * @param resourceName Name of the static file to read
     *
     * @return Static file content
     *
     * @throws ApplicationNotFoundException
     * @throws StaticFileNotFoundException
     * @throws StaticFileCannotBeReadException
     * @throws ModuleAccessException
     * */
    DataHandler getApplicationResource(String componentName, String resourceName)
        throws
            ApplicationNotFoundException,
            StaticFileNotFoundException,
            StaticFileCannotBeReadException,
            ModuleAccessException;

    /**
     * Returns list of applications loaded into container
     * @return List of applications names
     */
    List<String> getComponentsNames();

    boolean hasComponent(String componentName);
}
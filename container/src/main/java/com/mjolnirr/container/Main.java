package com.mjolnirr.container;

import com.beust.jcommander.JCommander;
import com.google.inject.Injector;
import com.mjolnirr.lib.msg.HornetSessionsRegister;
import com.mjolnirr.container.msg.ContainerUtilCallback;
import com.mjolnirr.lib.*;
import com.mjolnirr.lib.msg.ContainerInfo;
import com.mjolnirr.lib.msg.HornetCommunicator;
import com.mjolnirr.lib.msg.MessagesController;
import com.mjolnirr.lib.util.OsHelper;
import com.mjolnirr.proxy.MessagesQueueServer;
import com.mjolnirr.proxy.msg.ProxyMsgReceiver;

public class Main {
    public static final int CONTAINER_PORT = 21891;
    private static ProxyMsgReceiver containerChannelReceiver;

    static {
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier(){

                    public boolean verify(String hostname,
                                          javax.net.ssl.SSLSession sslSession) {
                        if (hostname.equals("localhost")) {
                            return true;
                        }
                        return false;
                    }
                });
    }

    public static void main(String[] args) throws Exception {
        //  Don't touch this. It's a magic
        System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.internal.bind.v2.ContextFactory");

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                shutdownHook();
            }
        });

        //  Overriding config from the command line
        Properties a = new Properties();
        JCommander jc = new JCommander(a);
        jc.parse(args);

        //  Hack for windows, halts application on remote request
        if (OsHelper.isWindows()) {
            if (!ApplicationInstanceManager.registerInstance(args, CONTAINER_PORT)) {
                // instance already running.
                System.out.println("Another instance of this application is already running.  Exiting.");
                System.exit(0);
            }

            //  Same shit
            ApplicationInstanceManager.setApplicationInstanceListener(new ApplicationInstanceManager.ApplicationInstanceListener() {
                public void newInstanceCreated(String[] argsPassed) {
                    shutdownHook();
                    System.exit(0);
                }
            });
        }

        //  Hornet server location
        HornetCommunicator.setHornetHost(Properties.PROXY_WEB_HOST);
        HornetCommunicator.setHornetPort(Properties.PROXY_QUEUE_PORT);
        TypeHelper.PROXY_HOST = Properties.PROXY_WEB_HOST;
        TypeHelper.PROXY_PORT = Properties.PROXY_WEB_PORT;

        //  It is not error - it's a dummy call for static initialisation
        MessagesController.getInstance();

        //  If container is running in development mode
        //  it's stand-alone. Setting up own Hornet server
        if (Properties.isDevelopmentMode()) {
            LoggerFactory.getLogger().info("Development mode detected. Starting HornetQ...");
            try {
                MessagesQueueServer.getInstance().setHornetConfigPath(Properties.getHornetConfigPath());
                MessagesQueueServer.getInstance().setJmsConfigPath(null);
                MessagesQueueServer.getInstance().start();
                LoggerFactory.getLogger().info("JMS server started!");
            } catch (Exception e) {
                LoggerFactory.getLogger().fatal("Something went wrong!", e);
                System.exit(1);
            }
        } else {
            registerContainer();
        }

//        Main.publishEndPoint();
    }

    private static void shutdownHook() {
        LoggerFactory.getLogger().info("Received shutdown signal. Stopping.");
        Injector injector = InjectorSingleton.getInjector(ContainerModule.class);

        unregisterContainer();
        MessagesController messagesController = MessagesController.getInstance();
        HornetSessionsRegister register = injector.getInstance(HornetSessionsRegister.class);
        try {
            messagesController.stopReceivers();
            containerChannelReceiver.kill();
            register.killAll();
        } catch (Exception e) {
            LoggerFactory.getLogger().fatal("Failed to stop receivers", e);
        }
    }

    private static void registerContainer() {
        LoggerFactory.getLogger().info("Registering container on proxy...");

        final ContainerInfo info = new ContainerInfo(Properties.CONTAINER_UUID, Properties.CONTAINER_NAME, true, Properties.getModel());

        final HornetCommunicator communicator = new HornetCommunicator();

        containerChannelReceiver = new ProxyMsgReceiver(Properties.CONTAINER_UUID, ContainerUtilCallback.class);
        containerChannelReceiver.start();

        new Thread() {
            public void run() {
                try {
                    communicator.registerContainer(info);
                } catch (Exception e) {
                    LoggerFactory.getLogger().error("Couldn't update container state!", e);
                }

                try {
                    Thread.sleep(ContainerInfo.REFRESH_TIMEOUT * 1000);
                } catch (InterruptedException e) {
                    LoggerFactory.getLogger().error("Error when sleep", e);
                }

                while (true) {
                    try {
                        info.setStateUpdate();
                        communicator.registerContainer(info);
                    } catch (Exception e) {
                        LoggerFactory.getLogger().error("Couldn't update container state!", e);
                    }

                    try {
                        Thread.sleep(ContainerInfo.REFRESH_TIMEOUT * 1000);
                    } catch (InterruptedException e) {
                        LoggerFactory.getLogger().error("Error when sleep", e);
                    }
                }
            }
        }.start();
    }

    private static void unregisterContainer() {
        LoggerFactory.getLogger().info("Unregistering container on proxy...");

        ContainerInfo info = new ContainerInfo(Properties.CONTAINER_UUID, Properties.CONTAINER_NAME, false, Properties.getModel());

        HornetCommunicator communicator = new HornetCommunicator();
        try {
            communicator.registerContainer(info);
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Couldn't unregister container!", e);
        }
    }
}

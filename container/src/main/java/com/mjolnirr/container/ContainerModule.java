package com.mjolnirr.container;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.mjolnirr.container.lib.*;
import com.mjolnirr.container.proxy.ContainerProxy;
import com.mjolnirr.container.proxy.ContainerProxyImpl;
import com.mjolnirr.lib.msg.HornetSessionsRegister;
import com.mjolnirr.lib.msg.HornetSessionsRegisterImpl;
import com.mjolnirr.lib.properties.ContainerPropertiesReaderImpl;
import com.mjolnirr.lib.properties.PropertiesReader;

/**
 *  Container Guice module. It's for dependency injection
 * */
public class ContainerModule extends AbstractModule {
    @Override
    protected void configure() {
        ComponentRegister register = new ComponentRegisterImpl();
        HornetSessionsRegister sessionsRegister = new HornetSessionsRegisterImpl();

        bind(ComponentRegister.class).toInstance(register);
        bind(HornetSessionsRegister.class).toInstance(sessionsRegister);
        bind(PropertiesReader.class).to(ContainerPropertiesReaderImpl.class).in(Singleton.class);
        bind(JarCacheManager.class).to(JarCacheManagerImpl.class).in(Singleton.class);

        bind(ContainerProxy.class).to(ContainerProxyImpl.class);
        bind(ProxyClient.class).to(ProxyClientImpl.class);
    }
}

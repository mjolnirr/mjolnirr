package com.mjolnirr.container;

import com.mjolnirr.lib.ApplicationInstanceManager;

/**
 * Hack for windows. Send stop signal to the main application
 * */
public class WindowsServiceStopper {
    public static void main(String[] args) {
        if (!ApplicationInstanceManager.registerInstance(args, Main.CONTAINER_PORT)) {
            // instance already running.
            System.out.println("Signal passed to proxy.");
            System.exit(0);
        }

        System.exit(0);
    }
}

package com.mjolnirr.container.proxy;

import com.mjolnirr.lib.Request;
import com.mjolnirr.lib.Response;
import com.mjolnirr.lib.exceptions.*;

import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Interface for communication between any client and specific application from this container
 * I've made it look like proxy for stand-alone development mode
 */
@WebService(name = "ProxyWeb", targetNamespace = "http://web.proxy.mjolnirr.com/")
@SOAPBinding(style = Style.RPC)
public interface ContainerProxy {
    /**
     * Calling specific method from the container
     *
     * @param request Description of the target method
     *
     * @return Response of the method
     *
     * @throws BadRequestException
     * @throws ApplicationNotFoundException
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    @WebMethod
    public Response processRequest(Request request) throws
            BadRequestException,
            ApplicationNotFoundException,
            ClassNotFoundException,
            NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException, ApplicationErrorException;

    /**
     * Returns the XML (JavaFX-compatible) of one of the application's pages
     *
     * @param applicationName Name of the target application
     * @param page Name of the page to look for
     *
     * @return Page content as bytes ('cause DataHandler is exposed as byte[] with JAX-WS)
     *
     * @throws StaticFileNotFoundException
     * @throws ApplicationNotFoundException
     * @throws StaticFileCannotBeReadException
     * @throws ModuleAccessException
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "showInterface", targetNamespace = "http://web.proxy.mjolnirr.com/", className = "com.mjolnirr.proxy.web.ShowInterface")
    @ResponseWrapper(localName = "showInterfaceResponse", targetNamespace = "http://web.proxy.mjolnirr.com/", className = "com.mjolnirr.proxy.web.ShowInterfaceResponse")
    @Action(input = "http://web.proxy.mjolnirr.com/ProxyWeb/showInterfaceRequest", output = "http://web.proxy.mjolnirr.com/ProxyWeb/showInterfaceResponse", fault = {
            @FaultAction(className = StaticFileNotFoundException.class, value = "http://web.proxy.mjolnirr.com/ProxyWeb/showInterface/Fault/StaticFileNotFoundException_Exception"),
            @FaultAction(className = ApplicationNotFoundException.class, value = "http://web.proxy.mjolnirr.com/ProxyWeb/showInterface/Fault/ApplicationNotFoundException_Exception"),
            @FaultAction(className = StaticFileCannotBeReadException.class, value = "http://web.proxy.mjolnirr.com/ProxyWeb/showInterface/Fault/StaticFileCannotBeReadException_Exception"),
            @FaultAction(className = ModuleAccessException.class, value = "http://web.proxy.mjolnirr.com/ProxyWeb/showInterface/Fault/ModuleAccessException_Exception")
    })
    DataHandler showInterface(String applicationName, String page)
            throws
            StaticFileNotFoundException,
            ApplicationNotFoundException,
            StaticFileCannotBeReadException,
            ModuleAccessException;

    /**
     * Unload an application from container
     * @param applicationName Name of an application
     */
    @WebMethod
    void unloadApplication(String applicationName);

    /**
     * Returns content of the static file
     * @param applicationName Name of the target application
     * @param resourceName Name of the resource file to read
     *
     * @return Content of the resource file as bytes ('cause DataHandler exposed as byte[] with JAX-WS)
     *
     * @throws ApplicationNotFoundException
     * @throws StaticFileNotFoundException
     * @throws StaticFileCannotBeReadException
     * @throws ModuleAccessException
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getResourceFile", targetNamespace = "http://web.proxy.mjolnirr.com/", className = "com.mjolnirr.proxy.web.GetResourceFile")
    @ResponseWrapper(localName = "getResourceFileResponse", targetNamespace = "http://web.proxy.mjolnirr.com/", className = "com.mjolnirr.proxy.web.GetResourceFileResponse")
    @Action(input = "http://web.proxy.mjolnirr.com/ProxyWeb/getResourceFileRequest", output = "http://web.proxy.mjolnirr.com/ProxyWeb/getResourceFileResponse", fault = {
            @FaultAction(className = ApplicationNotFoundException.class, value = "http://web.proxy.mjolnirr.com/ProxyWeb/getResourceFile/Fault/ApplicationNotFoundException_Exception"),
            @FaultAction(className = StaticFileNotFoundException.class, value = "http://web.proxy.mjolnirr.com/ProxyWeb/getResourceFile/Fault/StaticFileNotFoundException_Exception"),
            @FaultAction(className = StaticFileCannotBeReadException.class, value = "http://web.proxy.mjolnirr.com/ProxyWeb/getResourceFile/Fault/StaticFileCannotBeReadException_Exception"),
            @FaultAction(className = ModuleAccessException.class, value = "http://web.proxy.mjolnirr.com/ProxyWeb/getResourceFile/Fault/ModuleAccessException_Exception")
    })
    DataHandler getResourceFile(String applicationName, String resourceName)
            throws
            ApplicationNotFoundException,
            StaticFileNotFoundException,
            StaticFileCannotBeReadException,
            ModuleAccessException;

    /**
     * Returns list of applications loaded into container
     *
     * @return List of applications names
     */
    @WebMethod
    List<String> getLoadedApplications();
}

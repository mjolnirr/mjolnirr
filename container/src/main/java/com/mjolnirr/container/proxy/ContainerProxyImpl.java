package com.mjolnirr.container.proxy;

import com.google.gson.Gson;
import com.mjolnirr.lib.*;
import com.mjolnirr.lib.exceptions.*;
import com.mjolnirr.container.lib.Component;
import com.mjolnirr.container.lib.ComponentRegister;
import org.xml.sax.SAXException;

import javax.activation.DataHandler;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the ContainerProxy interface functionality
 * I've made it look like proxy for stand-alone development mode
 */
@WebService(name = "ProxyWeb",
        portName = "ProxyWebPort",
        serviceName = "ProxyWebImplService",
        targetNamespace = "http://web.proxy.mjolnirr.com/")
public class ContainerProxyImpl implements ContainerProxy {
    /**
     * Component register. Stores all the loaded components
     * */
    @Inject
    private ComponentRegister register;

    /**
     * Calling specific method from the container
     *
     * @param request Description of the target method
     *
     * @return Response of the method
     *
     * @throws BadRequestException
     * @throws ApplicationNotFoundException
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    @Override
    public Response processRequest(Request request) throws BadRequestException, ApplicationNotFoundException, ClassNotFoundException, NoSuchMethodException, ApplicationErrorException {
        String badRequestMessage = "";
        if (request == null) {
            badRequestMessage += "Request can't be a null object!";
        } else {
            if (request.getApplicationName() == null || request.getApplicationName().isEmpty()) {
                badRequestMessage += "Name of the requested component can't be null or empty! ";
            }
            if (request.getMethodName() == null || request.getMethodName().isEmpty()) {
                badRequestMessage += "Name of the requested method can't be null or empty! ";
            }
        }

        if (!badRequestMessage.isEmpty()) {
            LoggerFactory.getLogger().error(badRequestMessage);

            throw new BadRequestException(badRequestMessage);
        }

        String message = String.format("Request to component \"%s\" container \"%s\", method \"%s\"", request.getApplicationName(), Properties.CONTAINER_UUID, request.getMethodName());
        LoggerFactory.getLogger().info(message);

        //  Get an instance of the requested component
        Component component = register.getInstance(request.getApplicationName());
        if (component == null) {
            message = String.format("Component \"%s\" is not found!", request.getApplicationName());
            LoggerFactory.getLogger().error(message);

            throw new ApplicationNotFoundException(message);
        }

        try {
            Object responseObject = component.invokeMethod(
                    request.getMethodName(),
                    false,
                    request,
                    request.getArgs(
                            component.getClassLoader(),
                            component.getComponentMethod(request.getMethodName())).toArray());
            Gson gson = new Gson();

            String responseClass = component.getReturnClassName(request.getMethodName());
            String responseString;
            if (TypeHelper.isFile(responseClass)) {
                //  Put file to string
                responseString = gson.toJson(TypeHelper.fromFile(responseObject));
            } else if (responseObject instanceof List) {
                List list = (List) responseObject;
                List<String> results = new ArrayList<String>();

                if ((list.size() > 0) && (list.get(0) instanceof File)) {
                    for (Object item : list) {
                        results.add(TypeHelper.fromFile(item));
                    }
                    responseString = gson.toJson(results);
                } else {
                    responseString = gson.toJson(list);
                }
            } else {
                responseString = gson.toJson(responseObject);
            }

            Response response = new Response(
                    component.getReturnClassName(request.getMethodName()),
                    responseString,
                    request.getSource()
            );
            response.setToken(request.getToken());
            response.setSync(request.isSync());

            return response;
        } catch (Exception e) {
            LoggerFactory.getLogger().info("Failed to load content to file", e);

            return new Response(e);
        } finally {
            System.gc();
        }
    }

    /**
     * Returns the XML (JavaFX-compatible) of one of the application's pages
     *
     * @param applicationName Name of the target application
     * @param page Name of the page to look for
     *
     * @return Page content as bytes ('cause DataHandler is exposed as byte[] with JAX-WS)
     *
     * @throws StaticFileNotFoundException
     * @throws ApplicationNotFoundException
     * @throws StaticFileCannotBeReadException
     * @throws ModuleAccessException
     */
    @Override
    public DataHandler showInterface(String applicationName, String page) throws StaticFileNotFoundException, ApplicationNotFoundException, StaticFileCannotBeReadException, ModuleAccessException {
        String pageName = page;

        if ((page == null) || (page.equals(""))) {
            pageName = Properties.MAIN_PAGE;
        }

        try {
            return register.getApplicationInterface(applicationName, pageName);
        } catch (SAXException e) {
            LoggerFactory.getLogger().fatal("Failed to parse interface", e);
        } catch (ParserConfigurationException e) {
            LoggerFactory.getLogger().fatal("Failed to initialize parser", e);
        }

        return null;
    }

    /**
     * Returns content of the static file
     * @param applicationName Name of the target application
     * @param resourceName Name of the resource file to read
     *
     * @return Content of the resource file as bytes ('cause DataHandler exposed as byte[] with JAX-WS)
     *
     * @throws ApplicationNotFoundException
     * @throws StaticFileNotFoundException
     * @throws StaticFileCannotBeReadException
     * @throws ModuleAccessException
     */
    @Override
    public DataHandler getResourceFile(String applicationName, String resourceName) throws ApplicationNotFoundException, StaticFileNotFoundException, StaticFileCannotBeReadException, ModuleAccessException {
        return register.getApplicationResource(applicationName, resourceName);
    }

    /**
     * Unload an application from container
     *
     * @param applicationName Name of an application
     */
    @Override
    public void unloadApplication(String applicationName) {
        register.unloadComponent(applicationName);
    }

    /**
     * Returns list of applications loaded into container
     *
     * @return List of applications names
     */
    @Override
    public List<String> getLoadedApplications() {
        return register.getComponentsNames();
    }
}

package com.mjolnirr.container;

import com.mjolnirr.lib.ContainerTransport;
import com.mjolnirr.lib.properties.PropertiesReader;

import javax.inject.Inject;

/**
 *  Singleton for Mjolnirr Proxy JAX-WS client implementation
 * */
public class ProxyClientImpl implements ProxyClient {
    /**
     *  Config key which holds the proxy JAX-WS host
     * */
    private static final String CONFIG_PROXY_HOST = "proxy_web_host";
    /**
     *  Config key which holds the proxy JAX-WS port
     * */
    private static final String CONFIG_PROXY_PORT = "proxy_web_port";

    /**
     *  Proxy JAX-WS client
     * */
    private ContainerTransport proxy = null;

    /**
     *  Proxy JAX-WS host value
     * */
    private String host;
    /**
     *  Proxy JAX-WS port value
     * */
    private int port;

    /**
     * Guice-injectable constructor
     * */
    @Inject
    ProxyClientImpl(PropertiesReader properties) {
        //  Reading config
        host = properties.getConfig().getString(CONFIG_PROXY_HOST);
        port = properties.getConfig().getInt(CONFIG_PROXY_PORT);

        //  Creating proxy JAX-WS client
//        try {
//            ProxyWebImplService service = new ProxyWebImplService(new URL("http://" + host + ":" + port + "/proxy?wsdl"));
//            proxy = service.getProxyWebPort();

            proxy = new ContainerTransport(host, port);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
    }

    /**
     *  Get Proxy client instance
     *
     *  @return Proxy client instance
     * */
    @Override
    public ContainerTransport getProxy() {
        return proxy;
    }
}

package com.mjolnirr.container;

import com.mjolnirr.lib.ContainerTransport;

/**
 *  Singleton for Mjolnirr Proxy JAX-WS client
 * */
public interface ProxyClient {
    /**
     *  Get Proxy client instance
     *
     *  @return Proxy client instance
     * */
    public ContainerTransport getProxy();
}

package com.mjolnirr.container.msg;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.mjolnirr.container.ContainerModule;
import com.mjolnirr.container.ProxyClient;
import com.mjolnirr.container.lib.ComponentRegister;
import com.mjolnirr.container.lib.JarCacheManager;
import com.mjolnirr.lib.Callback;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.exceptions.MjolnirrException;
import com.mjolnirr.lib.msg.ContainerUtilRequest;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.12.13
 * Time: 13:06
 * To change this template use File | Settings | File Templates.
 */
public class ContainerUtilCallback implements Callback<String> {
    private static Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    @Inject
    private JarCacheManager cacheManager;

    @Inject
    private ProxyClient proxy;

    @Inject
    private ComponentRegister componentRegister;

    public ContainerUtilCallback() {
        Injector injector = InjectorSingleton.getInjector(ContainerModule.class);
        injector.injectMembers(this);
    }

    @Override
    public void run(String result) {
        if (result != null) {
            try {
                ContainerUtilRequest request = gson.fromJson(result, ContainerUtilRequest.class);

                if (request.isLoad()) {
                    boolean wasReloaded = false;

                    //  If JAR is in cache - load it
                    if (!cacheManager.hasInCache(request.getComponentName())) {
                        //  Download component
                        File downloadedJar = downloadJAR(request.getComponentName(), request.getJarName());
                        cacheManager.saveToCache(request.getComponentName(), downloadedJar);
                    } else {
                        //  JAR is in cache, so let's check modification data
                        if (getRemoteModificationDate(request.getComponentName(), request.getJarName()) > cacheManager.lastModified(request.getComponentName())) {
                            LoggerFactory.getLogger().warn("Local JAR is too old, downloading new version");

                            wasReloaded = true;
                            File downloadedJar = downloadJAR(request.getComponentName(), request.getJarName());
                            cacheManager.saveToCache(request.getComponentName(), downloadedJar);
                        }
                    }

                    if (wasReloaded) {
                        componentRegister.reloadComponent(cacheManager.getComponentJAR(request.getComponentName()), request.getComponentName());
                    }

                    if (!componentRegister.hasComponent(request.getComponentName())) {
                        componentRegister.addComponent(cacheManager.getComponentJAR(request.getComponentName()), request.getComponentName());
                    } else {
                        LoggerFactory.getLogger().info("Component already loaded");
                    }
                } else if (request.isUnload()) {
                    //  Unload
                    componentRegister.unloadComponent(request.getComponentName());
                }
            } catch (Exception ex) {
                LoggerFactory.getLogger().error("Failed to parse message", ex);
            }
        }
    }

    private long getRemoteModificationDate(String componentName, String jarName) throws MjolnirrException {
        return proxy.getProxy().getResourceModificationDate(componentName, jarName);
    }

    private File downloadJAR(String componentName, String jarName) throws Exception {
        LoggerFactory.getLogger().warn("No JAR in cache, downloading: " + componentName + " / " + jarName);

        byte[] content = proxy.getProxy().getResourceFile(componentName, jarName);

        File result = File.createTempFile("hive", "");

        IOUtils.write(content, new FileOutputStream(result));

        return result;
    }
}

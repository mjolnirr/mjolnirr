package com.mjolnirr.lib.msg;

import com.google.inject.Injector;
import com.mjolnirr.container.ContainerModule;
import com.mjolnirr.container.lib.ComponentRegister;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.LoggerFactory;
import com.mjolnirr.lib.Properties;
import org.hornetq.api.core.HornetQException;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

/**
 * Class to control messages receiving and processing
 */
public class MessagesController {
    /**
     * Message controller singleton field
     * */
    private static MessagesController instance;

    /**
     * Register which stores all the information about loaded components
     * */
    @Inject
    private ComponentRegister register;

    /**
     * Current instance of the messages receiver
     */
    private Map<String, ComponentMsgReceiver> receivers;

    /**
     * Singleton initialization scope
     * */
    static {
        try {
            instance = new MessagesController();
        } catch (InterruptedException e) {
            LoggerFactory.getLogger().fatal("Can't instantiate messages controller!", e);
            System.exit(1);
        } catch (HornetQException e) {
            LoggerFactory.getLogger().fatal(e);
        }
    }

    /**
     * Get the instance of MessageController. Standard singleton method
     *
     * @return MessageController instance
     * */
    public static MessagesController getInstance() {
        return instance;
    }

    /**
     * MessageController constructor. Private to instantiate only as singleton
     * */
    private MessagesController() throws InterruptedException, HornetQException {
        Injector injector = InjectorSingleton.getInjector(ContainerModule.class);
        injector.injectMembers(this);

        startReceivers();
    }

    /**
     * Start message receiving and processing
     *
     * @throws InterruptedException
     */
    public void startReceivers() throws InterruptedException, HornetQException {
        if (receivers != null) {
            stopReceivers();
        }

        receivers = new HashMap<String, ComponentMsgReceiver>();

        for (String componentName : register.getComponentsNames()) {
            startReceiver(componentName);
        }
    }

    /**
     * Start receiving messages for specific component
     *
     * @param componentName Name of the component and channel to listen to
     */
    public void startReceiver(String componentName) throws HornetQException, InterruptedException {
        if (receivers.containsKey(componentName)) {
            LoggerFactory.getLogger().info("Receivers for " + componentName + " already run. Stopping old ones...");

            ComponentMsgReceiver existingReceivers = receivers.get(componentName);

            existingReceivers.getPrivateChannelReceiver().kill();
            existingReceivers.getPrivateChannelReceiver().join(1000);

            existingReceivers.getPublicChannelReceiver().kill();
            existingReceivers.getPublicChannelReceiver().join(1000);
        }

        ComponentMsgReceiver receiver = new ComponentMsgReceiver(componentName);
        receiver.getPrivateChannelReceiver().start();
        receiver.getPublicChannelReceiver().start();

        receivers.put(componentName, receiver);
    }

    /**
     * Stop receiving messages for all components
     *
     * @throws InterruptedException
     */
    public void stopReceivers() throws InterruptedException, HornetQException {
        if (receivers == null) return;

        for (String componentName : receivers.keySet()) {
            ComponentMsgReceiver receiver = receivers.get(componentName);

            if (receiver != null) {
                receiver.getPrivateChannelReceiver().kill();
                receiver.getPrivateChannelReceiver().join(1000);

                receiver.getPublicChannelReceiver().kill();
                receiver.getPublicChannelReceiver().join(1000);
            }
        }

        receivers.clear();
    }

    /**
     * Stop receiving messages for specific component
     *
     * @param componentName
     * @throws InterruptedException
     */
    public void stopReceiver(String componentName) throws InterruptedException, HornetQException {
        ComponentMsgReceiver receiver = receivers.get(componentName);

        if (receiver != null) {
            receiver.getPrivateChannelReceiver().kill();
            receiver.getPrivateChannelReceiver().join(1000);

            receiver.getPublicChannelReceiver().kill();
            receiver.getPublicChannelReceiver().join(1000);

            receivers.remove(componentName);
        }
    }

    /**
     * Wrapper for component messages receivers
     */
    public static class ComponentMsgReceiver {
        /**
         * Public channel receiver
         * */
        private MsgReceiver publicChannelReceiver;
        /**
         * Private channel receiver
         * */
        private MsgReceiver privateChannelReceiver;

        /**
         * Get the private channel receiver
         *
         * @return Private channel receiver
         * */
        public MsgReceiver getPrivateChannelReceiver() {
            return privateChannelReceiver;
        }

        /**
         * Get the public channel receiver
         *
         * @return Public channel receiver
         * */
        public MsgReceiver getPublicChannelReceiver() {
            return publicChannelReceiver;
        }

        /**
         * Component message receiver constuctor
         *
         * @param componentName Name of the component to listen to
         * */
        public ComponentMsgReceiver(String componentName) {
            publicChannelReceiver = new MsgReceiver(componentName, RequestCallback.class);

            String privateChannel = Properties.privateChannelName(componentName);
            privateChannelReceiver = new MsgReceiver(privateChannel, ResponseCallback.class);
        }
    }
}

package com.mjolnirr.lib.msg;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.mjolnirr.container.ContainerModule;
import com.mjolnirr.container.lib.ComponentRegister;
import com.mjolnirr.lib.Callback;
import com.mjolnirr.lib.InjectorSingleton;
import com.mjolnirr.lib.LoggerFactory;
import org.hornetq.api.core.HornetQException;

/**
 * Implementation of messages receiver
 */
public class MsgReceiver extends Thread {
    @Inject
    private ComponentRegister componentRegister;

    @Inject
    private HornetSessionsRegister sessionsRegister;

    /**
     * Communicator instance. Used for message sending and receiving
     * */
    private Communicator communicator;

    /**
     * Channel name
     * */
    private String channelName;
    /**
     * Flag shows that system need or needn't any messages
     */
    private boolean alive;
    /**
     * Callback to run on response
     * */
    private Class callback;

    /**
     * Constructor
     *
     * @param channel Name of the channel to listen to
     * @param callback callback to run on response
     * */
    public <T extends Callback<String>> MsgReceiver(String channel, Class<T> callback) {
        Injector injector = InjectorSingleton.getInjector(ContainerModule.class);
        injector.injectMembers(this);

        communicator = new HornetCommunicator();
        this.channelName = channel;
        this.callback = callback;
    }

    /**
     * Notify receiver that system needn't any messages
     */
    public void kill() {
        alive = false;
    }

    /**
     * Starts waiting for message on the specified channel
     * */
    @Override
    public void run() {
        alive = true;
        while (alive) {
            try {
                LoggerFactory.getLogger().info("Waiting for message on channelName \"" + channelName + "\".");
                HornetCommunicator.SessionObject currentSession = sessionsRegister.getReceiverSession(channelName);
                LoggerFactory.getLogger().info("Receive initialized on channelName \"" + channelName + "\".");
                communicator.receive(currentSession, channelName, callback, new Communicator.AcknowledgeCallback() {
                    @Override
                    public boolean isAcknowledged(String channelName) {
                        if (channelName.split("/").length > 1) {
                            //  This is private channel
                            return true;
                        }

                        return componentRegister.hasComponent(channelName);
                    }
                });
                LoggerFactory.getLogger().info("Message came on channelName \"" + channelName + "\".");
            } catch (Exception e) {
                String message = String.format("Can't receive message from channelName \"%s\"!", channelName);
                LoggerFactory.getLogger().error(message, e);
            }
        }
    }
}
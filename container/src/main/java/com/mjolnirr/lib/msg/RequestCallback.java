package com.mjolnirr.lib.msg;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Injector;
import com.mjolnirr.lib.*;
import com.mjolnirr.container.ContainerModule;
import com.mjolnirr.container.proxy.ContainerProxy;
import com.mjolnirr.lib.msg.Communicator;
import com.mjolnirr.lib.msg.HornetCommunicator;

import javax.inject.Inject;

/**
 * Specific implementation of callback for requests processing
 */
public class RequestCallback implements Callback<String> {
    /**
     * JSON parser instance
     * */
//    private static Gson gson = new Gson();
    private static Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();;
    /**
     * Communicator instance. Used for messaging
     * */
    private HornetCommunicator communicator;

    /**
     * Container JAX-WS listener instance. User for request processing
     * */
    @Inject
    private ContainerProxy containerProxy;

    /**
     * Constructor
     * */
    public RequestCallback() {
        Injector injector = InjectorSingleton.getInjector(ContainerModule.class);
        injector.injectMembers(this);
        communicator = new HornetCommunicator();
    }

    /**
     * Handle result and parse it to Request class
     *
     * @param result Result to process
     * */
    @Override
    public void run(String result) {
        if (result != null) {
            try {
                System.out.println("Request came: " + result);

                Request request = gson.fromJson(result, Request.class);
                processRequest(request);
            } catch (JsonSyntaxException e) {
                LoggerFactory.getLogger().error("Can't process message. It is not json serialization of com.mjolnirr.lib.Request.");
            }
        }
    }

    /**
     * Process request to component
     *
     * @param request Request
     * */
    private void processRequest(Request request) {
        Response response;

        try {
//            communicator.changeExecutionState(new Execution(Properties.CONTAINER_UUID, request.getToken(), request.getApplicationName(), true));

            response = containerProxy.processRequest(request);
            response.setOnResponse(request.getOnResponse());

//            communicator.changeExecutionState(new Execution(Properties.CONTAINER_UUID, request.getToken(), request.getApplicationName(), false));
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Can't process request from \"" + request.getSource() + "\".", e);
            response = new Response(e);
        }

        try {
            communicator.send(request.getSource(), response.toString());
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Can't send response to channel \"" + request.getSource() + "\".", e);
        }
    }
}
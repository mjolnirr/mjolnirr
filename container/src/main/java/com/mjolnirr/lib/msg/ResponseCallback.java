package com.mjolnirr.lib.msg;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Injector;
import com.mjolnirr.container.ContainerModule;
import com.mjolnirr.container.lib.Component;
import com.mjolnirr.container.lib.ComponentRegister;
import com.mjolnirr.container.proxy.ContainerProxy;
import com.mjolnirr.lib.*;

import javax.inject.Inject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ResponseCallback implements Callback<String> {
    /**
     * JSON parser instance
     * */
    private static Gson gson = new Gson();

    /**
     * Container JAX-WS listener instance. User for request processing
     * */
    @Inject
    private ContainerProxy containerProxy;
    /**
     * Component register. Stores all loaded component info
     * */
    @Inject
    private ComponentRegister register;

    /**
     * Constructor
     * */
    public ResponseCallback() {
        Injector injector = InjectorSingleton.getInjector(ContainerModule.class);
        injector.injectMembers(this);
    }

    /**
     * Handle result and parse it to Response class
     *
     * @param result Result to handle
     * */
    @Override
    public void run(String result) {
        if (result != null) {
            try {
                Response response = gson.fromJson(result, Response.class);
                processResponse(response);
            } catch (JsonSyntaxException ex) {
                LoggerFactory.getLogger().warn("Can't process message. It is not json serialization of com.mjolnirr.lib.Response.");
            }
        }
    }

    /**
     * Process response (parsing and Callback invocation)
     *
     * @param response Response to process
     * */
    private void processResponse(Response response) {
//        SemaphoreHelper.release(response.getToken());

        if (response.getCode() == Response.FAIL) {
            LoggerFactory.getLogger().error("Something went wrong during request processing!", response.getFailureCause());
        } else {
            try {
                if (!response.isSync()) {
                    String componentName = parseComponentName(response.getTarget());
                    Class resultClass = getClassByName(response.getResponseClass(), componentName);

                    Object result;

                    if (TypeHelper.isFile(response.getResponseClass())) {
                        //  Put bytes to file
                        result = TypeHelper.toFile(gson.fromJson(response.getResponseObject(), String.class));
                    } else {
                        //  Parse as class
                        result = gson.fromJson(response.getResponseObject(), resultClass);

                        if (result instanceof List) {
                            List list = (List) result;
                            List fileList = new ArrayList();

                            for (int j = 0 ; j < list.size() ; j++) {
                                if (TypeHelper.isTransmittedFile((String) list.get(j))) {
                                    fileList.add(TypeHelper.toFile((String) list.get(j)));
                                } else {
                                    fileList.add(list.get(j));
                                }
                            }

                            for (Object l : fileList) {
                                System.out.println(l.getClass());
                            }

                            result = fileList;
                        }
                    }

                    //  Trying to get callback from register
                    Callback callback = CallbackRegister.getCallbackInstance(response.getOnResponse());
                    callback.run(result);
                } else {
                    CallbackRegister.setSyncResponse(response.getOnResponse(), response);
                }
            } catch (Exception e) {
                LoggerFactory.getLogger().error("Something went wrong during response processing!", e);
            }
        }
    }

    /**
     * Retrieve class by name. Tries to get it from the component JAR, if not - from env
     *
     * @param className The name of the class to load
     * @param componentName The name of the component which JAR contains target class
     *
     * @return Desired class
     *
     * @throws ClassNotFoundException
     * */
    public Class getClassByName(String className, String componentName)
            throws ClassNotFoundException {
        Component component = register.getInstance(componentName);

        try {
            return component.findClass(className);
        } catch (Exception e) {
            return Class.forName(className);
        }
    }

    /**
     * Parses component name from channel name
     *
     * @param channelName Name of the channel to parse
     *
     * @return Component name
     * */
    private String parseComponentName(String channelName) {
        String[] parts = channelName.split("/");

        return parts[1];
    }
}
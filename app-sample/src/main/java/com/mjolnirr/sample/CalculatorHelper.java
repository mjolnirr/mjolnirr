package com.mjolnirr.sample;

import com.mjolnirr.lib.msg.Execution;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

public class CalculatorHelper {
    // evaluate - returns the value obtained by evaluating the input expression
    protected static double evaluate(String inputString) {
        // temp token handle
        String tok1, tok2;

        // create a string tokenizer object, keeping the delimiters as tokens
        StringTokenizer tokens = new StringTokenizer(inputString, "+-/*", true);

        // get the first token and put its value into result
        if ((tok1 = tokens.nextToken()).equals("+"))
            tok1 = tokens.nextToken();
        else if (tok1.equals("-"))
            tok1 = "-".concat(tokens.nextToken());
        double result = new Double(tok1).doubleValue();

        // evaluate the expression
        while (tokens.hasMoreTokens()) {
            // get the operator token
            tok1 = tokens.nextToken();

            // get the second operand token
            if ((tok2 = tokens.nextToken()).equals("+"))
                tok2 = tokens.nextToken();
            else if (tok2.equals("-"))
                tok2 = "-".concat(tokens.nextToken());

            // evaluate the subexpression
            switch (tok1.charAt(0)) {
                case '+':
                    result += new Double(tok2).doubleValue();
                    break;
                case '-':
                    result -= new Double(tok2).doubleValue();
                    break;
                case '/':
                    result /= new Double(tok2).doubleValue();
                    break;
                case '*':
                    result *= new Double(tok2).doubleValue();
                    break;
            }
        }
        return result;
    }

    public static boolean checkSyntax(String inputString) throws Exception {
        int opCount = 1;        // start with a value
        boolean hasDec = false; // does the current # have a decimal component?
        int i;

        // check each character
        for (i = 0; i < inputString.length(); i++)
            switch (inputString.charAt(i)) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    opCount = 0;
                    break;
                case '.':
                    if (((opCount += 2) > 2) || hasDec)
                        return reportError(i, inputString);
                    hasDec = true;
                    break;
                case '/':
                case '*':
                    if (opCount++ > 0)
                        return reportError(i, inputString);
                    hasDec = false;
                    break;
                case '+':
                case '-':
                    if (opCount++ > 1)
                        return reportError(i, inputString);
                    hasDec = false;
                    break;
                default:
                    return reportError(i, inputString);
            }

        // make sure expression ends with a value
        if (opCount > 0)
            return reportError(i, inputString);
        else
            return true;
    }

    protected static boolean reportError(int index, String inputString) throws Exception {
        StringBuilder errorText = new StringBuilder();

        // create a new string with the first line of output
        errorText.append("Syntax error:\n");

        // echo the user input as second line
        errorText.append(inputString);
        errorText.append("\n");

        // display pointer to error position as third line
        for (int i = 0; i < index; i++) {
            errorText.append(" ");
        }
        errorText.append("^");

        // copy the output string text to the output text area
        throw new Exception(errorText.toString());
    }
}

package com.mjolnirr.sample;

import com.mjolnirr.lib.annotations.MjolnirrComponent;
import com.mjolnirr.lib.annotations.MjolnirrMethod;
import com.mjolnirr.lib.annotations.MjolnirrInterface;
import com.mjolnirr.lib.component.AbstractApplication;
import com.mjolnirr.lib.component.ComponentContext;
import com.mjolnirr.lib.msg.HornetCommunicator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@MjolnirrComponent(
        componentName = "calculator",
        instancesMaxCount = 1,
        memoryVolume = 128,
        interfaces = {
                @MjolnirrInterface(pageNameWildCard = "main", allowedUsers = { "privileged" } )
        }
)
public class Calculator extends AbstractApplication {
    private ComponentContext context;

    @MjolnirrMethod(maximumExecutionTime = 30)
    public String calculate(String expression) throws Exception {
        if (expression.length() > 0) {
            // check syntax, evaluate and display results if correct
            if (CalculatorHelper.checkSyntax(expression)) {
                return String.valueOf(CalculatorHelper.evaluate(expression));
            }
        }

        throw new Exception("Expression missing!");
        
        List<File> testFiles;

        testFiles = new HornetCommunicator().sendSync(context, 
            "unicore", "run", new ArrayList<Object>() {{
                Map<String, String> params = new HashMap<String, String>();
                List<String> inputs = new ArrayList<String>();
                List<String> outputs = new ArrayList<String>();
                outputs.add("*");

                add("Date");
                add("1.0");
                add(params);
                add(inputs);
               add(outputs);
        }}, List.class);

        for (File file : testFiles) {
            System.out.println("After: " + Files.readAllLines(file.toPath(), Charset.defaultCharset()));
        }

    }

    @Override
    public void initialize(ComponentContext context) {
        this.context = context;
    }
}

package com.mjolnirr.sample;

import com.mjolnirr.lib.annotations.MjolnirrComponent;
import com.mjolnirr.lib.annotations.MjolnirrMethod;
import com.mjolnirr.lib.component.AbstractApplication;
import com.mjolnirr.lib.component.AbstractModule;
import com.mjolnirr.lib.component.ComponentContext;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

/**
 * Created by sk_ on 4/15/14.
 */
@MjolnirrComponent(componentName = "file_transmission", instancesMaxCount = 1, memoryVolume = 128)
public class FileTransmission extends AbstractModule {
    private ComponentContext context;

    @Override
    public void initialize(ComponentContext context) {
        this.context = context;
    }

    @MjolnirrMethod
    public List<File> testTran(List<File> input) throws IOException {
        for (File file : input) {
            System.out.println("Incoming: " + Files.readAllLines(file.toPath(), Charset.defaultCharset()));

            FileWriter fileWritter = new FileWriter(file,true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write("OLOLO TROLOLO");
            bufferWritter.close();
        }

        return input;
    }
}

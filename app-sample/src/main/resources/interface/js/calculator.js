function addToField(symbol) {
    var inputField = $("#calculator-string");

    inputField.val(inputField.val() + symbol);
}

function calc() {
    var inputField = $("#calculator-string");

    try {
        inputField.val(callRemoteMethodSync({
            method: "calculate"
            , args: [ inputField.val() ]
        }));
    } catch (err) {
        bootbox.alert(err);
    }
}

function clearField() {
    var inputField = $("#calculator-string");

    inputField.val("");
}

function removeLastSymbol() {
    var inputField = $("#calculator-string");
    var content = inputField.val();

    content = content.substring(0, content.length - 1);
    inputField.val(content);
}